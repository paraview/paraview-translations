<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>pqClientMainWindow</name>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="19"/>
        <source>MainWindow</source>
        <translation>FenetrePrincipale</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="55"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="60"/>
        <source>&amp;Sources</source>
        <translation>&amp;Sources</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="65"/>
        <source>Fi&amp;lters</source>
        <translation>Fi&amp;ltres</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="70"/>
        <source>&amp;Edit</source>
        <translation>&amp;Modifier</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="75"/>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="80"/>
        <source>&amp;Tools</source>
        <translation>Ou&amp;tils</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="85"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="90"/>
        <source>&amp;Macros</source>
        <translation>&amp;Macros</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="95"/>
        <source>&amp;Catalyst</source>
        <translation>&amp;Catalyst</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="100"/>
        <source>E&amp;xtractors</source>
        <translation>E&amp;xtracteurs</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="120"/>
        <source>Pipeline Browser</source>
        <translation>Navigateur de Pipeline</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="136"/>
        <source>Statistics Inspector</source>
        <translation>Inspecteur de statistiques</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="148"/>
        <source>Comparative View Inspector</source>
        <translation>Inspecteur de vue comparative</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="160"/>
        <source>Collaboration Panel</source>
        <translation>Panneau de collaboration</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="172"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="207"/>
        <source>Memory Inspector</source>
        <translation>Inspecteur de consommation mémoire</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="225"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="237"/>
        <source>MultiBlock Inspector</source>
        <translation>Inspecteur de multi bloc</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="249"/>
        <source>Light Inspector</source>
        <translation>Inspecteur de l&apos;éclairage</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="261"/>
        <source>Color Map Editor</source>
        <translation>Éditeur de fonction de transfert de couleur</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="273"/>
        <source>Selection Editor</source>
        <translation>Éditeur de sélection</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="294"/>
        <source>Material Editor</source>
        <translation>Éditeur de matériaux</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="310"/>
        <source>OSPRay support not available!</source>
        <translation>Gestion de OSPRay non disponible !</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="325"/>
        <source>Display</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="341"/>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="357"/>
        <source>Time Manager</source>
        <translation>Gestionnaire du temps</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="369"/>
        <source>Output Messages</source>
        <translation>Messages de sortie</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="376"/>
        <source>OutputMessages</source>
        <translation>MessagesDeSortie</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="391"/>
        <source>Python Shell</source>
        <translation>Interprète de commandes Python</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="407"/>
        <source>Python support not available!</source>
        <translation>Gestion de Python non disponible !</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="422"/>
        <source>Find Data</source>
        <translation>Localiser une donnée</translation>
    </message>
</context>
</TS>
