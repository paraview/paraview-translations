<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>pqClientMainWindow</name>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="19"/>
        <source>MainWindow</source>
        <translation>AnaPencere</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="55"/>
        <source>&amp;File</source>
        <translation>Dosya</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="60"/>
        <source>&amp;Sources</source>
        <translation>Kaynaklar</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="65"/>
        <source>Fi&amp;lters</source>
        <translation>Filtreler</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="70"/>
        <source>&amp;Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="75"/>
        <source>&amp;View</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="80"/>
        <source>&amp;Tools</source>
        <translation>Araçlar</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="85"/>
        <source>&amp;Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="90"/>
        <source>&amp;Macros</source>
        <translation>Makrolar</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="95"/>
        <source>&amp;Catalyst</source>
        <translation>Catalyst</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="100"/>
        <source>E&amp;xtractors</source>
        <translation>Ayıklayıcılar</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="120"/>
        <source>Pipeline Browser</source>
        <translation>Veri-İşlem Gözatıcı</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="136"/>
        <source>Statistics Inspector</source>
        <translation>İstatistik Denetleyicisi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="148"/>
        <source>Comparative View Inspector</source>
        <translation>Karşılaştırmalı Görünüm Denetleyicisi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="160"/>
        <source>Collaboration Panel</source>
        <translation>İşbirliği Paneli</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="172"/>
        <source>Information</source>
        <translation>Bilgi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="207"/>
        <source>Memory Inspector</source>
        <translation>Bellek Denetleyicisi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="225"/>
        <source>Properties</source>
        <translation>Özellikler</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="237"/>
        <source>MultiBlock Inspector</source>
        <translation>Çok Bloklu Denetleyici</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="249"/>
        <source>Light Inspector</source>
        <translation>Işık Denetleyicisi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="261"/>
        <source>Color Map Editor</source>
        <translation>Renk Haritası Denetleyicisi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="273"/>
        <source>Selection Editor</source>
        <translation>Seçim Düzenleyicisi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="294"/>
        <source>Material Editor</source>
        <translation>Malzeme Düzenleyicisi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="310"/>
        <source>OSPRay support not available!</source>
        <translation>OSPRay desteği mevcut değil!</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="325"/>
        <source>Display</source>
        <translation>Görüntüleme</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="341"/>
        <source>View</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="357"/>
        <source>Time Manager</source>
        <translation>Zaman Yöneticisi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="369"/>
        <source>Output Messages</source>
        <translation>Çıktı Mesajları</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="376"/>
        <source>OutputMessages</source>
        <translation>ÇıktıMesajları</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="391"/>
        <source>Python Shell</source>
        <translation>Python Kabuk</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="407"/>
        <source>Python support not available!</source>
        <translation>Python desteği mevcut değil!</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="422"/>
        <source>Find Data</source>
        <translation>Veri Bul</translation>
    </message>
</context>
</TS>
