<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>PythonShell</name>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="16"/>
        <source>Form</source>
        <translation>Modulo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="62"/>
        <source>Run Script</source>
        <translation>Eseguire uno script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="72"/>
        <source>Clear</source>
        <translation>Pulisci</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="82"/>
        <source>Reset</source>
        <translation>Azzera</translation>
    </message>
</context>
<context>
    <name>pqPythonEditorActions</name>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="38"/>
        <source>&amp;New</source>
        <translation>&amp;Nuovo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="41"/>
        <source>Create a new file</source>
        <translation>Crea un nuovo file</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="44"/>
        <source>&amp;Open...</source>
        <translation>&amp;Apri...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="47"/>
        <source>Open an existing file</source>
        <translation>Apri un file esistente</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="50"/>
        <source>&amp;Save</source>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="53"/>
        <source>Save the document to disk</source>
        <translation>Salva il documento sul disco</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="56"/>
        <source>Save &amp;As...</source>
        <translation>Salva &amp;come...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="58"/>
        <source>Save the document under a new name</source>
        <translation>Salva il documento con un nuovo nome</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="61"/>
        <source>Save As &amp;Macro...</source>
        <translation>Salva come &amp;Macro...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="63"/>
        <source>Save the document as a Macro</source>
        <translation>Salva il documento come Macro</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="66"/>
        <source>Save As &amp;Script...</source>
        <translation>Salva come &amp;Script...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="68"/>
        <source>Save the document as a Script</source>
        <translation>Salva il documento come Script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="71"/>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="373"/>
        <source>Delete All</source>
        <translation>Cancella tutto</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="73"/>
        <source>Delete all scripts from disk</source>
        <translation>Cancella tutti gli script dal disco</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="76"/>
        <source>Run...</source>
        <translation>Esegui...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="78"/>
        <source>Run the currently edited script</source>
        <translation>Esegui lo script modificato ora</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="81"/>
        <source>Cut</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="84"/>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation>Taglia il contenuto selezionato negli appunti</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="89"/>
        <source>Undo</source>
        <translation>Torna indietro</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="92"/>
        <source>Undo the last edit of the file</source>
        <translation>Cancella l&apos;ultima modifica del file</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="95"/>
        <source>Redo</source>
        <translation>Rifai</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="98"/>
        <source>Redo the last undo of the file</source>
        <translation>Rifai l&apos;ultima modifica cancellata dal file</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="101"/>
        <source>Copy</source>
        <translation>Copia</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="104"/>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation>Copia il contenuto selezionato negli appunti</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="109"/>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="112"/>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation>Incolla il contenuto degli appunti dove selezionato</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="117"/>
        <source>C&amp;lose</source>
        <translation>C&amp;hiudi</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="120"/>
        <source>Close the script editor</source>
        <translation>Chiudi l&apos;editor degli script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="123"/>
        <source>Close Current Tab</source>
        <translation>Chiudi la scheda corrente</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="126"/>
        <source>Close the current opened tab</source>
        <translation>Chiudi la scheda aperta corrente</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="356"/>
        <source>Open File</source>
        <translation>Apri file</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="357"/>
        <source>Python Files</source>
        <translation>File Python</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="374"/>
        <source>All scripts will be deleted. Are you sure?</source>
        <translation>Tutti gli script verranno cancellati. Sei sicur*?</translation>
    </message>
</context>
<context>
    <name>pqPythonFileIO</name>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="34"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="287"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="314"/>
        <source>Sorry!</source>
        <translation>Scusa!</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="35"/>
        <source>Could not create user PythonSwap directory: %1.</source>
        <translation>Non si può creare la destinazione utente PythonSwap: %1.</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="80"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="94"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="81"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="95"/>
        <source>No Filename Given!</source>
        <translation>Nessun nome assegnato!</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="102"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="183"/>
        <source>Script Editor</source>
        <translation>Editor degli script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="103"/>
        <source>Paraview found an old automatic save file %1. Would you like to recover its content?</source>
        <translation>Paraview ha trovato un vecchio file %1 salvato automaticamente. Vuoi recuperarne il contenuto?</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="184"/>
        <source>The document has been modified.
 Do you want to save your changes?</source>
        <translation>Il documento è stato modificato.
Vuoi salvare le modifiche?</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="260"/>
        <source>Save File As</source>
        <translation>Salva il file come</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="261"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="294"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="322"/>
        <source>Python Files</source>
        <translation>File Python</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="288"/>
        <source>Could not create user Macro directory: %1.</source>
        <translation>Non si può creare la destinazione utente Macro: %1.</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="294"/>
        <source>Save As Macro</source>
        <translation>Salva come Macro</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="315"/>
        <source>Could not create user Script directory: %1.</source>
        <translation>Non si può creare la destinazione utente Script: %1.</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="322"/>
        <source>Save As Script</source>
        <translation>Salva come Script</translation>
    </message>
</context>
<context>
    <name>pqPythonMacroSupervisor</name>
    <message>
        <location filename="../Qt/Python/pqPythonMacroSupervisor.cxx" line="115"/>
        <source>empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonScriptEditor</name>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="44"/>
        <source>ParaView Python Script Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="48"/>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="54"/>
        <source>%1[*] - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="48"/>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="54"/>
        <source>Script Editor</source>
        <translation type="unfinished">Editor degli script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="49"/>
        <source>File %1 saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="55"/>
        <source>File %1 opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="126"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="140"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="151"/>
        <source>&amp;Scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="156"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="157"/>
        <source>Open a python script in a new tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="161"/>
        <source>Load script into current editor tab...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="163"/>
        <source>Load a python script in the current opened tab and override its content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="166"/>
        <source>Delete...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="167"/>
        <source>Delete the script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="170"/>
        <source>Run...</source>
        <translation type="unfinished">Esegui...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="172"/>
        <source>Load a python script in a new tab and run it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="184"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonShell</name>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="162"/>
        <source>resetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="182"/>
        <source>
Python %1 on %2
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="445"/>
        <source>Run Script</source>
        <translation type="unfinished">Eseguire uno script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="446"/>
        <source>Python Files</source>
        <translation type="unfinished">File Python</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="446"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="469"/>
        <source>Error: script </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="469"/>
        <source> was empty or could not be opened.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonTabWidget</name>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="151"/>
        <source>Sorry!</source>
        <translation type="unfinished">Scusa!</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="152"/>
        <source>Cannot open file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="204"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="275"/>
        <source>Close Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="402"/>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="404"/>
        <source>New File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
