<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>pqClientMainWindow</name>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="19"/>
        <source>MainWindow</source>
        <translation>FinestraPrincipale</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="55"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="60"/>
        <source>&amp;Sources</source>
        <translation>&amp;Sorgenti</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="65"/>
        <source>Fi&amp;lters</source>
        <translation>Fi&amp;ltri</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="70"/>
        <source>&amp;Edit</source>
        <translation>&amp;Modificare</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="75"/>
        <source>&amp;View</source>
        <translation>&amp;Vista</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="80"/>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="85"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="90"/>
        <source>&amp;Macros</source>
        <translation>&amp;Macros</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="95"/>
        <source>&amp;Catalyst</source>
        <translation>&amp;Catalizzatore</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="100"/>
        <source>E&amp;xtractors</source>
        <translation>E&amp;strattori</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="120"/>
        <source>Pipeline Browser</source>
        <translation>Navigatore della Pipeline</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="136"/>
        <source>Statistics Inspector</source>
        <translation>Controllo statistiche</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="148"/>
        <source>Comparative View Inspector</source>
        <translation>Controllo del confronto delle visualizzazioni</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="160"/>
        <source>Collaboration Panel</source>
        <translation>Pannello di collaborazione</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="172"/>
        <source>Information</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="207"/>
        <source>Memory Inspector</source>
        <translation>Controllo di uso della memoria</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="225"/>
        <source>Properties</source>
        <translation>Proprietà</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="237"/>
        <source>MultiBlock Inspector</source>
        <translation>Controllo multi-blocchi</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="249"/>
        <source>Light Inspector</source>
        <translation>Controllo della luminosità</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="261"/>
        <source>Color Map Editor</source>
        <translation>Editor delle mappe di colore</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="273"/>
        <source>Selection Editor</source>
        <translation>Editor delle selezioni</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="294"/>
        <source>Material Editor</source>
        <translation>Editor dei materiali</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="310"/>
        <source>OSPRay support not available!</source>
        <translation>Supporto di OSPRay non disponibile!</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="325"/>
        <source>Display</source>
        <translation>Schermata</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="341"/>
        <source>View</source>
        <translation>Visualizzazione</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="357"/>
        <source>Time Manager</source>
        <translation>Gestione del tempo</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="369"/>
        <source>Output Messages</source>
        <translation>Messaggi di risposta</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="376"/>
        <source>OutputMessages</source>
        <translation>MessaggiDiRisposta</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="391"/>
        <source>Python Shell</source>
        <translation>Python Shell</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="407"/>
        <source>Python support not available!</source>
        <translation>Supporto Python non disponibile!</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="422"/>
        <source>Find Data</source>
        <translation>Trova dati</translation>
    </message>
</context>
</TS>
