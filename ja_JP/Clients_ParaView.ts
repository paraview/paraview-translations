<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>pqClientMainWindow</name>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="19"/>
        <source>MainWindow</source>
        <translation>メインウィンドウ</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="55"/>
        <source>&amp;File</source>
        <translation>ファイル(&amp;F)</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="60"/>
        <source>&amp;Sources</source>
        <translation>ソース(&amp;S)</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="65"/>
        <source>Fi&amp;lters</source>
        <translation>フィルター(&amp;I)</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="70"/>
        <source>&amp;Edit</source>
        <translation>編集(&amp;E)</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="75"/>
        <source>&amp;View</source>
        <translation>表示(&amp;V)</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="80"/>
        <source>&amp;Tools</source>
        <translation>ツール(&amp;T)</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="85"/>
        <source>&amp;Help</source>
        <translation>ヘルプ(&amp;H)</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="90"/>
        <source>&amp;Macros</source>
        <translation>マクロ(&amp;M)</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="95"/>
        <source>&amp;Catalyst</source>
        <translation>&amp;Catalyst</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="100"/>
        <source>E&amp;xtractors</source>
        <translation>抽出(&amp;x)</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="120"/>
        <source>Pipeline Browser</source>
        <translation>パイプライン・ブラウザ</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="136"/>
        <source>Statistics Inspector</source>
        <translation>統計表示</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="148"/>
        <source>Comparative View Inspector</source>
        <translation>比較表示検査</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="160"/>
        <source>Collaboration Panel</source>
        <translation>協調パネル</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="172"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="207"/>
        <source>Memory Inspector</source>
        <translation>メモリ検査</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="225"/>
        <source>Properties</source>
        <translation>プロパティ</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="237"/>
        <source>MultiBlock Inspector</source>
        <translation>マルチブロック設定</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="249"/>
        <source>Light Inspector</source>
        <translation>照明設定</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="261"/>
        <source>Color Map Editor</source>
        <translation>カラーマップ編集</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="273"/>
        <source>Selection Editor</source>
        <translation>選択編集</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="294"/>
        <source>Material Editor</source>
        <translation>マテリアル編集</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="310"/>
        <source>OSPRay support not available!</source>
        <translation>OSPRayはサポートされていません！</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="325"/>
        <source>Display</source>
        <translation>ディスプレイ</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="341"/>
        <source>View</source>
        <translation>表示</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="357"/>
        <source>Time Manager</source>
        <translation>時間マネージャー</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="369"/>
        <source>Output Messages</source>
        <translation>出力メッセージ</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="376"/>
        <source>OutputMessages</source>
        <translation>出力メッセージ</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="391"/>
        <source>Python Shell</source>
        <translation>Pythonシェル</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="407"/>
        <source>Python support not available!</source>
        <translation>Python はサポートされていません！</translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="422"/>
        <source>Find Data</source>
        <translation>データ検索</translation>
    </message>
</context>
</TS>
