<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>PythonShell</name>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="16"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="62"/>
        <source>Run Script</source>
        <translation>スクリプト実行</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="72"/>
        <source>Clear</source>
        <translation>クリア</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="82"/>
        <source>Reset</source>
        <translation>リセット</translation>
    </message>
</context>
<context>
    <name>pqPythonEditorActions</name>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="38"/>
        <source>&amp;New</source>
        <translation>新規(&amp;N)</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="41"/>
        <source>Create a new file</source>
        <translation>新規ファイル作成</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="44"/>
        <source>&amp;Open...</source>
        <translation>開く(&amp;O)...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="47"/>
        <source>Open an existing file</source>
        <translation>既存ファイルを開く</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="50"/>
        <source>&amp;Save</source>
        <translation>保存(&amp;S)</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="53"/>
        <source>Save the document to disk</source>
        <translation>文書をディスクに保存</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="56"/>
        <source>Save &amp;As...</source>
        <translation>名前を付けて保存(&amp;A)...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="58"/>
        <source>Save the document under a new name</source>
        <translation>文書を新しい名前で保存</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="61"/>
        <source>Save As &amp;Macro...</source>
        <translation>マクロを保存(&amp;M)...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="63"/>
        <source>Save the document as a Macro</source>
        <translation>文書をマクロとして保存</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="66"/>
        <source>Save As &amp;Script...</source>
        <translation>スクリプトを保存(&amp;S)...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="68"/>
        <source>Save the document as a Script</source>
        <translation>文書をスクリプトとして保存</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="71"/>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="373"/>
        <source>Delete All</source>
        <translation>すべて削除</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="73"/>
        <source>Delete all scripts from disk</source>
        <translation>すべてのスクリプトをディスクから削除</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="76"/>
        <source>Run...</source>
        <translation>実行...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="78"/>
        <source>Run the currently edited script</source>
        <translation>現在編集中のスクリプトを実行</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="81"/>
        <source>Cut</source>
        <translation>切り取り</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="84"/>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation>現在選択している内容をクリップボードに切り取り</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="89"/>
        <source>Undo</source>
        <translation>元に戻す</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="92"/>
        <source>Undo the last edit of the file</source>
        <translation>ファイルの最後の編集を元に戻す</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="95"/>
        <source>Redo</source>
        <translation>やり直し</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="98"/>
        <source>Redo the last undo of the file</source>
        <translation>ファイルの最後の元に戻すのやり直し</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="101"/>
        <source>Copy</source>
        <translation>コピー</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="104"/>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation>現在選択している内容をクリップボードにコピー</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="109"/>
        <source>Paste</source>
        <translation>貼り付け</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="112"/>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation>クリップボードの内容を現在選択している箇所に貼り付け</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="117"/>
        <source>C&amp;lose</source>
        <translation>閉じる(&amp;C)</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="120"/>
        <source>Close the script editor</source>
        <translation>スクリプト編集を閉じる</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="123"/>
        <source>Close Current Tab</source>
        <translation>現在のタブを閉じる</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="126"/>
        <source>Close the current opened tab</source>
        <translation>現在開いているタブを閉じる</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="356"/>
        <source>Open File</source>
        <translation>ファイルを開く</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="357"/>
        <source>Python Files</source>
        <translation>Pythonファイル</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="374"/>
        <source>All scripts will be deleted. Are you sure?</source>
        <translation>すべてのスクリプトが削除されます。よろしいですか？</translation>
    </message>
</context>
<context>
    <name>pqPythonFileIO</name>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="34"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="287"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="314"/>
        <source>Sorry!</source>
        <translation>申し訳ない！</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="35"/>
        <source>Could not create user PythonSwap directory: %1.</source>
        <translation>ユーザ PythonSwap ディレクトリを作成できませんでした: %1。</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="80"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="94"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="81"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="95"/>
        <source>No Filename Given!</source>
        <translation>ファイル名が与えられていません！</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="102"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="183"/>
        <source>Script Editor</source>
        <translation>スクリプト編集</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="103"/>
        <source>Paraview found an old automatic save file %1. Would you like to recover its content?</source>
        <translation>ParaView で古い自動保存ファイル %1 が見つかりました。内容を復元しますか?</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="184"/>
        <source>The document has been modified.
 Do you want to save your changes?</source>
        <translation>文書が変更されました。
変更を保存しますか？</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="260"/>
        <source>Save File As</source>
        <translation>名前を付けて保存</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="261"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="294"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="322"/>
        <source>Python Files</source>
        <translation>Pythonファイル</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="288"/>
        <source>Could not create user Macro directory: %1.</source>
        <translation>ユーザ Macro ディレクトリを作成できませんでした: %1。</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="294"/>
        <source>Save As Macro</source>
        <translation>マクロとして保存</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="315"/>
        <source>Could not create user Script directory: %1.</source>
        <translation>ユーザ・スクリプト・ディレクトリ %1 を作成できませんでした。</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="322"/>
        <source>Save As Script</source>
        <translation>スクリプトとして保存</translation>
    </message>
</context>
<context>
    <name>pqPythonMacroSupervisor</name>
    <message>
        <location filename="../Qt/Python/pqPythonMacroSupervisor.cxx" line="115"/>
        <source>empty</source>
        <translation>空</translation>
    </message>
</context>
<context>
    <name>pqPythonManagerRawInputHelper</name>
    <message>
        <source>Enter Input requested by Python</source>
        <translation type="vanished">Pythonが要求する入力を入力</translation>
    </message>
    <message>
        <source>Input: </source>
        <translation type="vanished">入力: </translation>
    </message>
</context>
<context>
    <name>pqPythonScriptEditor</name>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="44"/>
        <source>ParaView Python Script Editor</source>
        <translation>ParaView Pythonスクリプト編集</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="48"/>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="54"/>
        <source>%1[*] - %2</source>
        <translation>%1[*] - %2</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="48"/>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="54"/>
        <source>Script Editor</source>
        <translation>スクリプト編集</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="49"/>
        <source>File %1 saved</source>
        <translation>ファイル %1 が保存されました</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="55"/>
        <source>File %1 opened</source>
        <translation>ファイル %1 が開かれました</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="126"/>
        <source>&amp;File</source>
        <translation>ファイル(&amp;F)</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="140"/>
        <source>&amp;Edit</source>
        <translation>編集(&amp;E)</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="151"/>
        <source>&amp;Scripts</source>
        <translation>スクリプト(%S)</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="156"/>
        <source>Open...</source>
        <translation>開く...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="157"/>
        <source>Open a python script in a new tab</source>
        <translation>pythonスクリプトを新しいタブで開く</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="161"/>
        <source>Load script into current editor tab...</source>
        <translation>現在の編集タブにスクリプトをロード...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="163"/>
        <source>Load a python script in the current opened tab and override its content</source>
        <translation>現在開いているタブにpythonスクリプトをロードし、その内容を上書きする</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="166"/>
        <source>Delete...</source>
        <translation>削除...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="167"/>
        <source>Delete the script</source>
        <translation>スクリプトの削除</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="170"/>
        <source>Run...</source>
        <translation>実行...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="172"/>
        <source>Load a python script in a new tab and run it</source>
        <translation>新しいタブでpythonスクリプトを読み込んで実行する</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="184"/>
        <source>Ready</source>
        <translation>準備完了</translation>
    </message>
</context>
<context>
    <name>pqPythonShell</name>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="162"/>
        <source>resetting</source>
        <translation>リセット</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="182"/>
        <source>
Python %1 on %2
</source>
        <translation>
Python %1 on %2
</translation>
    </message>
    <message>
        <source>Enter Input requested by Python</source>
        <translation type="vanished">Pythonが要求する入力を入力</translation>
    </message>
    <message>
        <source>Input: </source>
        <translation type="vanished">入力: </translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="445"/>
        <source>Run Script</source>
        <translation>スクリプト実行</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="446"/>
        <source>Python Files</source>
        <translation>Pythonファイル</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="446"/>
        <source>All Files</source>
        <translation>すべてのファイル</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="469"/>
        <source>Error: script </source>
        <translation>エラー：スクリプト </translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="469"/>
        <source> was empty or could not be opened.</source>
        <translation> が空であったか、開くことができませんでした。</translation>
    </message>
</context>
<context>
    <name>pqPythonTabWidget</name>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="151"/>
        <source>Sorry!</source>
        <translation>申し訳ない！</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="152"/>
        <source>Cannot open file %1:
%2.</source>
        <translation>ファイル %1 を開けません:
%2。</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="204"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="275"/>
        <source>Close Tab</source>
        <translation>タブを閉じる</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="402"/>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="404"/>
        <source>New File</source>
        <translation>新規ファイル</translation>
    </message>
</context>
</TS>
