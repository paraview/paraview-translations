<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>pqClientMainWindow</name>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="19"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="55"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="60"/>
        <source>&amp;Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="65"/>
        <source>Fi&amp;lters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="70"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="75"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="80"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="85"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="90"/>
        <source>&amp;Macros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="95"/>
        <source>&amp;Catalyst</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="100"/>
        <source>E&amp;xtractors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="120"/>
        <source>Pipeline Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="136"/>
        <source>Statistics Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="148"/>
        <source>Comparative View Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="160"/>
        <source>Collaboration Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="172"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="207"/>
        <source>Memory Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="225"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="237"/>
        <source>MultiBlock Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="249"/>
        <source>Light Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="261"/>
        <source>Color Map Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="273"/>
        <source>Selection Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="294"/>
        <source>Material Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="310"/>
        <source>OSPRay support not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="325"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="341"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="357"/>
        <source>Time Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="369"/>
        <source>Output Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="376"/>
        <source>OutputMessages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="391"/>
        <source>Python Shell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="407"/>
        <source>Python support not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Clients/ParaView/ParaViewMainWindow.ui" line="422"/>
        <source>Find Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
