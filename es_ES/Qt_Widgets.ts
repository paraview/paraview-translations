<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>QuickLaunchDialog</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqQuickLaunchDialog.ui" line="16"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SeriesGeneratorDialog</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="16"/>
        <source>Generate Number Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="23"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="28"/>
        <source>Logarithmic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="33"/>
        <source>Geometric (samples)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="38"/>
        <source>Geometric (common ratio)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="58"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="81"/>
        <source>Reset using current data range values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="90"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="97"/>
        <source>Number of Samples:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="117"/>
        <source>Common Ratio:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="124"/>
        <source>1.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="146"/>
        <source>Range</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorChooserButton</name>
    <message>
        <location filename="../Qt/Widgets/pqColorChooserButton.cxx" line="154"/>
        <source>Set Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExpanderButton</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqExpanderButton.ui" line="68"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProgressWidget</name>
    <message>
        <location filename="../Qt/Widgets/pqProgressWidget.cxx" line="114"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqQuickLaunchDialog</name>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="81"/>
        <source>Type to search.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="82"/>
        <source>Enter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="83"/>
        <source>to create selected source/filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="84"/>
        <source>Shift + Enter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="85"/>
        <source>to create and apply selected source/filter. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="87"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="88"/>
        <source>to cancel.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqScaleByButton</name>
    <message>
        <location filename="../Qt/Widgets/pqScaleByButton.cxx" line="46"/>
        <source>Scale by ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSeriesGeneratorDialog</name>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="56"/>
        <source>Error: range cannot contain 0 for log.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="63"/>
        <source>Error: range cannot begin or end with 0 for a geometric series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="67"/>
        <source>Error: range cannot contain 0 for a geometric series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="74"/>
        <source>Error: range cannot begin with 0 for a geometric series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="78"/>
        <source>Error: common ratio cannot be 0 for a geometric series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="93"/>
        <source>Sample series: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="191"/>
        <source>&amp;Generate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTreeViewSelectionHelper</name>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="154"/>
        <source>Filter items (regex)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="181"/>
        <source>Check highlighted items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="189"/>
        <source>Uncheck highlighted items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="211"/>
        <source>Sort (ascending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="212"/>
        <source>Sort (descending)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="222"/>
        <source>Clear sorting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTreeWidgetSelectionHelper</name>
    <message>
        <location filename="../Qt/Widgets/pqTreeWidgetSelectionHelper.cxx" line="66"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeWidgetSelectionHelper.cxx" line="67"/>
        <source>Uncheck</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
