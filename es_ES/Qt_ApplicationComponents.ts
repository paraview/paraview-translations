<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AnnulusPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="29"/>
        <source>Enable/disable showing the interactive annulus widget in the 3d render view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="32"/>
        <source>Show Annulus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="47"/>
        <source>Enable/disable the ability to translate the bounding box by moving it with the mouse.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="50"/>
        <source>Outline Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="57"/>
        <source>Enable/disable the ability to scale the widget with the mouse.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="60"/>
        <source>Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="77"/>
        <source>Inner Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="84"/>
        <source>Outer Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="94"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="117"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="134"/>
        <source>Use the X-axis as the annulus&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="137"/>
        <source>Along X Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="144"/>
        <source>Use the Y-axis as the annulus&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="147"/>
        <source>Along Y Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="154"/>
        <source>Use the Z-axis as the annulus&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="157"/>
        <source>Along Z Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="164"/>
        <source>Use the camera&apos;s view direction as the axis of the annulus.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="167"/>
        <source>Along Camera Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="174"/>
        <source>Reset the camera to look along the annulus&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="177"/>
        <source>Reset Camera to Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="184"/>
        <source>Reset the annulus&apos;s properties based on the data bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAnnulusPropertyWidget.ui" line="187"/>
        <source>Reset to Data Bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackgroundEditorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="26"/>
        <source>Single color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="31"/>
        <source>Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="36"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="41"/>
        <source>Skybox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="62"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="81"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="119"/>
        <source>Restore Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="94"/>
        <source>Color 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBackgroundEditorWidget.ui" line="143"/>
        <source>Use As Environment Lighting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="37"/>
        <source>Show the interactive box widget in the 3d scene.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="40"/>
        <source>Show Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="67"/>
        <source>Rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="77"/>
        <source>Translate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="108"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="143"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use reference bounding box. When checked, &lt;span style=&quot; font-weight:600;&quot;&gt;Position&lt;/span&gt;, &lt;span style=&quot; font-weight:600;&quot;&gt;Rotation&lt;/span&gt;, and &lt;span style=&quot; font-weight:600;&quot;&gt;Scale&lt;/span&gt; are specified relative to the explicitly provided reference bounding box.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="146"/>
        <source>Use Reference Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="158"/>
        <source>Minimum X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="165"/>
        <source>Maximum X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="172"/>
        <source>Minimum Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="179"/>
        <source>Maximum Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="186"/>
        <source>Minimum Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="193"/>
        <source>Maximum Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="122"/>
        <source>Reference Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="207"/>
        <source>Interactivity Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="230"/>
        <source>Enable rotation of the 3d box widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="233"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="240"/>
        <source>Enable translation of the 3d box widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="243"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="250"/>
        <source>Enable scaling of the 3d box widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="253"/>
        <source>Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="260"/>
        <source>Enable moving faces of the 3d box widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="263"/>
        <source>Face Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="272"/>
        <source>Reset box using current data bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="275"/>
        <source>Reset Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqBoxPropertyWidget.ui" line="282"/>
        <source>Take account of block visibility</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CameraManipulatorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="22"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="31"/>
        <source>Left Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="41"/>
        <source>Middle Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="51"/>
        <source>Right Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="70"/>
        <source>Shift +</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraManipulatorWidget.ui" line="89"/>
        <source>Ctrl +</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorAnnotationsWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="24"/>
        <source>Add new entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="27"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="41"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="55"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="69"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="138"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="38"/>
        <source>Remove current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="52"/>
        <source>Add active values from selected source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="66"/>
        <source>Add active values from visible pipeline objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="80"/>
        <source>Choose preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="94"/>
        <source>Save to preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="108"/>
        <source>Save to new preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="135"/>
        <source>Delete all annotations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="173"/>
        <source>When checked, the opacity function is used to render translucent surfaces.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorAnnotationsWidget.ui" line="176"/>
        <source>Enable opacity mapping for surfaces</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorEditorPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="22"/>
        <source>Rescale to custom range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="39"/>
        <source>Edit color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="42"/>
        <source>Edit Color Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="45"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="59"/>
        <source>Use Separate color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="62"/>
        <source>Use Separate Color Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="79"/>
        <source>Rescale to data range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="82"/>
        <source>Rescale to Data Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="99"/>
        <source>Show/hide color legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="102"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="153"/>
        <source>Toggle Color Legend Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="122"/>
        <source>Edit color legend properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="136"/>
        <source>Rescale to data range over all timesteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorEditorPropertyWidget.ui" line="150"/>
        <source>Choose preset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorMapEditor</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="28"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="49"/>
        <source>Show/hide color legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="66"/>
        <source>Edit color legend properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="69"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="179"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="105"/>
        <source>Selected Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="112"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="119"/>
        <source>Color Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="126"/>
        <source>Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="133"/>
        <source>Use separate color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="156"/>
        <source>Use a 2D transfer function. Available only for volume rendering.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="176"/>
        <source>Use a separate array to map opacity. Available only for volume rendering.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="209"/>
        <source>Select array used to map color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="216"/>
        <source>Select array used to map to the Y-Axis of a 2D transfer function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="223"/>
        <source>Select array used to map opacity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="256"/>
        <source>Restore application default setting values for color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="273"/>
        <source>Save current color map settings values as default for arrays of this name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="290"/>
        <source>Save current color map settings values as default for all arrays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="317"/>
        <source>Update views.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="334"/>
        <source>Render view(s) automatically.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorMapEditor.ui" line="320"/>
        <source>Render Views</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorOpacityEditorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="22"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="34"/>
        <source>Display Data Histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="44"/>
        <source>Automatically recompute data histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="53"/>
        <source>Number of Bins:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="85"/>
        <source>Select a color map from default presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="94"/>
        <source>Data:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="107"/>
        <source>Set the data value for the selected control point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="151"/>
        <source>Enable Freehand Drawing Of Opacity Transfer Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="158"/>
        <source>Enable Opacity Mapping For Surfaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="168"/>
        <source>Use Log Scale When Mapping Data To Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="175"/>
        <source>Opacity transfer function values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="216"/>
        <source>Select control point and press &quot;Enter&quot; or &quot;Return&quot; to change colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="31"/>
        <source>When checked, a data histogram will be shown below the opacity function. Please update it by clicking on its dedicated button when needed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="41"/>
        <source>When checked, the data histogram will be automatically recomputed when needed instead of needing to click on the compute data histogram button.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="148"/>
        <source>When checked, the control point of the opacity transfer fonction can be &quot;drawn&quot; instead of being placed one by one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="223"/>
        <source>When checked, the mapping from data to colors is done using a log-scale. Note that this does not affect the mapping of data to opacity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="226"/>
        <source>Use Log Scale When Mapping Data To Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="239"/>
        <source>Color transfer function values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="286"/>
        <source>Rescale to data range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="300"/>
        <source>Rescale to custom range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="314"/>
        <source>Rescale to data range over all timesteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="328"/>
        <source>Rescale to visible range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="342"/>
        <source>Invert the transfer functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="356"/>
        <source>Choose preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="370"/>
        <source>Save to preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="384"/>
        <source>Choose box color and alpha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorOpacityEditorWidget.ui" line="398"/>
        <source>Compute data histogram</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="29"/>
        <source>Enable/disable showing the interactive cone widget in the 3d render view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="32"/>
        <source>Show Cone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="47"/>
        <source>Enable/disable the ability to translate the bounding box by moving it with the mouse.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="50"/>
        <source>Outline Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="57"/>
        <source>Enable/disable the ability to scale the widget with the mouse.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="60"/>
        <source>Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="77"/>
        <source>Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="87"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="107"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="124"/>
        <source>Use the X-axis as the cone&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="127"/>
        <source>Along X Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="134"/>
        <source>Use the Y-axis as the cone&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="137"/>
        <source>Along Y Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="144"/>
        <source>Use the Z-axis as the cone&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="147"/>
        <source>Along Z Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="154"/>
        <source>Use the camera&apos;s view direction as the axis of the cone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="157"/>
        <source>Along Camera Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="164"/>
        <source>Reset the camera to look along the cone&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="167"/>
        <source>Reset Camera to Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="174"/>
        <source>Reset the cone&apos;s properties based on the data bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConePropertyWidget.ui" line="177"/>
        <source>Reset to Data Bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomResolutionDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="16"/>
        <source>Custom Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="22"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="34"/>
        <source>width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="41"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="48"/>
        <source>height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="57"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="87"/>
        <source>(optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomResolutionDialog.ui" line="94"/>
        <source>Add custom resolution</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CylinderPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="28"/>
        <source>Enable/disable showing the interactive cylinder widget in the 3d render view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="31"/>
        <source>Show Cylinder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="46"/>
        <source>Enable/disable the ability to translate the bounding box by moving it with the mouse.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="49"/>
        <source>Outline Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="56"/>
        <source>Enable/disable the ability to scale the widget with the mouse.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="59"/>
        <source>Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="76"/>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="86"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="106"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="123"/>
        <source>Use the X-axis as the cylinder&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="126"/>
        <source>Along X Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="133"/>
        <source>Use the Y-axis as the cylinder&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="136"/>
        <source>Along Y Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="143"/>
        <source>Use the Z-axis as the cylinder&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="146"/>
        <source>Along Z Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="153"/>
        <source>Use the camera&apos;s view direction as the axis of the cylinder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="156"/>
        <source>Along Camera Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="163"/>
        <source>Reset the camera to look along the cylinder&apos;s axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="166"/>
        <source>Reset Camera to Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="173"/>
        <source>Reset the cylinder&apos;s properties based on the data bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCylinderPropertyWidget.ui" line="176"/>
        <source>Reset to Data Bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataAssemblyPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="41"/>
        <source>Hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="79"/>
        <source>Selectors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="127"/>
        <source>Add new entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="130"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="147"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="174"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="144"/>
        <source>Remove current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="171"/>
        <source>Remove all entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDataAssemblyPropertyWidget.ui" line="191"/>
        <source>Select active assembly to use.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DoubleRangeSliderPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDoubleRangeSliderPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDoubleRangeSliderPropertyWidget.ui" line="28"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDoubleRangeSliderPropertyWidget.ui" line="41"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDoubleRangeSliderPropertyWidget.ui" line="58"/>
        <source>Reset using current data values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmptyView</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEmptyView.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEmptyView.ui" line="78"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Create View&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileListPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="42"/>
        <source>Property Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="68"/>
        <source>Add new entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="71"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="88"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="102"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="125"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="85"/>
        <source>Remove current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileListPropertyWidget.ui" line="122"/>
        <source>Remove all entries</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindDataWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="22"/>
        <source>Create Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="67"/>
        <source>Find data using selection criteria</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="70"/>
        <source>Find Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="87"/>
        <source>Reset any unaccepted changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="90"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="104"/>
        <source>Clear selection criteria and qualifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="107"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="126"/>
        <source>Selected Data (none)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="154"/>
        <source>Freeze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="164"/>
        <source>Extract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="174"/>
        <source>Plot Over Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFindDataWidget.ui" line="189"/>
        <source>Selection Display</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FontPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="42"/>
        <source>Select font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="55"/>
        <source>Set font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="68"/>
        <source>Set font color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="78"/>
        <source>Set font opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="91"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="94"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="111"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="128"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="142"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="108"/>
        <source>Italics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="125"/>
        <source>Shadow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFontPropertyWidget.ui" line="169"/>
        <source>Specify the path to a TTF file here.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FrustumPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="29"/>
        <source>Enable/disable showing the interactive frustum widget in the 3d render view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="32"/>
        <source>Show Frustum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="50"/>
        <source>Horizontal Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="57"/>
        <source>Vertical Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="64"/>
        <source>Near Plane Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="74"/>
        <source>Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="100"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="117"/>
        <source>Reset the frustum&apos;s properties based on the data bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFrustumPropertyWidget.ui" line="120"/>
        <source>Reset to Data Bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HandlePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="40"/>
        <source>Show Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="53"/>
        <source>Center on Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="66"/>
        <source>Note: Use &apos;P&apos; to pick &apos;%1&apos; on mesh or &apos;Ctrl+P&apos; to snap to the closest mesh point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="79"/>
        <source>Center on Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqHandlePropertyWidget.ui" line="86"/>
        <source>Point</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageCompressorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="25"/>
        <source>Set the compression method used when transferring rendered images from the server to the client.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="36"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="41"/>
        <source>LZ4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="46"/>
        <source>Squirt (run-length encoding based compression)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="51"/>
        <source>Zlib</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="59"/>
        <source>Set the Squirt/LZ4 compression level. Move to right for better compression ratio at the cost of reduced image quality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="82"/>
        <source>Set the Zlib compression level. 1 is the fastest compression possible at the cost of compression ratio, while 9 give the best compression possible, but may be slower.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="105"/>
        <source>Set the Zlib the color sampling space width factor. Move to right for better compression ratio at the cost of image quality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="128"/>
        <source>Set whether to strip alpha channel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="135"/>
        <source>Set the NvPipe compression level. 1 is high image quality; 5 is low image quality. Even at the highest setting the bandwidth cost is well below LZ4 or Squirt.
      </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="161"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="215"/>
        <source>Configure the compressor using default settings tailored to a selected connection type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="164"/>
        <source>Apply presets for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="174"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Presets. &lt;/span&gt;Configure the image compressor based on a network type. These settings will provide reasonable performance and place to start when optimizing the compressor setting for a specific network.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="182"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="187"/>
        <source>consumer broadband/DSL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="192"/>
        <source>Megabit Ethernet / 802.11* wireless</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="197"/>
        <source>Gigabit Ethernet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="202"/>
        <source>10 Gigabit Ethernet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="207"/>
        <source>shared memory/localhost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImageCompressorWidget.ui" line="218"/>
        <source>connection.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImplicitPlanePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="28"/>
        <source>Show the interactive plane in the 3D render view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="31"/>
        <source>Show Plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="49"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="59"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="89"/>
        <source>Note: Use &apos;P&apos; to pick &apos;%1&apos; on mesh or &apos;Ctrl+P&apos; to snap to the closest mesh point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="107"/>
        <source>Set the normal to the camera&apos;s view direction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="110"/>
        <source>Camera Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="117"/>
        <source>Set the normal to the x-axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="120"/>
        <source>&amp;X Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="127"/>
        <source>Set the normal to the z-axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="130"/>
        <source>&amp;Z Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="137"/>
        <source>Set the normal to the y-axis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="140"/>
        <source>&amp;Y Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="147"/>
        <source>Reset the plane based on the data bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="150"/>
        <source>Reset to Data Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="157"/>
        <source>Reset the camera to look along the normal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqImplicitPlanePropertyWidget.ui" line="160"/>
        <source>Reset Camera to Normal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LightPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="34"/>
        <source>Focal Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="47"/>
        <source>Light Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="60"/>
        <source>Show Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="70"/>
        <source>Cone Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightPropertyWidget.ui" line="89"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="28"/>
        <source>Point 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="47"/>
        <source>Point 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="66"/>
        <source>Flip Vector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="79"/>
        <source>Note: Use &apos;P&apos; to place alternating points on mesh or &apos;Ctrl+P&apos; to snap to the closest mesh point.
Use &apos;1&apos;/&apos;Ctrl+1&apos; for point 1 and &apos;2&apos;/&apos;Ctrl+2&apos; for point 2.
Use &apos;N&apos; to place P1 on the mesh and make P1-P2 be the normal at the surface.
Use &apos;X&apos;/&apos;Y&apos;/&apos;Z&apos;/&apos;L&apos; to constrain the movement to the X / Y / Z / Line axis respectively.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="100"/>
        <source>X Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="107"/>
        <source>Y Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="114"/>
        <source>Z Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="123"/>
        <source>Center on Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="130"/>
        <source>Show Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLinePropertyWidget.ui" line="140"/>
        <source>Length: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="16"/>
        <source>ParaView - The Beast  UNLEASHED !!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="30"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="36"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="46"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="49"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqDefaultMainWindow.ui" line="54"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoleculePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="22"/>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="32"/>
        <source>Show Atoms </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="49"/>
        <source>Atom Radius type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="59"/>
        <source>Atom Radius Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="75"/>
        <source>Atomic Radius Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="92"/>
        <source>Show Bonds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="109"/>
        <source>Bonds Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="125"/>
        <source>Use Multi Cylinders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="142"/>
        <source>Use Atom Color </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMoleculePropertyWidget.ui" line="159"/>
        <source>Bonds color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OMETransferFunctionsPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OMETransferFunctionsPropertyWidgetPage</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidgetPage.ui" line="22"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidgetPage.ui" line="105"/>
        <source>Invert the transfer functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidgetPage.ui" line="119"/>
        <source>Choose preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqOMETransferFunctionsPropertyWidgetPage.ui" line="139"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PauseLiveSourcePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPauseLiveSourcePropertyWidget.ui" line="34"/>
        <source>Pause Live Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPauseLiveSourcePropertyWidget.ui" line="44"/>
        <source>Emulated Time Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPauseLiveSourcePropertyWidget.ui" line="63"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPauseLiveSourcePropertyWidget.ui" line="77"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPauseLiveSourcePropertyWidget.ui" line="74"/>
        <source>Reset time of all emulated time sources.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyCollectionWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="47"/>
        <source>Group Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="59"/>
        <source>Append new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="62"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="76"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqPropertyCollectionWidget.ui" line="73"/>
        <source>Remove all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPropertyCollectionWidget.cxx" line="108"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyLinksConnection</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUseSeparateColorMapReaction.cxx" line="58"/>
        <source>Change </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickLaunchDialogExtended</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="16"/>
        <source>Quick Launch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="22"/>
        <source>Type to search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="29"/>
        <source>SelectedElement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="32"/>
        <source>Create the selected element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="46"/>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="49"/>
        <source>Open the Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="60"/>
        <source>Short help for selected element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="83"/>
        <source>Requirements (if unmet)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="99"/>
        <source>Unavailable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="102"/>
        <source>Select one to see its unmet requirements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="112"/>
        <source>Unavailable: select one to see its unmet requirements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqQuickLaunchDialogExtended.ui" line="119"/>
        <source>Press Enter to create, Shift+Enter to create and apply.
Esc to cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResetScalarRangeToDataOverTime</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="16"/>
        <source>Rescale range over time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="25"/>
        <source>Determining range over all timesteps can potentially take a long time to complete. Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="59"/>
        <source>Rescale and lock the color map to avoid automatic rescaling.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="62"/>
        <source>Rescale and disable automatic rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="72"/>
        <source>Rescale and leave automatic rescaling mode unchanged.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="75"/>
        <source>Rescale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="82"/>
        <source>Close without rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqResetScalarRangeToDataOverTime.ui" line="85"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SavePresetOptions</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="16"/>
        <source>Save Preset Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="25"/>
        <source>Save colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="35"/>
        <source>Save opacities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="22"/>
        <source>Check to save colors to the preset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="32"/>
        <source>Check to save opacities to the preset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="42"/>
        <source>Check to save annotations to the preset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="45"/>
        <source>Save annotations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="54"/>
        <source>Preset name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="61"/>
        <source>Set a name to the preset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSavePresetOptions.ui" line="64"/>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionEditor</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="22"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="39"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="49"/>
        <source>The dataset for which selections are saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="42"/>
        <source>Data Producer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="78"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="88"/>
        <source>The element type of the saved selections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="81"/>
        <source>Element Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="111"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="121"/>
        <source>Specify the expression which defines the relation between the saved selections using boolean operators: !(NOT), &amp;(AND), |(OR), ^(XOR) and ().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="114"/>
        <source>Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="171"/>
        <source>Add active selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="188"/>
        <source>Remove selected selection from the saved selections. Remember to edit the Expression.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="218"/>
        <source>Remove all saved selections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="246"/>
        <source>Set the combined saved selections as the active selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionEditor.ui" line="249"/>
        <source>Activate Combined Selections</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SeriesEditorPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="34"/>
        <source>Enter the thickness for the line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="47"/>
        <source>Line Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="63"/>
        <source>Select the line style for the series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="67"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="124"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="72"/>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="77"/>
        <source>Dash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="82"/>
        <source>Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="87"/>
        <source>Dash Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="92"/>
        <source>Dash Dot Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="100"/>
        <source>Line Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="107"/>
        <source>Marker Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="129"/>
        <source>Cross</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="134"/>
        <source>Plus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="139"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="144"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="149"/>
        <source>Diamond</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="166"/>
        <source>Enter the size for the marker.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="182"/>
        <source>Marker Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="189"/>
        <source>Chart Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="205"/>
        <source>Select the chart axes for the line series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="209"/>
        <source>Bottom-Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="214"/>
        <source>Bottom-Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="219"/>
        <source>Top-Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSeriesEditorPropertyWidget.ui" line="224"/>
        <source>Top-Left</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpherePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="28"/>
        <source>Enable/disable showing the interactive sphere widget in the 3D render view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="31"/>
        <source>Show Sphere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="41"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="57"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="73"/>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="89"/>
        <source>Note: Use &apos;P&apos; to a &apos;%1&apos; on mesh or &apos;Ctrl+P&apos; to snap to the closest mesh point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpherePropertyWidget.ui" line="102"/>
        <source>Center on Bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SplinePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="37"/>
        <source>Show Spline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="67"/>
        <source>Add new point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="70"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="84"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="111"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="81"/>
        <source>Remove selected point(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="108"/>
        <source>Remove all points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="126"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; to place selected point on mesh or &lt;span style=&quot; font-weight:600;&quot;&gt;Ctrl+P&lt;/span&gt; to snap the selected point to closest mesh point. Use &lt;span style=&quot; font-weight:600;&quot;&gt;1 &lt;/span&gt;/ &lt;span style=&quot; font-weight:600;&quot;&gt;Ctrl+1&lt;/span&gt; for first point and &lt;span style=&quot; font-weight:600;&quot;&gt;2 &lt;/span&gt;/ &lt;span style=&quot; font-weight:600;&quot;&gt;Ctrl+2&lt;/span&gt; for the last point.&lt;br/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Click&lt;/span&gt; to select a point, &lt;span style=&quot; font-weight:600;&quot;&gt;Shift+Click&lt;/span&gt; to remove a point, &lt;span style=&quot; font-weight:600;&quot;&gt;Ctrl+Click&lt;/span&gt; to insert a point on the line, &lt;span style=&quot; font-weight:600;&quot;&gt;Alt+Click&lt;/span&gt; to add a point after the selected extremity.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSplinePropertyWidget.ui" line="136"/>
        <source>Closed Spline</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextLocationWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="22"/>
        <source>Coordinates of the bottom left corner of the text object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="25"/>
        <source>Use Coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="65"/>
        <source>Top Left Corner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="103"/>
        <source>Bottom Right Corner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="138"/>
        <source>Bottom Left Corner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="173"/>
        <source>Top Right Corner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="208"/>
        <source>Top Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="243"/>
        <source>Bottom Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="281"/>
        <source>Coordinates of the lower left corner of the text object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTextLocationWidget.ui" line="311"/>
        <source>Use Window Location</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimeManagerWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeManagerWidget.ui" line="14"/>
        <source>TimeManager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeManagerWidget.ui" line="59"/>
        <source>Number of frames to use when no &quot;Time Sources&quot; are checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeManagerWidget.ui" line="83"/>
        <source>Number of frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeManagerWidget.ui" line="124"/>
        <source>Stride</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTimeManagerWidget.ui" line="160"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransferFunctionWidgetPropertyDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTransferFunctionWidgetPropertyDialog.ui" line="16"/>
        <source>Edit Transfer Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqTransferFunctionWidgetPropertyDialog.ui" line="22"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewResolutionPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="28"/>
        <source>width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="35"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="42"/>
        <source>height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="49"/>
        <source>Select resolution from presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="52"/>
        <source>Presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="63"/>
        <source>Use previous resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="86"/>
        <source>Lock aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqViewResolutionPropertyWidget.ui" line="106"/>
        <source>Reset to default based on current values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>YoungsMaterialPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqYoungsMaterialPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqYoungsMaterialPropertyWidget.ui" line="28"/>
        <source>Ordering Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqYoungsMaterialPropertyWidget.ui" line="35"/>
        <source>Normal Array</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnglePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="80"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="84"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="96"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="100"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="111"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="115"/>
        <source>Ctrl+2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnglePropertyWidget.cxx" line="157"/>
        <source>Angle: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimatedExportReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimatedExportReaction.cxx" line="59"/>
        <source>Export Animated Scene ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimatedExportReaction.cxx" line="88"/>
        <source>Export animated scene progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimatedExportReaction.cxx" line="88"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimatedExportReaction.cxx" line="90"/>
        <source>Saving animated scene ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationShortcutWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="88"/>
        <source>Create a new animation track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="96"/>
        <source>Edit the animation track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="100"/>
        <source>Remove the animation track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="118"/>
        <source>Remove Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="129"/>
        <source>Add Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="144"/>
        <source>Animation Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="154"/>
        <source>No. frames:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationShortcutWidget.cxx" line="160"/>
        <source>Duration (s):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationTimeToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAnimationTimeToolbar.cxx" line="17"/>
        <source>Current Time Controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqApplyBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqApplyBehavior.cxx" line="259"/>
        <source>update the view to ensure updated data information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAutoSaveBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAutoSaveBehavior.cxx" line="111"/>
        <source>AutoSave directory was not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAutoSaveBehavior.cxx" line="114"/>
        <source>Cannot find the autosave directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAutoSaveBehavior.cxx" line="116"/>
        <source>AutoSave will not work until an existing directory is provided.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAxesToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="11"/>
        <source>Center Axes Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="34"/>
        <source>Show Orientation Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="37"/>
        <source>Show/Hide orientation axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="52"/>
        <source>Show Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="55"/>
        <source>Show/Hide center of rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="69"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="72"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="75"/>
        <source>Pick Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="78"/>
        <source>Mouse press to pick center of rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="89"/>
        <source>Reset Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqAxesToolbar.ui" line="92"/>
        <source>Reset center of rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="112"/>
        <source> Show orientation axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="112"/>
        <source> Hide orientation axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="128"/>
        <source> Show center axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="128"/>
        <source> Hide center axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="146"/>
        <location filename="../Qt/ApplicationComponents/pqAxesToolbar.cxx" line="172"/>
        <source> update center of rotation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqBackgroundEditorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBackgroundEditorWidget.cxx" line="212"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBackgroundEditorWidget.cxx" line="212"/>
        <source>Color 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBackgroundEditorWidget.cxx" line="338"/>
        <source>Restore Default Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqBlockContextMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="68"/>
        <source>Block &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="69"/>
        <source>%1 Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="80"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="158"/>
        <source>Hide Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="166"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="169"/>
        <source>Show Only Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="176"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="179"/>
        <source>Show All Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="190"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="203"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="209"/>
        <source>Set Block Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="219"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="221"/>
        <source>Unset Block Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="236"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="239"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="246"/>
        <source>Set Block Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="237"/>
        <source>Opacity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="258"/>
        <location filename="../Qt/ApplicationComponents/pqBlockContextMenu.cxx" line="260"/>
        <source>Unset Block Opacity</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCameraToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="16"/>
        <source>Camera Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="30"/>
        <source>&amp;Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="33"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="36"/>
        <source>Reset Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="48"/>
        <source>+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="51"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="54"/>
        <source>Set view direction to +X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="66"/>
        <source>-X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="69"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="72"/>
        <source>Set view direction to -X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="84"/>
        <source>+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="87"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="90"/>
        <source>Set view direction to +Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="102"/>
        <source>-Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="105"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="108"/>
        <source>Set view direction to -Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="120"/>
        <source>+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="123"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="126"/>
        <source>Set view direction to +Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="138"/>
        <source>-Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="141"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="144"/>
        <source>Set view direction to -Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="156"/>
        <source>&amp;Isometric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="159"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="162"/>
        <source>Apply isometric view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="177"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="180"/>
        <source>Rotate 90° clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="195"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="198"/>
        <source>Rotate 90° counterclockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="210"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="213"/>
        <source>Zoom to Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="218"/>
        <source>ZTD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="221"/>
        <source>Zoom To Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="230"/>
        <source>ZCTD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="233"/>
        <source>Zoom Closest To Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="242"/>
        <source>RCC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCameraToolbar.ui" line="245"/>
        <source>Reset Camera Closest</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCatalystConnectReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystConnectReaction.cxx" line="108"/>
        <source>Connect...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystConnectReaction.cxx" line="113"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCatalystExportReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="33"/>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="34"/>
        <source>Needs Python support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="46"/>
        <source>Catalyst state file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="50"/>
        <source>Save Catalyst State:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="48"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCatalystExportReaction.cxx" line="78"/>
        <source>Save Catalyst State Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCatalystMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="880"/>
        <source>Connect...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="883"/>
        <source>Pause Simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="887"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="891"/>
        <source>Set Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="895"/>
        <source>Remove Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqChangeFileNameReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqChangeFileNameReaction.cxx" line="101"/>
        <location filename="../Qt/ApplicationComponents/pqChangeFileNameReaction.cxx" line="108"/>
        <source>Change File operation aborted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqChangeFileNameReaction.cxx" line="103"/>
        <source>No reader associated to the selected source was found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqChangeFileNameReaction.cxx" line="110"/>
        <source>More than one reader associated to the selected source were found!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqChangePipelineInputReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqChangePipelineInputReaction.cxx" line="67"/>
        <source>Change Input for %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqChooseColorPresetReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqChooseColorPresetReaction.cxx" line="201"/>
        <source>Apply color preset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorAnnotationsPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsPropertyWidget.cxx" line="178"/>
        <source>Could not initialize annotations for categorical coloring. There may be too many discrete values in your data, (more than %1) or you may be coloring by a floating point data array. Please add annotations manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsPropertyWidget.cxx" line="184"/>
        <source>Could not determine discrete values to use for annotations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorAnnotationsSelectionHelper</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="273"/>
        <source>Set opacity of highlited items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="298"/>
        <source>Set opacity of highlighted items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorAnnotationsWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="576"/>
        <source>Set global opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="597"/>
        <source>Choose Annotation Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="606"/>
        <source>Opacity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="607"/>
        <source>Select Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="788"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="879"/>
        <source>Could not determine discrete values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="791"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="882"/>
        <source>Could not automatically determine annotation values. Usually this means too many discrete values (more than %1) are available in the data produced by the current source/filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="796"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="887"/>
        <source> This can happen if the data array type is floating point. Please add annotations manually or force generation. Forcing the generation will automatically hide the Scalar Bar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="802"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="894"/>
        <source>Force</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="813"/>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="905"/>
        <source>Could not force generation of discrete values using the data produced by the current source/filter. Please add annotations manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="1040"/>
        <source>Save %1s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="1047"/>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorMapEditor</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorMapEditor.cxx" line="160"/>
        <source>Representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorMapEditor.cxx" line="161"/>
        <source>Block(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorMapEditor.cxx" line="402"/>
        <source>Color X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorMapEditor.cxx" line="406"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorMapEditor.cxx" line="559"/>
        <source>Reset </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorMapEditor.cxx" line="562"/>
        <source> to defaults</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorOpacityEditorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorOpacityEditorWidget.cxx" line="1062"/>
        <source>Reset transfer function ranges using visible data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorOpacityEditorWidget.cxx" line="1097"/>
        <source>Invert transfer function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorOpacityEditorWidget.cxx" line="1135"/>
        <source>Select a color map from default presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorOpacityEditorWidget.cxx" line="1555"/>
        <source>Choose box color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorPaletteSelectorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorPaletteSelectorWidget.cxx" line="75"/>
        <source>Select palette to load ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorPaletteSelectorWidget.cxx" line="79"/>
        <source>No palettes available.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="8"/>
        <source>Active Variable Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="30"/>
        <source>Show Color Legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="33"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="36"/>
        <source>Toggle Color Legend Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="50"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="53"/>
        <source>Edit Color Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="64"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="67"/>
        <source>Use Separate Color Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="78"/>
        <source>Reset Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="81"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="84"/>
        <source>Rescale to Data Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="95"/>
        <source>Rescale Custom Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="98"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="101"/>
        <source>Rescale to Custom Data Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="113"/>
        <source>Rescale Temporal Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="116"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="119"/>
        <source>Rescale to data range over all timesteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="130"/>
        <source>Rescale Visible Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="133"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqColorToolbar.ui" line="136"/>
        <source>Rescale to Visible Data Range</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCommandLineOptionsBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCommandLineOptionsBehavior.cxx" line="174"/>
        <source>Internal Open File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqConfigureCategoriesDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="16"/>
        <source>Configure Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="28"/>
        <source>Available Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="81"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="255"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="102"/>
        <source>Current Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="111"/>
        <source>Add Filter to custom menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="114"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="128"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="142"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="156"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="199"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="213"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="125"/>
        <source>Set an icon for the current proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="139"/>
        <source>Create a category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="153"/>
        <source>Insert a subcategory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="170"/>
        <source>Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="173"/>
        <source>Add a toolbar for this category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="196"/>
        <source>Delete selected entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqConfigureCategoriesDialog.ui" line="210"/>
        <source>Reset categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqConfigureCategoriesDialog.cxx" line="187"/>
        <source>Filter is not available.
It may be part of a plugin that is not loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqConfigureCategoriesDialog.cxx" line="216"/>
        <location filename="../Qt/ApplicationComponents/pqConfigureCategoriesDialog.cxx" line="218"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCoordinateFramePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="210"/>
        <source>Coordinate frame utilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="229"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="235"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="242"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="248"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="255"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCoordinateFramePropertyWidget.cxx" line="261"/>
        <source>Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCopyReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCopyReaction.cxx" line="448"/>
        <source>Copy Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCrashRecoveryBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="51"/>
        <source>ParaView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="52"/>
        <source>A crash recovery state file has been found.
Would you like to save it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="57"/>
        <source>ParaView state file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="57"/>
        <source>All files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="59"/>
        <source>Save crash state file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="139"/>
        <source>Server disconnected!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="140"/>
        <source>The server side has disconnected. The application will now quit since it may be in an unrecoverable state.

Would you like to save a ParaView state file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="144"/>
        <source>Save state and exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCrashRecoveryBehavior.cxx" line="145"/>
        <source>Exit without saving state</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCreateCustomFilterReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCreateCustomFilterReaction.cxx" line="53"/>
        <source>Create Custom Filter Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCreateCustomFilterReaction.cxx" line="54"/>
        <source>The selected objects cannot be used to make a custom filter.
To create a new custom filter, select the sources and filters you want.
Then, launch the creation wizard.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointsToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCustomViewpointsToolbar.cxx" line="41"/>
        <source>Custom Viewpoints Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCustomViewpointsToolbar.cxx" line="66"/>
        <source>Configure custom viewpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCustomViewpointsToolbar.cxx" line="73"/>
        <source>Add current viewpoint as custom viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomizeShortcutsDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="22"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="75"/>
        <source>To set the shortcut for the selected action, press this then enter the shortcut.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="78"/>
        <source>Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="91"/>
        <source>Remove the shortcut for the selected action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="94"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="101"/>
        <source>Restore the default shortcut for the selected action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="104"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="121"/>
        <source>Resets the shortcuts for all actions to the default or clears the shortcut if there is no default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqCustomizeShortcutsDialog.ui" line="124"/>
        <source>Reset All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqCustomizeShortcutsDialog.cxx" line="417"/>
        <source>Customize Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDefaultContextMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="96"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="99"/>
        <source>Representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="122"/>
        <source>Color By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="127"/>
        <source>Edit Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="139"/>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="225"/>
        <source>Show All Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="145"/>
        <source>Link Camera...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="146"/>
        <source>Unlink Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="151"/>
        <source>Show Frame Decorations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="171"/>
        <source>Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="240"/>
        <source>Change coloring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="292"/>
        <source>Representation Type Changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultContextMenu.cxx" line="308"/>
        <source>Visibility Changed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDefaultViewBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="66"/>
        <source>Your OpenGL drivers don&apos;t support
required OpenGL features for basic rendering.
Application cannot continue. Please exit and use an older version.

CONTINUE AT YOUR OWN RISK!

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="72"/>
        <source>OpenGL support inadequate!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="84"/>
        <source>Server DISPLAY not accessible!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="85"/>
        <source>Display is not accessible on the server side.
Remote rendering will be disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="91"/>
        <source>OpenGL drivers on the server side don&apos;t support
required OpenGL features for basic rendering.
Remote rendering will be disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="96"/>
        <source>Server OpenGL support inadequate!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="157"/>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="166"/>
        <source>Server Timeout Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="158"/>
        <source>The server connection will timeout under 5 minutes.
Please save your work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDefaultViewBehavior.cxx" line="167"/>
        <source>The server connection will timeout shortly.
Please save your work.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDeleteReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDeleteReaction.cxx" line="312"/>
        <source>Delete %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDeleteReaction.cxx" line="316"/>
        <source>Delete Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDeleteReaction.cxx" line="437"/>
        <source>Delete All?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDeleteReaction.cxx" line="438"/>
        <source>The current visualization will be reset
and the state will be discarded.

Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDesktopServicesReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDesktopServicesReaction.cxx" line="35"/>
        <source>The requested file is not available in your installation. You can manually obtain and place the file (or ask your administrators) at the following location for this to work.

&apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDesktopServicesReaction.cxx" line="41"/>
        <source>Missing file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDisplaySizedImplicitPlanePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="72"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="139"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="145"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="152"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqDisplaySizedImplicitPlanePropertyWidget.cxx" line="158"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditCameraReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditCameraReaction.cxx" line="54"/>
        <source>Adjust Camera</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditColorMapReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditColorMapReaction.cxx" line="104"/>
        <source>Block </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditColorMapReaction.cxx" line="113"/>
        <source>Pick </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditColorMapReaction.cxx" line="113"/>
        <location filename="../Qt/ApplicationComponents/pqEditColorMapReaction.cxx" line="120"/>
        <source>Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditColorMapReaction.cxx" line="120"/>
        <source>Change </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditMacrosDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="16"/>
        <source>Edit Macros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="104"/>
        <source>Import new macro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="107"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="121"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="135"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="149"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="176"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="132"/>
        <source>Open macro in a new tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="118"/>
        <source>Remove selected macros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="146"/>
        <source>Set an icon to the macro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="173"/>
        <source>Remove all macros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMacrosDialog.ui" line="84"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditMacrosDialog.cxx" line="400"/>
        <source>... (%1 more)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditMacrosDialog.cxx" line="495"/>
        <source>Open Python File to create a Macro:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditMacrosDialog.cxx" line="496"/>
        <source>Python Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditMacrosDialog.cxx" line="496"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="223"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="227"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditMenuBuilder</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="8"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="22"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="25"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="37"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="40"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="49"/>
        <source>Camera Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="52"/>
        <source>Ctrl+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="61"/>
        <source>Camera Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="64"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="69"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="72"/>
        <source>Change File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="75"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="78"/>
        <source>Change File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="87"/>
        <source>Change &amp;Input...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="90"/>
        <source>Change Input...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="93"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="96"/>
        <source>Change a Filter&apos;s Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="101"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="104"/>
        <source>Rename...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="107"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="110"/>
        <source>Rename selected source (or filter)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="119"/>
        <source>&amp;Copy Screenshot to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="122"/>
        <source>Copy Screenshot to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="131"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="134"/>
        <source>Copy properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="143"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="146"/>
        <source>Paste copied properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="155"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="158"/>
        <source>Copy Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="167"/>
        <source>Paste Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="170"/>
        <source>Paste copied pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="179"/>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="182"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="191"/>
        <source>Delete Downstream Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="194"/>
        <source>Delete selection and all downstream filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="203"/>
        <source>Reset Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="206"/>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="215"/>
        <source>Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="218"/>
        <source>Show all sources in selected view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="227"/>
        <source>Hide All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="235"/>
        <source>Ignore Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="238"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="241"/>
        <source>Disregard this source/filter&apos;s time from animations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="250"/>
        <source>Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="253"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="256"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="265"/>
        <source>Find Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="268"/>
        <source>Find Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="271"/>
        <source>Find data matching various criteria from the current source.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="274"/>
        <source>V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="279"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="282"/>
        <source>Search in item list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="285"/>
        <source>Pops up a search dialog if focus is on an item list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="288"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="297"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="300"/>
        <source>Reset to Default Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="303"/>
        <source>Reset user settings to default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="312"/>
        <source>Rename Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqEditMenuBuilder.ui" line="315"/>
        <source>Change the main window title</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEditScalarBarReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditScalarBarReaction.cxx" line="95"/>
        <source>Edit Color Legend Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqEqualizerPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="64"/>
        <source>Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="67"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="68"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="69"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="157"/>
        <source>Save Equalizer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEqualizerPropertyWidget.cxx" line="182"/>
        <source>Load Equalizer:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExampleVisualizationsDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="16"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="70"/>
        <source>ParaView Example Visualizations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="22"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Click on one of thumbnails below to load an example visualization&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="49"/>
        <source>Data analysis of hot gas from Exodus II file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="80"/>
        <source>Wavelet with volume rendering and contours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="93"/>
        <source>Contouring CT scan of a head</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sea surface temperatures&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="129"/>
        <source>Exodus II file, Clip filter, Stream Tracer filter, Tube filter, Glyph filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqExampleVisualizationsDialog.ui" line="204"/>
        <source>Exodus II file with timesteps, Clip filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExampleVisualizationsDialog.cxx" line="97"/>
        <source>Your installation doesn&apos;t have datasets to load the example visualizations. You can manually download the datasets from paraview.org and then place them under the following path for examples to work:

&apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExampleVisualizationsDialog.cxx" line="103"/>
        <source>Missing data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExampleVisualizationsDialog.cxx" line="116"/>
        <source>Loading example visualization, please wait ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExportReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExportReaction.cxx" line="108"/>
        <source>Export View:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExportReaction.cxx" line="130"/>
        <source>Export Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExtractorsMenuReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExtractorsMenuReaction.cxx" line="109"/>
        <location filename="../Qt/ApplicationComponents/pqExtractorsMenuReaction.cxx" line="139"/>
        <source>Requires an input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqExtractorsMenuReaction.cxx" line="177"/>
        <source>Create Extract Generator &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFileListPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFileListPropertyWidget.cxx" line="47"/>
        <source>Select %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFileMenuBuilder</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="8"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="16"/>
        <source>&amp;Recent Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="25"/>
        <source>&amp;Connect...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="28"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="37"/>
        <source>&amp;Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="40"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="49"/>
        <source>Save Screenshot...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="58"/>
        <source>Save &amp;Animation...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="70"/>
        <source>Save Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="73"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="85"/>
        <source>&amp;Load State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="97"/>
        <source>&amp;Save State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="106"/>
        <source>Save Catalyst State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="111"/>
        <source>Save &amp;Geometry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="123"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="126"/>
        <source>Reload Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="129"/>
        <source>Reload data files in case they were changed externally.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="132"/>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="141"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="144"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="147"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="159"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="162"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="165"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="174"/>
        <source>Import Scene...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="183"/>
        <source>Export Scene...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="192"/>
        <source>Export Animated Scene...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="195"/>
        <source>Save scene for all timesteps by playing the animation step by step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="198"/>
        <source>Save scene for all timesteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="207"/>
        <source>Save Window Arrangement...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="210"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="213"/>
        <source>Save window arrangement to a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="222"/>
        <source>Load Window Arrangement...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="225"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="228"/>
        <source>Load window arrangement from a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="233"/>
        <source>Load Path Tracer Materials...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="236"/>
        <source>Load a Materials file that contains appearance settings for use in high quality rendering modes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="239"/>
        <source>Load an Appearance Materials file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqFileMenuBuilder.ui" line="248"/>
        <source>Save Extracts...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFileNamePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFileNamePropertyWidget.cxx" line="63"/>
        <source>Reset using current data values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFiltersMenuReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFiltersMenuReaction.cxx" line="217"/>
        <source>Create &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFindDataWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFindDataWidget.cxx" line="179"/>
        <source>Convert Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFindDataWidget.cxx" line="180"/>
        <source>This selection conversion can potentially result in fetching a large amount of data to the client.
Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFindDataWidget.cxx" line="314"/>
        <source>Selected Data (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFindDataWidget.cxx" line="318"/>
        <source>Selected Data (none)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFontPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="233"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="236"/>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="260"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="239"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="257"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqFontPropertyWidget.cxx" line="263"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqHandlePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqHandlePropertyWidget.cxx" line="39"/>
        <source>Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqHandlePropertyWidget.cxx" line="72"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqHandlePropertyWidget.cxx" line="78"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqHelpMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="680"/>
        <source>Getting Started with ParaView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="693"/>
        <source>ParaView Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="712"/>
        <source>Reader, Filter, and Writer Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="726"/>
        <source>ParaView Self-directed Tutorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="734"/>
        <source>ParaView Classroom Tutorials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="741"/>
        <source>Example Visualizations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="749"/>
        <source>ParaView Web Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="754"/>
        <source>ParaView Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="759"/>
        <source>ParaView Community Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="766"/>
        <source>Release Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="774"/>
        <source>Professional Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="779"/>
        <source>Professional Training</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="784"/>
        <source>Online Tutorials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="789"/>
        <source>Online Blogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="805"/>
        <source>Bug Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="809"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqHelpReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqHelpReaction.cxx" line="49"/>
        <source>%1 Online Help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqIgnoreSourceTimeReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqIgnoreSourceTimeReaction.cxx" line="60"/>
        <source>Toggle Ignore Time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqImageCompressorWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImageCompressorWidget.cxx" line="46"/>
        <source>Image Compression</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqImplicitPlanePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImplicitPlanePropertyWidget.cxx" line="72"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImplicitPlanePropertyWidget.cxx" line="124"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImplicitPlanePropertyWidget.cxx" line="130"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqImportReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImportReaction.cxx" line="47"/>
        <source>Import:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqImportReaction.cxx" line="71"/>
        <source>Import Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLanguageChooserWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLanguageChooserWidget.cxx" line="72"/>
        <source>Setting overriden by environment variable PV_TRANSLATIONS_LOCALE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLightToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightToolbar.ui" line="11"/>
        <source>Light controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqLightToolbar.ui" line="34"/>
        <source>Enable/Disable Light Kit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLinePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="87"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="93"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="98"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="104"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="109"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="115"/>
        <source>Ctrl+2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="121"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLinePropertyWidget.cxx" line="144"/>
        <source>Length: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLoadDataReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadDataReaction.cxx" line="169"/>
        <source>Open File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadDataReaction.cxx" line="439"/>
        <source>Create &apos;Reader&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLoadMaterialsReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadMaterialsReaction.cxx" line="33"/>
        <source>Load Materials:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadMaterialsReaction.cxx" line="34"/>
        <source>OSPRay Material Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadMaterialsReaction.cxx" line="34"/>
        <source>Wavefront Material Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLoadPaletteReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadPaletteReaction.cxx" line="109"/>
        <source>Edit Current Palette ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadPaletteReaction.cxx" line="126"/>
        <source>Load color palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadPaletteReaction.cxx" line="142"/>
        <source>Color Palette</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLoadStateReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadStateReaction.cxx" line="80"/>
        <source>Load State Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqLoadStateReaction.cxx" line="131"/>
        <source>Load State File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMacroReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMacroReaction.cxx" line="33"/>
        <source>Open Python File to create a Macro:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMacroReaction.cxx" line="34"/>
        <source>Python Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMacroReaction.cxx" line="34"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMacrosMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="381"/>
        <source>Manage Macros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="657"/>
        <source>Import new macro...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="661"/>
        <source>Edit Macros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditMacrosDialog.cxx" line="562"/>
        <source>Selected macros will be deleted: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditMacrosDialog.cxx" line="566"/>
        <source>Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditMacrosDialog.cxx" line="570"/>
        <source>Delete Macro(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditMacrosDialog.cxx" line="586"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqEditMacrosDialog.cxx" line="587"/>
        <source>All macros will be deleted. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMainControlsToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="16"/>
        <source>Main Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="27"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="30"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="42"/>
        <source>Save Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="54"/>
        <source>&amp;Connect...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="57"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="66"/>
        <source>&amp;Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="69"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="78"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="87"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="99"/>
        <source>Auto Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="102"/>
        <source>Apply changes to parameters automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="117"/>
        <source>Find Data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="120"/>
        <source>Find data matching various criteria from the current source (v)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="129"/>
        <source>Load Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="132"/>
        <source>Load a color palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="141"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="144"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="147"/>
        <source>Reset Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="156"/>
        <source>Save Extracts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="159"/>
        <source>Save Extracts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="168"/>
        <source>Save State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="171"/>
        <source>Save State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="180"/>
        <source>Save Catalyst State...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqMainControlsToolbar.ui" line="183"/>
        <source>Save Catalyst State</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMainWindowEventBehavior</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMainWindowEventBehavior.cxx" line="95"/>
        <source>Read PNG or state file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMainWindowEventBehavior.cxx" line="96"/>
        <source>This PNG file has a ParaView state file embedded.
Do you want to open this file as a state file?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqManageLinksReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqManageLinksReaction.cxx" line="13"/>
        <source>Link Manager</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMoleculePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMoleculePropertyWidget.cxx" line="60"/>
        <location filename="../Qt/ApplicationComponents/pqMoleculePropertyWidget.cxx" line="119"/>
        <source>Reset the range values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqMoleculePropertyWidget.cxx" line="98"/>
        <source>Apply a preset to display properties, including advanced ones.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqOpacityRangeDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqColorAnnotationsWidget.cxx" line="238"/>
        <source>Opacity value :</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPauseLiveSourcePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPauseLiveSourcePropertyWidget.cxx" line="29"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPauseLiveSourcePropertyWidget.cxx" line="29"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPauseLiveSourcePropertyWidget.cxx" line="30"/>
        <source> emulated time sources.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPipelineBrowserContextMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="412"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="415"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="419"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="429"/>
        <source>&amp;Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="431"/>
        <source>Show all source outputs in the pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="442"/>
        <source>&amp;Hide All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="444"/>
        <source>Hide all source outputs in the pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="454"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="457"/>
        <source>Copy Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="467"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="470"/>
        <source>Paste Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="477"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="480"/>
        <source>Copy Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="487"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="490"/>
        <source>Paste Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="496"/>
        <source>Change &amp;Input...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="498"/>
        <source>Change Input...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="500"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="504"/>
        <source>Change a Filter&apos;s Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="511"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="513"/>
        <source>Reload Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="515"/>
        <source>Reload data files in case they were changed externally.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="522"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="525"/>
        <source>Change File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="532"/>
        <source>Ignore Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="534"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="538"/>
        <source>Disregard this source/filter&apos;s time from animations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="549"/>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="552"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="566"/>
        <source>Delete Downstream Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="569"/>
        <source>Delete selection and all downstream filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="575"/>
        <source>&amp;Create Custom Filter...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="581"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="583"/>
        <source>Link with selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="585"/>
        <source>Link this source and current selected source as a selection link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="592"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="594"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="598"/>
        <source>Rename currently selected source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="620"/>
        <source>Add Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="112"/>
        <source>&amp;Add current filter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPreviewMenuManager</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPreviewMenuManager.cxx" line="99"/>
        <source>Custom ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPreviewMenuManager.cxx" line="277"/>
        <source>Requested resolution too big for window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqPreviewMenuManager.cxx" line="278"/>
        <source>The resolution requested is too big for the current window. Fitting to aspect ratio instead.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyGroupMenuManager</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="606"/>
        <source>&amp;Manage Favorites...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="742"/>
        <source>Search...	Alt+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="745"/>
        <source>Search...	Ctrl+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="752"/>
        <source>&amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="759"/>
        <source>&amp;OldFavorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="771"/>
        <source>&amp;Miscellaneous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="915"/>
        <source>Add to Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="227"/>
        <source>&amp;Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqProxyGroupMenuManager.cxx" line="767"/>
        <source>&amp;Alphabetical</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqReloadFilesReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqReloadFilesReaction.cxx" line="37"/>
        <source>Reload Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqReloadFilesReaction.cxx" line="38"/>
        <source>This reader supports file series. Do you want to look for new files in the series and load those, or reload the existing files?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqReloadFilesReaction.cxx" line="45"/>
        <source>Find new files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqReloadFilesReaction.cxx" line="49"/>
        <source>Reload existing file(s)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRenameProxyReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="55"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="55"/>
        <source>Proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="59"/>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="78"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRenameProxyReaction.cxx" line="59"/>
        <source>New name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRepresentationToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRepresentationToolbar.cxx" line="13"/>
        <source>Representation Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRescaleScalarRangeReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="142"/>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="292"/>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="392"/>
        <source>Block </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="145"/>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="295"/>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="395"/>
        <source>Reset </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="145"/>
        <source>Transfer Function Ranges Using Data Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="295"/>
        <source>Transfer Function Ranges To Custom Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="395"/>
        <source>Transfer Function Ranges Using Temporal Data Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqRescaleScalarRangeReaction.cxx" line="445"/>
        <source>Reset Transfer Function Ranges To Visible Data Range</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqResetDefaultSettingsReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="32"/>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="80"/>
        <source>Reset to Default Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="34"/>
        <source>Reset custom settings to default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="36"/>
        <source>All custom setting will be reset to their default values. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="39"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="40"/>
        <source>Yes, and backup current settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="57"/>
        <source>Backups failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="58"/>
        <source>Failed to generate backup files. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="66"/>
        <source>Following backup files have been generated:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="67"/>
        <source>

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="77"/>
        <source>Please restart %1 for the changes to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqResetDefaultSettingsReaction.cxx" line="82"/>
        <source>Settings reset to default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveAnimationGeometryReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationGeometryReaction.cxx" line="58"/>
        <source>Save Animation Geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationGeometryReaction.cxx" line="87"/>
        <source>Save geometry progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationGeometryReaction.cxx" line="87"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationGeometryReaction.cxx" line="89"/>
        <source>Saving Geometry ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveAnimationReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationReaction.cxx" line="136"/>
        <source>Save Animation Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationReaction.cxx" line="143"/>
        <source>Save animation progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationReaction.cxx" line="143"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveAnimationReaction.cxx" line="144"/>
        <source>Saving Animation ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveDataReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveDataReaction.cxx" line="90"/>
        <source>Save File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveDataReaction.cxx" line="133"/>
        <source>Serial Writer Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveDataReaction.cxx" line="134"/>
        <source>This writer (%1) will collect all of the data to the first node before writing because it does not support parallel IO. This may cause the first node to run out of memory if the data is large.
Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveDataReaction.cxx" line="151"/>
        <source>Configure Writer (%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveExtractsReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveExtractsReaction.cxx" line="44"/>
        <source>Save Extracts Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveExtractsReaction.cxx" line="50"/>
        <source>Saving Extracts ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveScreenshotReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveScreenshotReaction.cxx" line="196"/>
        <source>Save Screenshot Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveScreenshotReaction.cxx" line="290"/>
        <source>View content has been copied to the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSaveStateReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="66"/>
        <source>Save State File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="93"/>
        <source>Failed to save %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="111"/>
        <source>Unable to read python state options.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="119"/>
        <source>Trace and Python State incompatibility.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="120"/>
        <source>Save Python&#x202f;State can not work while Trace is active. Aborting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="128"/>
        <source>Empty state generated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="137"/>
        <source>Failed to save state in %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="152"/>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="201"/>
        <source>Failed to save &apos;%1&apos; since Python support is not enabled in this build.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="183"/>
        <source>Failed to create python state options since Python support is not enabled in this build.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSaveStateReaction.cxx" line="173"/>
        <source>Python State Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqScalarBarVisibilityReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqScalarBarVisibilityReaction.cxx" line="174"/>
        <source>Number of annotations warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqScalarBarVisibilityReaction.cxx" line="175"/>
        <source>The color map have been configured to show lots of annotations. Showing the scalar bar in this situation may slow down the rendering and it may not be readable anyway. Do you really want to show the color map ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqScalarBarVisibilityReaction.cxx" line="190"/>
        <source>Block </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqScalarBarVisibilityReaction.cxx" line="192"/>
        <source>Toggle </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqScalarBarVisibilityReaction.cxx" line="192"/>
        <source>Color Legend Visibility</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectionEditor</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionEditor.cxx" line="452"/>
        <location filename="../Qt/ApplicationComponents/pqSelectionEditor.cxx" line="653"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionEditor.cxx" line="815"/>
        <source>Different Element Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionEditor.cxx" line="816"/>
        <source>The current active selection has a different element type compared to chosen element type.
Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectionListPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionListPropertyWidget.ui" line="23"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSelectionListPropertyWidget.ui" line="48"/>
        <source>Labels list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSeriesAnnotationsModel</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="48"/>
        <source>Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="48"/>
        <source>Series name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="50"/>
        <source>Toggle series visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="52"/>
        <source>Set color to use for the series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="54"/>
        <source>Legend Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSeriesEditorPropertyWidget.cxx" line="55"/>
        <source>Set the text to use for the series in the legend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerConnectReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerConnectReaction.cxx" line="51"/>
        <source>Disconnect from current server?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerConnectReaction.cxx" line="53"/>
        <source>Before connecting to a new server, the current connection will be closed and the state will be discarded. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerConnectReaction.cxx" line="55"/>
        <source>Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerDisconnectReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerDisconnectReaction.cxx" line="59"/>
        <source>Disconnect from current server?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqServerDisconnectReaction.cxx" line="60"/>
        <source>The current connection will be closed and
the state will be discarded.

Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSetMainWindowTitleReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSetMainWindowTitleReaction.cxx" line="23"/>
        <source>Rename Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSetMainWindowTitleReaction.cxx" line="23"/>
        <source>New title:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqShowHideAllReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqShowHideAllReaction.cxx" line="28"/>
        <source>Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqShowHideAllReaction.cxx" line="35"/>
        <source>Hide All</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSourcesMenuReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSourcesMenuReaction.cxx" line="149"/>
        <source>Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSourcesMenuReaction.cxx" line="153"/>
        <source>Creating &apos;%1&apos;. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSourcesMenuReaction.cxx" line="173"/>
        <source>Create &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSpherePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSpherePropertyWidget.cxx" line="38"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSpherePropertyWidget.cxx" line="88"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSpherePropertyWidget.cxx" line="94"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSplinePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="312"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="317"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="321"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="328"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="334"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSplinePropertyWidget.cxx" line="341"/>
        <source>Ctrl+2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSpreadSheetViewDecorator</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="37"/>
        <source>&lt;b&gt;Showing  &lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="51"/>
        <source>&lt;b&gt;   Attribute:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="71"/>
        <source>Precision:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="88"/>
        <source>Switches between scientific and fixed-point representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="91"/>
        <source>Toggle fixed-point representation (always show #Precision digits)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="94"/>
        <source>FixedRep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="108"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="111"/>
        <source>Show only selected elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="114"/>
        <source>Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="131"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="134"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="137"/>
        <source>Toggle column visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="151"/>
        <source>Toggle cell connectivity visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="154"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="171"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="168"/>
        <source>Toggle field data visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqSpreadSheetViewDecorator.ui" line="205"/>
        <source>Export Spreadsheet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqStandardViewFrameActionsImplementation</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="99"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="100"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="101"/>
        <source>f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="102"/>
        <source>g</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="183"/>
        <source>Polygon Selection (d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="196"/>
        <source>Rectangle Selection (s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="210"/>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="586"/>
        <source>Clear selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="241"/>
        <source>Add selection (Ctrl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="250"/>
        <source>Subtract selection (Shift)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="259"/>
        <source>Toggle selection (Ctrl+Shift)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="306"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="309"/>
        <source>Convert To ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="319"/>
        <source>Camera Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="327"/>
        <source>Camera Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="340"/>
        <source>Capture to Clipboard or File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="342"/>
        <source>Capture screenshot to a file or to the clipboard if a modifier key (Ctrl, Alt or Shift) is pressed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="361"/>
        <source>Change Interaction Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="369"/>
        <source>Adjust Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="381"/>
        <source>Select Cells On (s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="393"/>
        <source>Select Points On (d)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="405"/>
        <source>Select Cells Through (f)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="417"/>
        <source>Select Points Through (g)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="429"/>
        <source>Select Cells With Polygon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="441"/>
        <source>Select Points With Polygon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="453"/>
        <source>Select Block (b)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="464"/>
        <source>Select Blocks Through (n)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="477"/>
        <source>Interactive Select Cell Data On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="492"/>
        <source>Interactive Select Point Data On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="508"/>
        <source>Interactive Select Cells On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="523"/>
        <source>Interactive Select Points On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="538"/>
        <source>Hover Cells On. Use Ctrl-C/Cmd-C to copy the content to clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="553"/>
        <source>Hover Points On. Use Ctrl-C/Cmd-C to copy the content to clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="567"/>
        <source>Grow selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="576"/>
        <source>Shrink selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="706"/>
        <source>Convert To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx" line="728"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTestingReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="34"/>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="83"/>
        <source>XML Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="36"/>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="85"/>
        <source>Python Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="38"/>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="87"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="40"/>
        <source>Record Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTestingReaction.cxx" line="89"/>
        <source>Play Test</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTimelineItemDelegate</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="91"/>
        <source>%1 is unlocked. Lock to avoid auto-update when adding/removing time sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="97"/>
        <source>%1 is locked. Unlock to allow auto-update when adding/removing time sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="107"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="114"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="119"/>
        <source>Start time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="126"/>
        <source>End Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="133"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineItemDelegate.cxx" line="138"/>
        <source>End time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTimelineView</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="39"/>
        <source>Reset Start and End Time to default values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="58"/>
        <source>Select proxy to animate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="61"/>
        <source>Python</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="67"/>
        <source>Select property to animate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="80"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="81"/>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="83"/>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="108"/>
        <source>Follow Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="110"/>
        <source>Follow Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="112"/>
        <source>Interpolate cameras</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="256"/>
        <source>Add animation cue for selected proxy and property.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineView.cxx" line="260"/>
        <source>Animation cue already exists.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTimelineWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="116"/>
        <source>If checked, scene use times from time sources.
Otherwise, generate NumberOfFrames time entries.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="118"/>
        <source>Time Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="123"/>
        <source>Check / Uncheck to enable all animation tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="165"/>
        <source>Remove Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="238"/>
        <source>Add Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="315"/>
        <source>Check/Uncheck to make timesteps available in the scene time list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="636"/>
        <source>Check / uncheck to enable the animation. Double click on name or timeline to edit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTimelineWidget.cxx" line="643"/>
        <source>Timekeeper updates pipeline. It maps scene time to a requested pipeline time.
 Uncheck to freeze pipeline time.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqToolsMenu</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="315"/>
        <source>Create Custom Filter...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="318"/>
        <source>Add Camera Link...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="321"/>
        <source>Link with Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="325"/>
        <source>Manage Custom Filters...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="328"/>
        <source>Manage Links...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="333"/>
        <source>Manage Plugins...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="342"/>
        <source>Configure Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="348"/>
        <source>Customize Shortcuts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="352"/>
        <source>Manage Expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="358"/>
        <source>Record Test...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="361"/>
        <source>Play Test...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="364"/>
        <source>Lock View Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="368"/>
        <source>Lock View Size Custom...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="372"/>
        <source>Timer Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="374"/>
        <source>Log Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="384"/>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="386"/>
        <source>Start Trace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="387"/>
        <source>Stop Trace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="390"/>
        <source>Python Script Editor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTraceReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="51"/>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="53"/>
        <source>Tracing unavailable since application built without Python support.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="94"/>
        <source>Trace Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="108"/>
        <source>Trace and Auto Save incompatibility.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="109"/>
        <source>Auto Save Python&#x202f;State setting can not work while Trace is active. Auto Save will be disabled until the Trace is stopped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTraceReaction.cxx" line="123"/>
        <source>Recording python trace...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTransferFunctionWidgetPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqTransferFunctionWidgetPropertyWidget.cxx" line="74"/>
        <source>Reset using current data values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqUndoRedoReaction</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="91"/>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="93"/>
        <source>Can&apos;t Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="91"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="93"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="97"/>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="99"/>
        <source>Can&apos;t Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="97"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqUndoRedoReaction.cxx" line="99"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqVCRToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="8"/>
        <source>VCR Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="27"/>
        <source>&amp;Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="38"/>
        <source>&amp;Reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="50"/>
        <source>Pre&amp;vious Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="64"/>
        <source>&amp;First Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="75"/>
        <source>&amp;Next Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="86"/>
        <source>&amp;Last Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="100"/>
        <source>L&amp;oop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="103"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="106"/>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqVCRToolbar.ui" line="109"/>
        <source>Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="72"/>
        <source>First Frame (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="73"/>
        <source>Last Frame (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="80"/>
        <source>Reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="80"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqVCRToolbar.cxx" line="90"/>
        <source>Pa&amp;use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqValueWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="207"/>
        <source>value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="211"/>
        <source>comma separated values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="224"/>
        <source>minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="228"/>
        <source>maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="230"/>
        <source>and</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="249"/>
        <source>X coordinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="252"/>
        <source>Y coordinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="255"/>
        <source>Z coordinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqSelectionQueryPropertyWidget.cxx" line="266"/>
        <source>within epsilon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqViewMenuManager</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="64"/>
        <source>Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="79"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="103"/>
        <source>Show Frame Decorations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="112"/>
        <source>Toggle Lock Panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="114"/>
        <source>Toggle locking of dockable panels so they    cannot be moved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="118"/>
        <source>Equalize Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="122"/>
        <source>Equalize layout so views are evenly sized horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="129"/>
        <source>Equalize layout so views are evenly sized vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="133"/>
        <source>Both</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="136"/>
        <source>Equalize layout so views are evenly sized horizontally and vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="119"/>
        <source>Horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="83"/>
        <source>Full Screen (layout)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="91"/>
        <source>Full Screen (active view)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewMenuManager.cxx" line="126"/>
        <source>Vertically</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqViewResolutionPropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewResolutionPropertyWidget.cxx" line="97"/>
        <source>Presets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqViewTypePropertyWidget</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewTypePropertyWidget.cxx" line="28"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/pqViewTypePropertyWidget.cxx" line="29"/>
        <source>Empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqWelcomeDialog</name>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqWelcomeDialog.ui" line="16"/>
        <source>Welcome to ParaView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqWelcomeDialog.ui" line="61"/>
        <source>Example Visualizations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqWelcomeDialog.ui" line="91"/>
        <source>Getting Started Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/ApplicationComponents/Resources/UI/pqWelcomeDialog.ui" line="110"/>
        <source>Don&apos;t show this window again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqmacrosToolbar</name>
    <message>
        <location filename="../Qt/ApplicationComponents/pqParaViewMenuBuilders.cxx" line="866"/>
        <source>Macros Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
