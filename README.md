# ParaView-Translations

This repository contains translation files for [ParaView][pvrepo].

Each folder contains translations for one language (the name of the folder), except `en` who stores the template files with all source strings from [ParaView][pvrepo].

Translated languages:
 - de_DE
 - es_ES
 - fr_FR
 - it_IT
 - ja_JP
 - ko_KR
 - nl_NL
 - pt_BR
 - tr_TR

## How to contribute to translations

Access [ParaView weblate][weblate] and start translating ParaView !

## Update Translation source

Translation sources for translated languages can be updated from the default language translation source using the dedicated target

```
cmake --build . --target files_update
```

## Building Translation Files

Translation files can be built using CMake:

```
cmake --build .
```

You can also build specific files with their specific locale name

In order to get the Japanese and French files, one should write:
```
cmake --build . --target ja_JP fr_FR
```

Resulting translation files can be used in place in compiled ParaView.
In the binary release, the expected location is `./share/paraview-5.11/translations/`.
The environnement variable `PV_TRANSLATIONS_DIR` can also be used to specify it.

## Adding a new language

- Create a directory with the right language name (eg `ja_JP`) and add a .gitkeep file in it
- Add the language in the main `CMakeLists.txt`
- build the `files_update` target
- Add the new files, commit and pushes the changes
- Open a merge request on this repository and tag maintainers

## License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.

## Maintainers

- @francois.mazen
- @nicolas.vuaille
- @mwestphal

[pvrepo]: https://gitlab.kitware.com/paraview/paraview
[weblate]: https://weblate.kitware.com/projects/paraview/
