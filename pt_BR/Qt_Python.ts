<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>PythonShell</name>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="62"/>
        <source>Run Script</source>
        <translation>Executar Script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="72"/>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="82"/>
        <source>Reset</source>
        <translation>Redefinir</translation>
    </message>
</context>
<context>
    <name>pqPythonEditorActions</name>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="38"/>
        <source>&amp;New</source>
        <translation>&amp;Novo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="41"/>
        <source>Create a new file</source>
        <translation>Criar um novo arquivo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="44"/>
        <source>&amp;Open...</source>
        <translation>&amp;Abrir...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="47"/>
        <source>Open an existing file</source>
        <translation>Abrir um arquivo existente</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="50"/>
        <source>&amp;Save</source>
        <translation>&amp;Salvar</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="53"/>
        <source>Save the document to disk</source>
        <translation>Salvar o documento em disco</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="56"/>
        <source>Save &amp;As...</source>
        <translation>Salvar &amp;como...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="58"/>
        <source>Save the document under a new name</source>
        <translation>Salvar o documento com um novo nome</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="61"/>
        <source>Save As &amp;Macro...</source>
        <translation>Salvar Como &amp;Macro...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="63"/>
        <source>Save the document as a Macro</source>
        <translation>Salvar o documento como uma Macro</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="66"/>
        <source>Save As &amp;Script...</source>
        <translation>Salvar Como &amp;Script...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="68"/>
        <source>Save the document as a Script</source>
        <translation>Salvar o documento como um Script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="71"/>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="373"/>
        <source>Delete All</source>
        <translation>Apagar Tudo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="73"/>
        <source>Delete all scripts from disk</source>
        <translation>Apagar todos os scripts do disco</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="76"/>
        <source>Run...</source>
        <translation>Executar...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="78"/>
        <source>Run the currently edited script</source>
        <translation>Executar o script editado atualmente</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="81"/>
        <source>Cut</source>
        <translation>Recortar</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="84"/>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation>Recortar o conteúdo da seleção atual para a área de transferência</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="89"/>
        <source>Undo</source>
        <translation>Desfazer</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="92"/>
        <source>Undo the last edit of the file</source>
        <translation>Desfazer a última edição do arquivo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="95"/>
        <source>Redo</source>
        <translation>Refazer</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="98"/>
        <source>Redo the last undo of the file</source>
        <translation>Refazer o último desfazer do arquivo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="101"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="104"/>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation>Copiar o conteúdo da seleção atual para a área de transferência</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="109"/>
        <source>Paste</source>
        <translation>Colar</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="112"/>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation>Colar o conteúdo da área de transferência na seleção atual</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="117"/>
        <source>C&amp;lose</source>
        <translation>F&amp;echar</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="120"/>
        <source>Close the script editor</source>
        <translation>Fechar o editor de scripts</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="123"/>
        <source>Close Current Tab</source>
        <translation>Fechar guia atual</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="126"/>
        <source>Close the current opened tab</source>
        <translation>Fechar a guia aberta atualmente</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="356"/>
        <source>Open File</source>
        <translation>Abrir Arquivo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="357"/>
        <source>Python Files</source>
        <translation>Arquivos Python</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="374"/>
        <source>All scripts will be deleted. Are you sure?</source>
        <translation>Todos os scripts serão apagados. Tem certeza?</translation>
    </message>
</context>
<context>
    <name>pqPythonFileIO</name>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="34"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="287"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="314"/>
        <source>Sorry!</source>
        <translation>Desculpe!</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="35"/>
        <source>Could not create user PythonSwap directory: %1.</source>
        <translation>Não foi possível criar o diretório PythonSwap do usuário: %1.</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="80"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="94"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="81"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="95"/>
        <source>No Filename Given!</source>
        <translation>Nenhum Nome de Arquivo Fornecido!</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="102"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="183"/>
        <source>Script Editor</source>
        <translation>Editor de Scripts</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="103"/>
        <source>Paraview found an old automatic save file %1. Would you like to recover its content?</source>
        <translation>O Paraview encontrou um arquivo de salvamento automático antigo %1. Gostaria de recuperar seu conteúdo?</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="184"/>
        <source>The document has been modified.
 Do you want to save your changes?</source>
        <translation>O documento foi modificado.
 Deseja salvar as alterações?</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="260"/>
        <source>Save File As</source>
        <translation>Salvar Arquivo Como</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="261"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="294"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="322"/>
        <source>Python Files</source>
        <translation>Arquivos Python</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="288"/>
        <source>Could not create user Macro directory: %1.</source>
        <translation>Não foi possível criar o diretório de Macro do usuário: %1.</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="294"/>
        <source>Save As Macro</source>
        <translation>Salvar Como Macro</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="315"/>
        <source>Could not create user Script directory: %1.</source>
        <translation>Não foi possível criar o diretório de Script do usuário: %1.</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="322"/>
        <source>Save As Script</source>
        <translation>Salvar Como Script</translation>
    </message>
</context>
<context>
    <name>pqPythonMacroSupervisor</name>
    <message>
        <location filename="../Qt/Python/pqPythonMacroSupervisor.cxx" line="115"/>
        <source>empty</source>
        <translation>vazio</translation>
    </message>
</context>
<context>
    <name>pqPythonManagerRawInputHelper</name>
    <message>
        <source>Enter Input requested by Python</source>
        <translation type="vanished">Insira a entrada solicitada pelo Python</translation>
    </message>
    <message>
        <source>Input: </source>
        <translation type="vanished">Entrada: </translation>
    </message>
</context>
<context>
    <name>pqPythonScriptEditor</name>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="44"/>
        <source>ParaView Python Script Editor</source>
        <translation>Editor de Scripts Python do ParaView</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="48"/>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="54"/>
        <source>%1[*] - %2</source>
        <translation>%1[*] - %2</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="48"/>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="54"/>
        <source>Script Editor</source>
        <translation>Editor de Scripts</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="49"/>
        <source>File %1 saved</source>
        <translation>Arquivo %1 salvo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="55"/>
        <source>File %1 opened</source>
        <translation>Arquivo %1 aberto</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="126"/>
        <source>&amp;File</source>
        <translation>&amp;Arquivo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="140"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="151"/>
        <source>&amp;Scripts</source>
        <translation>&amp;Scripts</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="156"/>
        <source>Open...</source>
        <translation>Abrir...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="157"/>
        <source>Open a python script in a new tab</source>
        <translation>Abrir um script python em uma nova guia</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="161"/>
        <source>Load script into current editor tab...</source>
        <translation>Carregar script na aba atual do editor...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="163"/>
        <source>Load a python script in the current opened tab and override its content</source>
        <translation>Carregar um script python na aba aberta atual e sobrescrever seu conteúdo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="166"/>
        <source>Delete...</source>
        <translation>Apagar...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="167"/>
        <source>Delete the script</source>
        <translation>Apagar o script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="170"/>
        <source>Run...</source>
        <translation>Executar...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="172"/>
        <source>Load a python script in a new tab and run it</source>
        <translation>Carregar um script python em uma nova aba e executá-lo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="184"/>
        <source>Ready</source>
        <translation>Pronto</translation>
    </message>
</context>
<context>
    <name>pqPythonShell</name>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="162"/>
        <source>resetting</source>
        <translation>redefinindo</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="182"/>
        <source>
Python %1 on %2
</source>
        <translation>
Python %1 em %2
</translation>
    </message>
    <message>
        <source>Enter Input requested by Python</source>
        <translation type="vanished">Insira a entrada solicitada pelo Python</translation>
    </message>
    <message>
        <source>Input: </source>
        <translation type="vanished">Entrada: </translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="445"/>
        <source>Run Script</source>
        <translation>Executar Script</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="446"/>
        <source>Python Files</source>
        <translation>Arquivos Python</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="446"/>
        <source>All Files</source>
        <translation>Todos os Arquivos</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="469"/>
        <source>Error: script </source>
        <translation>Erro: script </translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="469"/>
        <source> was empty or could not be opened.</source>
        <translation> estava vazio ou não pôde ser aberto.</translation>
    </message>
</context>
<context>
    <name>pqPythonTabWidget</name>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="151"/>
        <source>Sorry!</source>
        <translation>Desculpe!</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="152"/>
        <source>Cannot open file %1:
%2.</source>
        <translation>Não é possível abrir o arquivo %1: 
%2.</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="204"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="275"/>
        <source>Close Tab</source>
        <translation>Fechar Aba</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="402"/>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="404"/>
        <source>New File</source>
        <translation>Novo Arquivo</translation>
    </message>
</context>
</TS>
