<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>ServerManagerXML</name>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="7"/>
        <source>&amp;Common</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>&amp;Comum</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="10"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="286"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="565"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="781"/>
        <source>Calculator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Calculadora</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="13"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="109"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="850"/>
        <source>Contour</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Contorno</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="16"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="106"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="820"/>
        <source>Clip</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Aparar</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="19"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="112"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="883"/>
        <source>Cut</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Cortar</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="22"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="157"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1477"/>
        <source>Threshold</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Limiar</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="25"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="967"/>
        <source>Extract Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Malha</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="28"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1057"/>
        <source>Glyph</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Glifo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="31"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1405"/>
        <source>Stream Tracer</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Traçador de Linhas de Fluxo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="34"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1513"/>
        <source>Warp Vector</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Vetor de Deformação</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="37"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="121"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1066"/>
        <source>Group Data Sets</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Agrupar Conjuntos de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="40"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="73"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="199"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="937"/>
        <source>Extract Block</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Bloco</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="160"/>
        <source>C&amp;osmoTools</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>FerramentasC&amp;osmo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="163"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="709"/>
        <source>ANL Halo Finder</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Localizador Halo ANL</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="166"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="712"/>
        <source>ANL Subhalo Finder</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Localizador Subhalo ANL</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="169"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1132"/>
        <source>LANL Halo Finder</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Localizador Halo LANL</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="172"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1168"/>
        <source>Minkowski Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro Minkowski</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="175"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1219"/>
        <source>P Merge Connected</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mesclar Polígonos Conectados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="283"/>
        <source>&amp;Data Analysis</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>&amp;Análise de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="292"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="544"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="940"/>
        <source>Extract Cells Along Line</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Células ao Longo da Linha</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="295"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="547"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="943"/>
        <source>Extract Cells Along Line Custom</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Células por Linha Customizada</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="298"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="550"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="946"/>
        <source>Extract Cells By Type</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Células Por Tipo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="262"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="301"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="958"/>
        <source>Extract Field Data Over Time</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Dados de Campo ao Longo do Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="313"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="556"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="985"/>
        <source>Extract Selection</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Seleção</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="316"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="988"/>
        <source>Extract Selection Over Time</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Seleção ao Longo do Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="265"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="304"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="973"/>
        <source>Extract Histogram</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Histograma</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="268"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="307"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="976"/>
        <source>Extract Histogram2D</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Histograma 2D</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="319"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1117"/>
        <source>Integrate Attributes</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Integrar Atributos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="277"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="334"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1294"/>
        <source>Probe Line</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Linha de Amostragem</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="337"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="559"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1297"/>
        <source>Probe Point</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Ponto de Amostragem</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="271"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="322"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1246"/>
        <source>Plot Attributes</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Plotar Atributos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="331"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1255"/>
        <source>Plot On Sorted Lines</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Plotar em Linhas Ordenadas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="328"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1252"/>
        <source>Plot On Intersection Curves</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Plotar em Curvas de Interseção</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="340"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="571"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1306"/>
        <source>Programmable Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro Programável</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="346"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1492"/>
        <source>Transpose Table</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Transpor Tabela</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="289"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="841"/>
        <source>Compute Quartiles</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Calcular Quartis</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="310"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="553"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="979"/>
        <source>Extract Location</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Localização</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="274"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="325"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1249"/>
        <source>Plot Data Over Time</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Plotar Dados ao Longo do Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="343"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1480"/>
        <source>Threshold Table</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Tabela de Limiarização</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="100"/>
        <source>H&amp;yper Tree Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Malha Hiper-Árvore</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="124"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1072"/>
        <source>Hyper Tree Grid Axis Reflection</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Reflexão de Eixos da Malha Hiper-Árvore</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="127"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1075"/>
        <source>Hyper Tree Grid Cell Centers</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Centros de Células da Malha Hiper-Árvore</translation>
    </message>
    <message>
        <source>Hyper Tree Grid Extract Ghost Cells</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml:51 - Clients/ParaView/ParaViewFilters.xml
----------
Real source: Clients/ParaView/ParaViewFilters.xml:386 - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="vanished">Extrair Células Fantasmas da Malha Hiper-Árvore</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="130"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1078"/>
        <source>Hyper Tree Grid Depth Limiter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Limitador de Profundidade da Malha Hiper-Árvore</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="136"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1084"/>
        <source>Hyper Tree Grid Feature Edges</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Arestas Características da Malha Hiper-Árvore</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="142"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1090"/>
        <source>Hyper Tree Grid Geometry</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Geometria da Malha Hiper-Árvore</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="118"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="631"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1063"/>
        <source>Gradient</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gradiente</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="133"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1081"/>
        <source>Hyper Tree Grid Evaluate Coarse</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Avaliar Resolução Grosseira da Malha Hiper-Árvore</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="145"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1093"/>
        <source>Hyper Tree Grid Ghost Cells Generator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerador de Células Fantasmas da Malha Hiper-Árvore</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="148"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1096"/>
        <source>Hyper Tree Grid To Dual Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Malha Hiper-Árvore para Malha Dual</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="151"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1099"/>
        <source>Hyper Tree Grid To Unstructured Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Malha Hiper-Árvore para Malha Não-Estruturada</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="667"/>
        <source>&amp;Statistics</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>&amp;Estatísticas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="670"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="847"/>
        <source>Contingency Statistics</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Estatísticas de Contingência</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="673"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="916"/>
        <source>Descriptive Statistics</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Estatísticas Descritivas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="676"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1129"/>
        <source>K Means</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>K Médias</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="679"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1177"/>
        <source>Multicorrelative Statistics</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Estatísticas Multicorrelativas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="682"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1216"/>
        <source>PCA Statistics</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Estatísticas da ACPs</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="490"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="685"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1456"/>
        <source>Temporal Statistics</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Estatísticas Temporais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="430"/>
        <source>&amp;Temporal</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>&amp;Temporal</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="433"/>
        <source>Animate Modes</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Modos de Animação</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="439"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="982"/>
        <source>Extract Particles Over Time</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Partículas ao Longo do Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="442"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="994"/>
        <source>Extract Time Steps</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Passos de Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="448"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1015"/>
        <source>Force Time</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Impor Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="451"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1033"/>
        <source>Generate Time Steps</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerar Passos de Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="454"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1069"/>
        <source>Group Time Steps</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Agrupar Passos de Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="457"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1159"/>
        <source>Merge Time</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mesclar Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="460"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1228"/>
        <source>Particle Path</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Trajetória de Partícula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="463"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1234"/>
        <source>Particle Tracer</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Traçador de Partículas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="466"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1402"/>
        <source>Streak Line</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Linha de Rastreio</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="469"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1426"/>
        <source>Synchronize Time</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Sincronizar Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="472"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1438"/>
        <source>Temporal Array Operator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Operador Temporal de Matrizes</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="475"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1441"/>
        <source>Temporal Cache</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Cache Temporal</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="478"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1444"/>
        <source>Temporal Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador Temporal</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="481"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1447"/>
        <source>Temporal Shift Scale</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Escala de Deslocamento Temporal</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="487"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1453"/>
        <source>Temporal Snap To Time Step</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Ajuste Temporal ao Passo de Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="235"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="493"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1483"/>
        <source>Time Step Progress Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Progresso do Passo de Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="238"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="496"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1486"/>
        <source>Time To Text Convertor</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Conversor de Tempo para Texto</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="370"/>
        <source>Materia&amp;l Analysis</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Análise de Materia&amp;l</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="205"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="373"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1150"/>
        <source>Material Interface Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Interface de Material</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="376"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1123"/>
        <source>Intersect Fragments</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interseção de Fragmentos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="178"/>
        <source>CT&amp;H</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>CT&amp;H</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="196"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="778"/>
        <source>CTH Part</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Parte CTH</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="208"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1180"/>
        <source>Non Overlapping Level Id Scalars</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Escalares de ID de Nível Não Sobrepostos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="52"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="184"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="691"/>
        <source>AMR Dual Clip</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Corte Duplo AMR</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="55"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="187"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="694"/>
        <source>AMR Dual Contour</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Contorno Duplo AMR</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="49"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="181"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="688"/>
        <source>AMR Connectivity</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Conectividade AMR</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="58"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="190"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="697"/>
        <source>AMR Fragment Integration</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Integração de Fragmentos AMR</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="61"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="193"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="700"/>
        <source>AMR Fragments Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Fragmentos AMR</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="76"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="202"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="970"/>
        <source>Extract Hierarchical Data Sets</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Conjuntos de Dados Hierárquicos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="46"/>
        <source>AM&amp;R</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>AM&amp;R</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="64"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="703"/>
        <source>AMR Resample Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Reamostragem AMR</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="67"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="706"/>
        <source>AMR To Multi Block</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>AMR para Bloco Múltiplo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1210"/>
        <source>Overlapping Level Id Scalars</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Escalares de ID de Nível Sobrepostos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="82"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1390"/>
        <source>Slice With Plane</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Cortar com Plano</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="418"/>
        <source>&amp;Quadrature Points</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>&amp;Pontos de Quadratura</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="421"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1315"/>
        <source>Quadrature Point Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador de Pontos de Quadratura</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="424"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1318"/>
        <source>Quadrature Points Generator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerador de Pontos de Quadratura</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="427"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1321"/>
        <source>Quadrature Scheme Dictionary Generator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerador de Dicionário de Esquema de Quadratura</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="214"/>
        <source>Annotation</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anotação</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="220"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="733"/>
        <source>Annotate Global Data</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anotar Dados Globais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="217"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="730"/>
        <source>Annotate Attribute Data</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anotar Dados de Atributo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="223"/>
        <source>Annotate Selection</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anotar Seleção</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="232"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="574"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1309"/>
        <source>Python Annotation</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anotação Python</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="226"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="889"/>
        <source>Data Set Region Surface Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Superfície de Região do Conjunto de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="229"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="925"/>
        <source>Environment Annotation</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anotação de Ambiente</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="391"/>
        <source>Point Interpolation</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolação de Pontos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="397"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1267"/>
        <source>Point Line Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador Ponto Linha</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="400"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1270"/>
        <source>Point Plane Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador Ponto Plano</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="403"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1279"/>
        <source>Point Volume Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador Ponto Volume</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="394"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1264"/>
        <source>Point Dataset Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador Conjunto de Dados de Ponto</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="409"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1372"/>
        <source>SPH Line Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador de Linha SPH</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="412"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1375"/>
        <source>SPH Plane Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador de Plano SPH</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="415"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1378"/>
        <source>SPH Volume Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador de Volume SPH</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="406"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1369"/>
        <source>SPH Dataset Interpolator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interpolador de Conjunto de Dados SPH</translation>
    </message>
    <message>
        <source>Chemistry</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml:148 - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="vanished">Química</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="88"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="748"/>
        <source>Append Molecule</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anexar Molécula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="91"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="835"/>
        <source>Compute Molecule Bonds</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Calcular Ligações de Molécula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="94"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1171"/>
        <source>Molecule To Lines</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Molécula para Linhas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="97"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1273"/>
        <source>Point Set To Molecule</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Conjunto de Pontos para Molécula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="379"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="385"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1174"/>
        <source>Moment Invariants</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Invariantes de Momento</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="382"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="838"/>
        <source>Compute Moments</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Calcular Momentos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="388"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1225"/>
        <source>Parallel Compute Moments</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Computação Paralela de Momentos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="715"/>
        <source>Adaptive Resample To Image</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Reamostragem Adaptativa para Imagem</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="718"/>
        <source>Add Field Arrays</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Adicionar Matrizes de Campo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="721"/>
        <source>Aggregate Data Set</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Agregar Conjunto de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="724"/>
        <source>Align Image Origin</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Alinhar Origem da Imagem</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="727"/>
        <source>Angular Periodic Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro Periódico Angular</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="736"/>
        <source>Append</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anexar</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="739"/>
        <source>Append Arc Length</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anexar Comprimento de Arco</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="604"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="742"/>
        <source>Append Attributes</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anexar Atributos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="745"/>
        <source>Append Location Attributes</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anexar Atributos de Localização</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="751"/>
        <source>Append Poly Data</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Acrescentar Dados Poligonais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="754"/>
        <source>Append Reduction Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anexar Filtro de Redução</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="757"/>
        <source>Arbitrary Source Stream Tracer</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Rastreador de Fluxo de Fonte Arbitrária</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="766"/>
        <source>Block Id Scalars</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Escalares de ID de Bloco</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="772"/>
        <source>Boundary Mesh Quality</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Qualidade da Malha de Contorno</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="586"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="775"/>
        <source>Brownian Points</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Pontos Brownianos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="784"/>
        <source>Cell Centers</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Centros de Célula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="607"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="787"/>
        <source>Cell Data To Point Data</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Dados de Célula para Dados de Ponto</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="625"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="790"/>
        <source>Cell Derivatives</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Derivadas Celulares</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="508"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="808"/>
        <source>Cell Size</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Tamanho da Célula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="811"/>
        <source>Clean Poly Data</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Limpar Dados Poligonais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="814"/>
        <source>Clean Unstructured Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Limpar Malha Não Estruturada</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="817"/>
        <source>Clean Unstructured Grid Cells</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Limpar Células da Malha Não Estruturada</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="823"/>
        <source>Clip Closed Surface</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Recortar Superfície Fechada</translation>
    </message>
    <message>
        <source>Compute Cell Grid Surface</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="vanished">Calcular Superfície de Malha Celular</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="832"/>
        <source>Compute Connected Surface Properties</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Calcular Propriedades de Superfície Conectada</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="856"/>
        <source>Convert To Cell Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Converter para Malha Celular</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="853"/>
        <source>Convert Polyhedra</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Converter Poliedros</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="859"/>
        <source>Convert To Multi Block</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Converter para Bloco Múltiplo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="862"/>
        <source>Convert To Partitioned Data Set Collection</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Converter para Coleção de Conjunto de Dados Particionados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="865"/>
        <source>Convert To Point Cloud</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Converter para Nuvem de Pontos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="517"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="871"/>
        <source>Count Cell Faces</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Contar Faces de Célula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="520"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="874"/>
        <source>Count Cell Vertices</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Contar Vértices de Célula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="523"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="880"/>
        <source>Curvatures</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Curvaturas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="352"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="886"/>
        <source>D3</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>D3</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="892"/>
        <source>Data Set Surface Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Superfície de Conjunto de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="895"/>
        <source>Data Set Triangle Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Triângulo do Conjunto de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="898"/>
        <source>Date To Numeric</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Data para Numérico</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="901"/>
        <source>Decimate Polyline</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Simplificação de Polilinha</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="904"/>
        <source>Decimate Pro</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Simplificação Avançada</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="628"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="907"/>
        <source>Deflect Normals</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Defletir Normais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="910"/>
        <source>Delaunay2D</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Delaunay 2D</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="913"/>
        <source>Delaunay3D</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Delaunay 3D</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="355"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="919"/>
        <source>Distribute Points</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Distribuir Pontos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="526"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="589"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="922"/>
        <source>Elevation Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Elevação</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="928"/>
        <source>Equalizer Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro Equalizador</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="931"/>
        <source>Evenly Spaced Streamlines2D</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Linhas de Fluxo 2D Igualmente Espaçadas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="949"/>
        <source>Extract Component</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Componente</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="952"/>
        <source>Extract Edges</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Arestas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="955"/>
        <source>Extract Enclosed Points</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Pontos Encapsulados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="961"/>
        <source>Extract Geometry</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Geometria</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="115"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="964"/>
        <source>Extract Ghost Cells</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Células Fantasmas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="991"/>
        <source>Extract Subset With Seed</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Extrair Subconjunto Com Semente</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="997"/>
        <source>FFT Selection Over Time</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Seleção FFT ao Longo do Tempo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1000"/>
        <source>Feature Edges</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Destacar Arestas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1009"/>
        <source>Field Data To Data Set Attribute</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Dados de Campo para Atributo do Conjunto de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1012"/>
        <source>Finite Element Field Distributor</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Distribuidor de Campo de Elemento Finito</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1018"/>
        <source>Gaussian Splatter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Espalhamento Gaussiano</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1021"/>
        <source>Generate Global Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerar IDs Globais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1027"/>
        <source>Generate Process Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerar IDs de Processo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1024"/>
        <source>Generate Id Scalars</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerar Id Escalares</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1030"/>
        <source>Generate Spatio Temporal Harmonics</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerar Harmônicos Espaço-Temporais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1036"/>
        <source>Generic Clip</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Recorte Genérico</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1039"/>
        <source>Generic Contour</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Contorno Genérico</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1042"/>
        <source>Generic Cut</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Corte Genérico</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1045"/>
        <source>Generic Geometry Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Geometria Genérica</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1048"/>
        <source>Generic Stream Tracer</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Traçador Genérico de Fluxo</translation>
    </message>
    <message>
        <source>Ghost Cells Generator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml:267 - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="vanished">Gerador de Células Fantasmas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1060"/>
        <source>Glyph With Custom Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Glifo Com Fonte Personalizada</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1105"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1108"/>
        <source>Image Data To AMR</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Dados de Imagem para AMR</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1111"/>
        <source>Image Data To Point Set</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Dados de Imagem para Conjunto de Pontos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1114"/>
        <source>Image Data To Uniform Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Dados de Imagem para Malha Uniforme</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1120"/>
        <source>Integrate Flow Through Surface</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Integrar Fluxo Através da Superfície</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1126"/>
        <source>Iso Volume</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Volume Iso</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1135"/>
        <source>Legacy Ghost Cells Generator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerador de Células Fantasmas Legado</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1138"/>
        <source>Linear Cell Extrusion Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Extrusão Linear de Célula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1141"/>
        <source>Linear Extrusion Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Extrusão Linear</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1144"/>
        <source>Loop Subdivision Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Subdivisão em Loop</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1147"/>
        <source>Mask Points</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mascarar Pontos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1153"/>
        <source>Median</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1156"/>
        <source>Merge Blocks</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mesclar Blocos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1162"/>
        <source>Merge Vector Components</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mesclar Componentes Vetoriais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="529"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1165"/>
        <source>Mesh Quality</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Qualidade da Malha</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1183"/>
        <source>Normal Glyphs</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Glifos Normais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1195"/>
        <source>Octree Image To Point Set</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Imagem Octree para Conjunto de Pontos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1186"/>
        <source>OMETIFF Channel Calculator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Calculadora de Canal OMETIFF</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1189"/>
        <source>OT Density Map</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mapa de Densidade OT</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1192"/>
        <source>OT Kernel Smoothing</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Suavização por Kernel de TO</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1198"/>
        <source>Outline Corner Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Cantos de Contorno</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1201"/>
        <source>Outline Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Contorno</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1207"/>
        <source>Overlapping Cells Detector</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Detector de Células Sobrepostas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1213"/>
        <source>PCA Normal Estimation</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Estimativa de Normais por ACP</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="592"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1243"/>
        <source>Perlin Noise</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Ruído Perlin</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="532"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="658"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1222"/>
        <source>PV Connectivity Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Conectividade PV</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1231"/>
        <source>Particle Path Lines</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Linhas de Trajetória de Partículas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1237"/>
        <source>Partition Balancer</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Balanceador de Partição</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="613"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1240"/>
        <source>Pass Arrays</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Passar Arrays</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1276"/>
        <source>Point Set To Octree Image</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Conjunto de Pontos para Imagem Octree</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="616"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1261"/>
        <source>Point Data To Cell Data</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Dados de Ponto para Dados de Célula</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1282"/>
        <source>Poly Data Normals</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Normais de Dados Poligonais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1285"/>
        <source>Poly Data Tangents</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Tangentes de Dados Poligonais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1291"/>
        <source>Probe Custom Lines</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Sondar Linhas Personalizadas</translation>
    </message>
    <message>
        <source>Process Id Scalars</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="vanished">Escalares de ID de Processo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="568"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1303"/>
        <source>Programmable Annotation</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Anotação Programável</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="577"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1312"/>
        <source>Python Calculator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Calculadora Python</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1324"/>
        <source>Quadric Clustering</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Agrupamento Quadrático</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="595"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1327"/>
        <source>Random Attribute Generator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gerador Aleatório de Atributos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1330"/>
        <source>Rectilinear Grid Connectivity</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Conectividade de Malha Retilínea</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1333"/>
        <source>Rectilinear Grid To Point Set</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Malha Retilínea para Conjunto de Pontos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="364"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1336"/>
        <source>Redistribute Data Set</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Redistribuir Conjunto de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1339"/>
        <source>Reflection Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Reflexão</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="619"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1345"/>
        <source>Rename Arrays</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Renomear Arrays</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="367"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1342"/>
        <source>Remove Ghost Information</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Remover Informações de Células Fantasmas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1348"/>
        <source>Resample To Image</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Reamostrar para Imagem</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1351"/>
        <source>Resample To Line</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Reamostrar para Linha</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1354"/>
        <source>Resample With Dataset</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Reamostrar com Conjunto de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1357"/>
        <source>Reverse Sense</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Inverter Sentido</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1360"/>
        <source>Ribbon Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Faixas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1363"/>
        <source>Rotational Extrusion Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Extrusão Rotacional</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1366"/>
        <source>Ruler Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro Régua</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1381"/>
        <source>Scatter Plot</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Gráfico de Dispersão</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1384"/>
        <source>Shrink Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Redução</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1387"/>
        <source>Slice Along Poly Line</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Fatiar ao longo da Polilinha</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1393"/>
        <source>Smooth Poly Data Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Suavização de Dados Poligonais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1399"/>
        <source>Stitch Image Data With Ghosts</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Unir Dados de Imagem com Células Fantasmas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1408"/>
        <source>Stripper</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Divisor</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1411"/>
        <source>Structured Grid Outline Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Contorno de Malha Estruturada</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1414"/>
        <source>Subdivide</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Subdividir</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="634"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1423"/>
        <source>Surface Vectors</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Vetores de Superfície</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1429"/>
        <source>Table FFT</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Tabela FFT</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1432"/>
        <source>Table To Poly Data</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Tabela para Dados Poligonais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1435"/>
        <source>Table To Structured Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Tabela para Malha Estruturada</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1459"/>
        <source>Tensor Glyph</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Glifo Tensorial</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="637"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1462"/>
        <source>Tensor Principal Invariants</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Invariantes Principais do Tensor</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1465"/>
        <source>Tessellator Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro de Tessellação</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="250"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1468"/>
        <source>Texture Map To Cylinder</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mapa de Textura para Cilindro</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="253"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1471"/>
        <source>Texture Map To Plane</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mapa de Textura para Plano</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="256"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1474"/>
        <source>Texture Map To Sphere</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Mapa de Textura para Esfera</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1489"/>
        <source>Transform Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro Transformar</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1495"/>
        <source>Triangle Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro Triângulo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1498"/>
        <source>Tube Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Filtro Tubo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="538"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1501"/>
        <source>Validate Cells</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Validar Células</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1504"/>
        <source>Volume Of Revolution</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Volume de Revolução</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1507"/>
        <source>Vortex Cores</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Núcleos de Vórtice</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1510"/>
        <source>Warp Scalar</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Deformação Escalar</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="640"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1516"/>
        <source>Yield Criteria</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Critério de Escoamento</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="43"/>
        <source>Data Model</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="70"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="103"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="763"/>
        <source>Axis Aligned Slice</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="79"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="655"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1204"/>
        <source>Overlapping AMR Level Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="85"/>
        <source>Molecule</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="139"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1087"/>
        <source>Hyper Tree Grid Generate Fields</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="154"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1102"/>
        <source>Hyper Tree Grid Visible Leaves Size</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="211"/>
        <source>Display</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="241"/>
        <source>Textures</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="244"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1417"/>
        <source>Surface Normals</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="247"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1420"/>
        <source>Surface Tangents</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="259"/>
        <source>Charts</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="280"/>
        <source>Domain</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="349"/>
        <source>Distributed</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="358"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1051"/>
        <source>Ghost Cells</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="361"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="661"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1300"/>
        <source>Process Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="436"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="877"/>
        <source>Critical Time</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="445"/>
        <source>Force Static Mesh</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="484"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1450"/>
        <source>Temporal Smoothing</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="499"/>
        <source>Mesh</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="502"/>
        <source>Analysis</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="505"/>
        <source>Cell Quality</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="511"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="844"/>
        <source>Connected Surface Properties</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="514"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="868"/>
        <source>Coordinates</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="535"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1288"/>
        <source>Polyline Length</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="541"/>
        <source>Extraction</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="562"/>
        <source>Programmable</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="580"/>
        <source>Data Array</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="583"/>
        <source>Array Generation</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="598"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1396"/>
        <source>Spatio Temporal Harmonics</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="601"/>
        <source>Array Forwarding</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="610"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1006"/>
        <source>Field Arrays From File</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="622"/>
        <source>Array Quality</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="643"/>
        <source>Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="646"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="769"/>
        <source>Block Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="649"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1258"/>
        <source>Point And Cell Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="652"/>
        <source>Global Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="664"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1003"/>
        <source>Feature Edges Region Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="760"/>
        <source>Axis Aligned Reflection Filter</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="793"/>
        <source>Cell Grid Centers</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="796"/>
        <source>Cell Grid Point Probe</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="799"/>
        <source>Cell Grid To Unstructured Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="802"/>
        <source>Cell Grid Transform</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="805"/>
        <source>Cell Grid Warp</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="826"/>
        <source>Compute Cell Grid Elevation</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="829"/>
        <source>Compute Cell Grid Sides</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="934"/>
        <source>Explode Data Set</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1054"/>
        <source>Global Point And Cell Ids</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1519"/>
        <source>Youngs Material Interface</source>
        <extracomment>Real source: Clients/ParaView/ParaViewFilters.xml - Clients/ParaView/ParaViewFilters.xml</extracomment>
        <translation>Interface de Materiais Elásticos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1522"/>
        <source>A&amp;nnotation</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>A&amp;notação</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1525"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1747"/>
        <source>Vector Text</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Texto Vetorial</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1528"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1741"/>
        <source>Time To Text Convertor Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Conversora de Tempo para Texto</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1531"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1639"/>
        <source>Arrow Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte de Setas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1534"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1687"/>
        <source>Logo Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte do Logotipo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1537"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1735"/>
        <source>Text Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte de Texto</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1540"/>
        <source>&amp;Data Objects</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>&amp;Objetos de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1543"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1636"/>
        <source>AMR Gaussian Pulse Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte de Pulso Gaussiano AMR</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1546"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1657"/>
        <source>Data Object Generator</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Gerador de Objeto de Dados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1549"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1666"/>
        <source>Fast Uniform Grid</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Malha Uniforme Rápida</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1552"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1672"/>
        <source>Hierarchical Fractal</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fractal Hierárquico</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1555"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1675"/>
        <source>Hyper Tree Grid Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte de Malha de Hiperárvore</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1558"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1678"/>
        <source>Image Mandelbrot Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte de Imagem Mandelbrot</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1561"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1684"/>
        <source>Live Programmable Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Programável ao Vivo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1564"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1714"/>
        <source>Random Hyper Tree Grid Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte de Malha de Hiperárvore Aleatória</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1567"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1711"/>
        <source>RT Analytic Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Analítica de TR</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1570"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1723"/>
        <source>Spatio Temporal Harmonics Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte de Harmônicos Espaço-Temporais</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1573"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1744"/>
        <source>Unstructured Cell Types</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Tipos de Células Não Estruturadas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1576"/>
        <source>&amp;Geometric Shapes</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>&amp;Formas Geométricas</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1579"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1651"/>
        <source>Cube Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Cubo</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1582"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1648"/>
        <source>Cone Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Cone</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1585"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1654"/>
        <source>Cylinder Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Cilindro</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1588"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1660"/>
        <source>Disk Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Disco</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1591"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1663"/>
        <source>Ellipse Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Elipse</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1594"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1681"/>
        <source>Line Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Linha</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1597"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1690"/>
        <source>Outline Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Contorno</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1600"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1696"/>
        <source>Plane Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Plano</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1603"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1699"/>
        <source>Point Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Ponto</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1606"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1702"/>
        <source>Poly Line Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Polilinha</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1609"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1705"/>
        <source>Poly Point Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Poliponto</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1612"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1726"/>
        <source>Sphere Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Esfera</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1615"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1729"/>
        <source>Spline Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Spline</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1618"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1732"/>
        <source>Superquadric Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Superquadrática</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1621"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1645"/>
        <source>Cell Grid Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1624"/>
        <source>M&amp;easurement Tools</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Ferram&amp;entas de Medição</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1627"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1642"/>
        <source>Axes</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Eixos</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1630"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1717"/>
        <source>Ruler</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Régua</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1633"/>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1720"/>
        <source>Protractor</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Transferidor</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1669"/>
        <source>Glyph Source2D</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte de Glifos 2D</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1693"/>
        <source>Partitioned Data Set Collection Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Coleção Conjunto de Dados Particionados</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1708"/>
        <source>Programmable Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte Programável</translation>
    </message>
    <message>
        <location filename="../build/Clients/ParaView/translationSourcesparaviewClientXMLs.h" line="1738"/>
        <source>Time Source</source>
        <extracomment>Real source: Clients/ParaView/ParaViewSources.xml - Clients/ParaView/ParaViewSources.xml</extracomment>
        <translation>Fonte de Tempo</translation>
    </message>
</context>
</TS>
