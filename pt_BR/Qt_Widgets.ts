<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>QuickLaunchDialog</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqQuickLaunchDialog.ui" line="16"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
</context>
<context>
    <name>SeriesGeneratorDialog</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="16"/>
        <source>Generate Number Series</source>
        <translation>Gerar Série Numérica</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="23"/>
        <source>Linear</source>
        <translation>Linear</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="28"/>
        <source>Logarithmic</source>
        <translation>Logarítmico</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="33"/>
        <source>Geometric (samples)</source>
        <translation>Geométrico (amostras)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="38"/>
        <source>Geometric (common ratio)</source>
        <translation>Geométrico (razão comum)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="58"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="81"/>
        <source>Reset using current data range values</source>
        <translation>Redefinir usando valores atuais do intervalo de dados</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="90"/>
        <source>Type:</source>
        <translation>Tipo:</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="97"/>
        <source>Number of Samples:</source>
        <translation>Número de Amostras:</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="117"/>
        <source>Common Ratio:</source>
        <translation>Razão Comum:</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="124"/>
        <source>1.1</source>
        <translation>1.1</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="146"/>
        <source>Range</source>
        <translation>Intervalo</translation>
    </message>
</context>
<context>
    <name>pqColorChooserButton</name>
    <message>
        <location filename="../Qt/Widgets/pqColorChooserButton.cxx" line="154"/>
        <source>Set Color</source>
        <translation>Definir Cor</translation>
    </message>
</context>
<context>
    <name>pqExpanderButton</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqExpanderButton.ui" line="68"/>
        <source>Properties</source>
        <translation>Propriedades</translation>
    </message>
</context>
<context>
    <name>pqProgressWidget</name>
    <message>
        <location filename="../Qt/Widgets/pqProgressWidget.cxx" line="114"/>
        <source>Abort</source>
        <translation>Abortar</translation>
    </message>
</context>
<context>
    <name>pqQuickLaunchDialog</name>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="81"/>
        <source>Type to search.</source>
        <translation>Digite para pesquisar.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="82"/>
        <source>Enter</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="83"/>
        <source>to create selected source/filter.</source>
        <translation>para criar a fonte/filtro selecionado.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="84"/>
        <source>Shift + Enter</source>
        <translation>Shift + Enter</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="85"/>
        <source>to create and apply selected source/filter. </source>
        <translation>para criar e aplicar a fonte/filtro selecionado. </translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="87"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="88"/>
        <source>to cancel.</source>
        <translation>para cancelar.</translation>
    </message>
</context>
<context>
    <name>pqScaleByButton</name>
    <message>
        <location filename="../Qt/Widgets/pqScaleByButton.cxx" line="46"/>
        <source>Scale by ...</source>
        <translation>Escalar por ...</translation>
    </message>
</context>
<context>
    <name>pqSeriesGeneratorDialog</name>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="56"/>
        <source>Error: range cannot contain 0 for log.</source>
        <translation>Erro: intervalo não pode conter 0 (zero) para logaritmo.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="63"/>
        <source>Error: range cannot begin or end with 0 for a geometric series.</source>
        <translation>Erro: intervalo não pode começar ou terminar com 0 (zero) para uma série geométrica.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="67"/>
        <source>Error: range cannot contain 0 for a geometric series.</source>
        <translation>Erro: intervalo não pode conter 0 (zero) para uma série geométrica.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="74"/>
        <source>Error: range cannot begin with 0 for a geometric series.</source>
        <translation>Erro: o intervalo não pode começar com 0 (zero) para uma série geométrica.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="78"/>
        <source>Error: common ratio cannot be 0 for a geometric series.</source>
        <translation>Erro: a razão comum não pode ser 0 (zero) para uma série geométrica.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="93"/>
        <source>Sample series: </source>
        <translation>Série de amostragem: </translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="191"/>
        <source>&amp;Generate</source>
        <translation>&amp;Gerar</translation>
    </message>
</context>
<context>
    <name>pqTreeViewSelectionHelper</name>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="154"/>
        <source>Filter items (regex)</source>
        <translation>Filtrar itens (expressão regular)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="181"/>
        <source>Check highlighted items</source>
        <translation>Marcar itens destacados</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="189"/>
        <source>Uncheck highlighted items</source>
        <translation>Desmarcar itens destacados</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="211"/>
        <source>Sort (ascending)</source>
        <translation>Ordenar (crescente)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="212"/>
        <source>Sort (descending)</source>
        <translation>Ordenar (decrescente)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="222"/>
        <source>Clear sorting</source>
        <translation>Limpar ordenação</translation>
    </message>
</context>
<context>
    <name>pqTreeWidgetSelectionHelper</name>
    <message>
        <location filename="../Qt/Widgets/pqTreeWidgetSelectionHelper.cxx" line="66"/>
        <source>Check</source>
        <translation>Marcar</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeWidgetSelectionHelper.cxx" line="67"/>
        <source>Uncheck</source>
        <translation>Desmarcar</translation>
    </message>
</context>
</TS>
