<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>AbortAnimation</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="19"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="39"/>
        <source>Abort Saving Animation</source>
        <translation>Abortar Salvamento da Animação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="42"/>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="45"/>
        <source>Interrupts the saving of animation and aborts it.</source>
        <translation>Interrompe o salvamento da animação e a aborta.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="48"/>
        <source>Abort Animation</source>
        <translation>Abortar Animação</translation>
    </message>
</context>
<context>
    <name>AnimationTimeWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="22"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="43"/>
        <source>Time:</source>
        <translation>Tempo:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="70"/>
        <source>max is N</source>
        <translation>o máximo é N</translation>
    </message>
</context>
<context>
    <name>CalculatorWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="45"/>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="61"/>
        <source>sin</source>
        <translation>seno</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="77"/>
        <source>asin</source>
        <translation>arcseno</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="93"/>
        <source>sinh</source>
        <translation>seno hiperbólico</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="109"/>
        <source>dot</source>
        <translation>produto escalar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="132"/>
        <source>(</source>
        <translation>(</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="148"/>
        <source>cos</source>
        <translation>cos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="164"/>
        <source>acos</source>
        <translation>acos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="180"/>
        <source>cosh</source>
        <translation>cosh</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="196"/>
        <source>mag</source>
        <translation>mag</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="216"/>
        <source>)</source>
        <translation>)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="232"/>
        <source>tan</source>
        <translation>tan</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="248"/>
        <source>atan</source>
        <translation>atan</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="264"/>
        <source>tanh</source>
        <translation>tanh</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="280"/>
        <source>norm</source>
        <translation>norm</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="300"/>
        <source>iHat</source>
        <translation>vetor unitário i</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="316"/>
        <source>abs</source>
        <translation>abs</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="332"/>
        <source>ceil</source>
        <translation>ceil</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="348"/>
        <source>x^y</source>
        <translation>x^y</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="364"/>
        <source>ln</source>
        <translation>ln</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="384"/>
        <source>jHat</source>
        <translation>vetor unitário j</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="400"/>
        <source>sqrt</source>
        <translation>raiz quadrada</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="416"/>
        <source>floor</source>
        <translation>floor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="432"/>
        <source>exp</source>
        <translation>exp</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="448"/>
        <source>log10</source>
        <translation>log10</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="468"/>
        <source>kHat</source>
        <translation>vetor unitário k</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="484"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="500"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="516"/>
        <source>*</source>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="532"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="548"/>
        <source>Scalars</source>
        <translation>Escalares</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="558"/>
        <source>Vectors</source>
        <translation>Vetores</translation>
    </message>
</context>
<context>
    <name>CameraKeyFrameWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="16"/>
        <source>Camera Animation</source>
        <translation>Animação da Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="38"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="43"/>
        <source>Camera Position</source>
        <translation>Posição da Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="48"/>
        <source>Camera Focus</source>
        <translation>Foco da Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="53"/>
        <source>Up Direction</source>
        <translation>Direção Para Cima</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="71"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:600;&quot;&gt;Define Camera Parameters&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt;&quot;&gt;Using the left pane, edit the path followed by the camera&apos;s position and focal point for the keyframe being edited.&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:600;&quot;&gt;Definir Parâmetros da Câmera&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt;&quot;&gt;Usando o painel esquerdo, edite o caminho seguido pela posição da câmera e pelo ponto focal para o keyframe que está sendo editado.&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="94"/>
        <source>Position Control Points</source>
        <translation>Pontos de Controle de Posição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="111"/>
        <source>Focus Control Points</source>
        <translation>Pontos de Controle de Foco</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="185"/>
        <source>Position</source>
        <translation>Posição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="201"/>
        <source>Focal Point</source>
        <translation>Ponto Focal</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="217"/>
        <source>View Up</source>
        <translation>Visualização Para Cima</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="233"/>
        <source>View Angle</source>
        <translation>Ângulo de Visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="240"/>
        <source>Parallel Scale</source>
        <translation>Escala Paralela</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="260"/>
        <source>Use Current</source>
        <translation>Usar Atual</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="267"/>
        <source>Apply to camera</source>
        <translation>Aplicar à câmera</translation>
    </message>
</context>
<context>
    <name>FindDataCurrentSelectionFrame</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="31"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Attribute:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Atributo:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="73"/>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="76"/>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="79"/>
        <source>Toggle column visibility</source>
        <translation>Alternar visibilidade da coluna</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="93"/>
        <source>Toggle field data visibility</source>
        <translation>Alternar visibilidade dos dados do campo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="96"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="113"/>
        <source>Invert the selection</source>
        <translation>Inverter a seleção</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="116"/>
        <source>Invert selection</source>
        <translation>Inverter seleção</translation>
    </message>
</context>
<context>
    <name>FindDataSelectionDisplayFrame</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Selection Labels&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Etiquetas de Seleção&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="48"/>
        <source>&lt;p&gt;Set the array to label selected cells with&lt;/p&gt;</source>
        <translation>&lt;p&gt;Defina a matriz para rotular as células selecionadas com&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="51"/>
        <source>Cell Labels</source>
        <translation>Rótulos de Célula</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="62"/>
        <source>&lt;p&gt;Set the array to label to selected points with&lt;/p&gt;</source>
        <translation>&lt;p&gt;Defina a matriz para rotular os pontos selecionados com&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="65"/>
        <source>Point Labels</source>
        <translation>Rótulos de Ponto</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="78"/>
        <source>Edit selection label properties</source>
        <translation>Editar propriedades do rótulo de seleção</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="81"/>
        <source>Edit Label Properties</source>
        <translation>Editar Propriedades de Rótulo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="97"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Selection Appearance&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Aparência da Seleção&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="122"/>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="162"/>
        <source>&lt;p&gt;Set the color to use to show selected elements&lt;/p&gt;</source>
        <translation>&lt;p&gt;Defina a cor a ser usada para mostrar os elementos selecionados&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="125"/>
        <source>Selection Color</source>
        <translation>Cor da Seleção</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="137"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Interactive Selection&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Seleção Interativa&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="165"/>
        <source>Interactive Selection Color</source>
        <translation>Cor da Seleção Interativa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="172"/>
        <source>Edit interactive selection label properties</source>
        <translation>Editar propriedades do rótulo de seleção interativa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="175"/>
        <source>Edit Interactive Label Properties</source>
        <translation>Editar Propriedades do Rótulo Interativo</translation>
    </message>
</context>
<context>
    <name>LightsEditor</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="22"/>
        <source>Lights Editor</source>
        <translation>Editor de Iluminação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="61"/>
        <source>Turn on or off all the lights in the lighting kit.</source>
        <translation>Ligar ou desligar todas as luzes no kit de iluminação.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="64"/>
        <source>Light Kit</source>
        <translation>Kit de Iluminação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="74"/>
        <source>Reset lights to their default values</source>
        <translation>Redefinir luzes para os valores padrão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="77"/>
        <source>Reset</source>
        <translation>Redefinir</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="102"/>
        <source>The two back lights, one on the left of the object as seen from the observer and one on the right, fill on the high-contrast areas behind the object. </source>
        <translation>As duas luzes traseiras, uma à esquerda do objeto visto do observador e uma à direita, preenchem as áreas de alto contraste atrás do objeto. </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="105"/>
        <source>Back</source>
        <translation>Traseira</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="118"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="376"/>
        <source>Set the Key-to-Head ratio. Similar to the Key-to-Fill ratio, this ratio controls how bright the headlight light is compared to the key light: larger values correspond to a dimmer headlight light. The headlight is special kind of fill light, lighting only the parts of the object that the camera can see. As such, a headlight tends to reduce the contrast of a scene. It can be used to fill in &quot;shadows&quot; of the object missed by the key and fill lights. The headlight should always be significantly dimmer than the key light: ratios of 2 to 15 are typical.</source>
        <translation>Defina a proporção Chave-para-Farol. Semelhante à proporção Chave-para-Preencher, esta proporção controla o quão brilhante é a luz do farol em comparação com a luz chave: valores maiores correspondem a uma luz de farol mais fraca. O farol é um tipo especial de luz de preenchimento, iluminando apenas as partes do objeto que a câmera pode ver. Como tal, um farol tende a reduzir o contraste de uma cena. Ele pode ser usado para preencher &quot;sombras&quot; do objeto não atingidas pelas luzes chave e de preenchimento. O farol deve sempre ser significativamente mais fraco que a luz chave: proporções de 2 a 15 são típicas.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="131"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="451"/>
        <source>The &quot;Warmth&quot; of the Fill Light. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation>A &quot;Caloridade&quot; da Luz de Preenchimento. Caloridade é um parâmetro que varia de 0 a 1, onde 0 é &quot;frio&quot; (parece gélido ou iluminado por um céu muito azul), 1 é &quot;quente&quot; (o vermelho de um pôr-do-sol muito vermelho, ou as brasas de uma fogueira) e 0,5 é um branco neutro. A escala de caloridade é não linear. Valores de caloridade próximos a 0,5 são sutilmente &quot;mais quentes&quot; ou &quot;mais frios&quot;, muito parecidos com uma lâmpada incandescente de tungstênio mais quente, um halógeno mais frio ou a luz do dia (ainda mais fria). Movendo-se ainda mais para longe de 0,5, as cores variam mais rapidamente em direção aos azuis e vermelhos. Com relação à estética, extremos de caloridade devem ser usados com moderação. </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="134"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="227"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="522"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="532"/>
        <source>Warm</source>
        <translation>Quente</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="141"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="272"/>
        <source>The Fill Light Azimuth. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation>O Azimute da Luz de Preenchimento. Por simplicidade, a posição das luzes no Kit de Iluminação só pode ser especificada usando ângulos: a elevação (latitude) e o azimute (longitude) de cada luz em relação à câmera, expressos em graus. (As luzes sempre brilham no ponto de olhar da câmera.) Por exemplo, uma luz em (elevação=0, azimute=0) está localizada na câmera (um farol). Uma luz em (elevação=90, azimute=0) está acima do ponto de olhar, brilhando para baixo. Valores negativos de azimute movem as luzes no sentido horário conforme visto acima, valores positivos no sentido anti-horário. Então, uma luz em (elevação=45, azimute=-20) está acima e na frente do objeto e brilhando levemente do lado esquerdo.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="144"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="154"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="542"/>
        <source>Azi</source>
        <translation>Azi</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="151"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="357"/>
        <source>The Key Light Azimuth. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation>O Azimute da Luz Chave. Por simplicidade, a posição das luzes no Kit de Iluminação só pode ser especificada usando ângulos: a elevação (latitude) e o azimute (longitude) de cada luz em relação à câmera, expressos em graus. (As luzes sempre brilham no ponto de olhar da câmera.) Por exemplo, uma luz em (elevação=0, azimute=0) está localizada na câmera (um farol). Uma luz em (elevação=90, azimute=0) está acima do ponto de olhar, brilhando para baixo. Valores negativos de azimute movem as luzes no sentido horário conforme visto acima, valores positivos no sentido anti-horário. Então, uma luz em (elevação=45, azimute=-20) está acima e na frente do objeto e brilhando levemente do lado esquerdo.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="167"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="489"/>
        <source>The Back Light Elevation. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation>A Elevação da Luz Traseira. Por simplicidade, a posição das luzes no Kit de Iluminação só pode ser especificada usando ângulos: a elevação (latitude) e o azimute (longitude) de cada luz em relação à câmera, expressos em graus. (As luzes sempre brilham no ponto de olhar da câmera.) Por exemplo, uma luz em (elevação=0, azimute=0) está localizada na câmera (um farol). Uma luz em (elevação=90, azimute=0) está acima do ponto de olhar, brilhando para baixo. Valores negativos de azimute movem as luzes no sentido horário conforme visto acima, valores positivos no sentido anti-horário. Então, uma luz em (elevação=45, azimute=-20) está acima e na frente do objeto e brilhando levemente do lado esquerdo.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="192"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="425"/>
        <source>The Intensity of the Key Light.  0 = off and 1 = full intensity.</source>
        <translation>A Intensidade da Luz Chave. 0 = desligada e 1 = intensidade máxima.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="211"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="499"/>
        <source>Set the Key-to-Back Ratio. This ratio controls how bright the back lights are compared to the key light: larger values correspond to dimmer back lights. The back lights fill in the remaining high-contrast regions behind the object. Values between 2 and 10 are good.</source>
        <translation>Defina a Proporção Chave-para-Traseira. Esta proporção controla o quão brilhantes são as luzes traseiras em comparação com a luz chave: valores maiores correspondem a luzes traseiras mais fracas. As luzes traseiras preenchem as regiões de alto contraste remanescentes atrás do objeto. Valores entre 2 e 10 são bons.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="224"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="392"/>
        <source>The &quot;Warmth&quot; of the Headlight. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation>A &quot;Caloridade&quot; do Farol. Caloridade é um parâmetro que varia de 0 a 1, onde 0 é &quot;frio&quot; (parece gélido ou iluminado por um céu muito azul), 1 é &quot;quente&quot; (o vermelho de um pôr-do-sol muito vermelho, ou as brasas de uma fogueira) e 0,5 é um branco neutro. A escala de caloridade é não linear. Valores de caloridade próximos a 0,5 são sutilmente &quot;mais quentes&quot; ou &quot;mais frios&quot;, muito parecidos com uma lâmpada incandescente de tungstênio mais quente, um halógeno mais frio ou a luz do dia (ainda mais fria). Movendo-se ainda mais para longe de 0,5, as cores variam mais rapidamente em direção aos azuis e vermelhos. Com relação à estética, extremos de caloridade devem ser usados com moderação. </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="240"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="519"/>
        <source>The &quot;Warmth&quot; of the Key Light. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation>A &quot;Caloridade&quot; da Luz Chave. Caloridade é um parâmetro que varia de 0 a 1, onde 0 é &quot;frio&quot; (parece gélido ou iluminado por um céu muito azul), 1 é &quot;quente&quot; (o vermelho de um pôr-do-sol muito vermelho, ou as brasas de uma fogueira) e 0,5 é um branco neutro. A escala de caloridade é não linear. Valores de caloridade próximos a 0,5 são sutilmente &quot;mais quentes&quot; ou &quot;mais frios&quot;, muito parecidos com uma lâmpada incandescente de tungstênio mais quente, um halógeno mais frio ou a luz do dia (ainda mais fria). Movendo-se ainda mais para longe de 0,5, as cores variam mais rapidamente em direção aos azuis e vermelhos. Com relação à estética, extremos de caloridade devem ser usados com moderação. </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="256"/>
        <source>The Fill Light is usually positioned across from or opposite from the key light (though still on the same side of the object as the camera) in order to simulate diffuse reflections from other objects in the scene.</source>
        <translation>A Luz de Preenchimento geralmente é posicionada em frente ou oposta à luz-chave (embora ainda do mesmo lado do objeto da câmera) para simular reflexões difusas de outros objetos na cena.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="259"/>
        <source>Fill</source>
        <translation>Preenchimento</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="297"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="405"/>
        <source>The Fill Light Elevation. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation>A Elevação da Luz de Preenchimento. Por simplicidade, a posição das luzes no Kit de Iluminação só pode ser especificada usando ângulos: a elevação (latitude) e o azimute (longitude) de cada luz em relação à câmera, expressos em graus. (As luzes sempre brilham no ponto de olhar da câmera.) Por exemplo, uma luz em (elevação=0, azimute=0) está localizada na câmera (um farol). Uma luz em (elevação=90, azimute=0) está acima do ponto de olhar, brilhando para baixo. Valores negativos de azimute movem as luzes no sentido horário conforme visto acima, valores positivos no sentido anti-horário. Então, uma luz em (elevação=45, azimute=-20) está acima e na frente do objeto e brilhando levemente do lado esquerdo.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="322"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="509"/>
        <source>The Key Light Elevation. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation>A Elevação da Luz Chave. Por simplicidade, a posição das luzes no Kit de Iluminação só pode ser especificada usando ângulos: a elevação (latitude) e o azimute (longitude) de cada luz em relação à câmera, expressos em graus. (As luzes sempre brilham no ponto de olhar da câmera.) Por exemplo, uma luz em (elevação=0, azimute=0) está localizada na câmera (um farol). Uma luz em (elevação=90, azimute=0) está acima do ponto de olhar, brilhando para baixo. Valores negativos de azimute movem as luzes no sentido horário conforme visto acima, valores positivos no sentido anti-horário. Então, uma luz em (elevação=45, azimute=-20) está acima e na frente do objeto e brilhando levemente do lado esquerdo.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="341"/>
        <source>The Key Light is the main light.  It is usually positioned so that it appears like an overhead light (like the sun or a ceiling light).  It is generally positioned to shine down from about a 45 degree angle vertically and at least a little offset side to side.  The key light is usually at least about twice as bright as the total of all other lights in the scene to provide good modeling of object features.</source>
        <translation>A Luz Chave é a luz principal. Ela geralmente é posicionada de modo que pareça uma luz superior (como o sol ou uma luz de teto). Ela é geralmente posicionada para brilhar de um ângulo de cerca de 45 graus verticalmente e pelo menos um pouco deslocada lateralmente. A luz-chave geralmente é pelo menos cerca de duas vezes mais brilhante que o total de todas as outras luzes na cena para fornecer uma boa modelagem dos recursos do objeto.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="344"/>
        <source>Key</source>
        <translation>Chave</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="379"/>
        <source>K:H</source>
        <translation>Ch:Fa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="408"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="492"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="512"/>
        <source>Ele</source>
        <translation>Ele</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="415"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="584"/>
        <source>Set the Key-to-Fill Ratio. This ratio controls how bright the fill light is compared to the key light: larger values correspond to a dimmer fill light. The purpose of the fill light is to light parts of the object not lit by the key light, while still maintaining contrast. This type of lighting may correspond to indirect illumination from the key light, bounced off a wall, floor, or other object. The fill light should never be brighter than the key light: a good range for the key-to-fill ratio is between 2 and 10.</source>
        <translation>Defina a Proporção Chave-para-Preenchimento. Esta proporção controla o quão brilhante é a luz de preenchimento em comparação com a luz chave: valores maiores correspondem a uma luz de preenchimento mais fraca. O objetivo da luz de preenchimento é iluminar partes do objeto não iluminadas pela luz chave, mantendo ainda o contraste. Este tipo de iluminação pode corresponder à iluminação indireta da luz chave, refletida de uma parede, piso ou outro objeto. A luz de preenchimento nunca deve ser mais brilhante que a luz chave: uma boa faixa para a proporção chave-para-preenchimento está entre 2 e 10.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="418"/>
        <source>K:F</source>
        <translation>Ch:Pr</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="428"/>
        <source>Int</source>
        <translation>Int</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="435"/>
        <source>The headlight, always located at the position of the camera, reduces the contrast between areas lit by the key and fill light. </source>
        <translation>O farol, sempre localizado na posição da câmera, reduz o contraste entre áreas iluminadas pela luz chave e de preenchimento. </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="438"/>
        <source>Head</source>
        <translation>Farol</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="470"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="539"/>
        <source>The Back Light Azimuth. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation>O azimute da Luz de Fundo. Por simplicidade, a posição das luzes no LightKit pode ser especificada apenas usando ângulos: a elevação (latitude) e o azimute (longitude) de cada luz em relação à câmera, expressos em graus. (As luzes sempre brilham no ponto de olhar da câmera). Por exemplo, uma luz em (elevação=0, azimute=0) está localizada na câmera (uma luz frontal). Uma luz em (elevação=90, azimute=0) está acima do ponto de olhar, brilhando para baixo. Valores negativos de azimute movem as luzes no sentido horário vistas de cima, valores positivos no sentido anti-horário. Portanto, uma luz em (elevação=45, azimute=-20) está acima e na frente do objeto, brilhando ligeiramente do lado esquerdo.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="502"/>
        <source>K:B</source>
        <translation>K:B</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="529"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="565"/>
        <source>The &quot;Warmth&quot; of the Back Light. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation>O &quot;Calor&quot; da Luz de Fundo. O calor é um parâmetro que varia de 0 a 1, onde 0 é &quot;frio&quot; (parece gelado ou iluminado por um céu muito azul), 1 é &quot;quente&quot; (o vermelho de um pôr do sol muito vermelho, ou as brasas de uma fogueira), e 0,5 é um branco neutro. A escala de calor é não linear. Valores de calor próximos a 0,5 são sutilmente &quot;mais quentes&quot; ou &quot;mais frios&quot;, muito parecidos com uma lâmpada incandescente de tungstênio mais quente, um halogênio mais frio ou a luz do dia (ainda mais fria). Afastando-se ainda mais de 0,5, as cores variam mais rapidamente em direção aos azuis e vermelhos. No que diz respeito à estética, os extremos de calor devem ser usados com moderação. </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="549"/>
        <source>If Maintain Luminance is set, the LightKit will attempt to maintain the apparent intensity of lights based on their perceptual brightnesses.</source>
        <translation>Se Manter Luminância estiver definido, o LightKit tentará manter a intensidade aparente das luzes com base em seus brilhos perceptivos.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="552"/>
        <source>Maintain Luminance</source>
        <translation>Manter Luminância</translation>
    </message>
</context>
<context>
    <name>LightsInspector</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsInspector.ui" line="16"/>
        <source>Light Inspector</source>
        <translation>Inspetor de Luz</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsInspector.ui" line="25"/>
        <source>Add Light</source>
        <translation>Adicionar Luz</translation>
    </message>
</context>
<context>
    <name>MultiBlockInspectorWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="16"/>
        <source>MultiBlock Inspector</source>
        <translation>Inspetor de MultiBloco</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="43"/>
        <source>Extract Blocks</source>
        <translation>Extrair Blocos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="65"/>
        <source>Show/Hide legend</source>
        <translation>Mostrar/Ocultar legenda</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="93"/>
        <source>State Legend:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="121"/>
        <source>At least one property is disabled because no blocks are selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="151"/>
        <source>At least one property is inherited from the representation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="181"/>
        <source>At least one property is inherited from block(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="211"/>
        <source>At least one property is inherited from block(s) and the representation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="241"/>
        <source>At least one property is set in block(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="271"/>
        <source>At least one property is set in block(s) and inherited from the representation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="301"/>
        <source>At least one property is set in block(s) and inherited from block(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="331"/>
        <source>At least one property is set in block(s) and inherited from block(s) and the representation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Color Legend:&lt;/span&gt;&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/no_color.png&quot;/&gt; - using coloring parameters from display&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/inherited_color.png&quot;/&gt; - color is inherited from a parent node or color map&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/explicit_color.png&quot;/&gt; - color explicitly specified for this node&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Opacity Legend:&lt;/span&gt;&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/no_opacity.png&quot;/&gt; - using opacity parameters from display&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/inherited_opacity.png&quot;/&gt; - opacity is inherited from a parent node&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/explicit_opacity.png&quot;/&gt; - opacity explicitly specified for this node&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Legenda de Cores:&lt;/span&gt;&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/no_color.png&quot;/&gt; - usando parâmetros de coloração da exibição&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/inherited_color.png&quot;/&gt; - a cor é herdada de um nó pai ou mapa de cores&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/explicit_color.png&quot;/&gt; - cor especificada explicitamente para este nó&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Legenda de Opacidade:&lt;/span&gt;&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/no_opacity.png&quot;/&gt; - usando parâmetros de opacidade da exibição&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/inherited_opacity.png&quot;/&gt; - a opacidade é herdada de um nó pai&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/explicit_opacity.png&quot;/&gt; - opacidade especificada explicitamente para este nó&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>OrbitCreatorDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="16"/>
        <source>Create Orbit</source>
        <translation>Criar Órbita</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="22"/>
        <source>Orbit Parameters</source>
        <translation>Parâmetros da Órbita</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="28"/>
        <source>Center</source>
        <translation>Centro</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="44"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="60"/>
        <source>Origin</source>
        <translation>Origem</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="94"/>
        <source>Reset Center</source>
        <translation>Redefinir Centro</translation>
    </message>
</context>
<context>
    <name>PopoutPlaceholder</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPopoutPlaceholder.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPopoutPlaceholder.ui" line="35"/>
        <source>Layout shown in separate window</source>
        <translation>Layout mostrado em janela separada</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPopoutPlaceholder.ui" line="60"/>
        <source>Click to restore</source>
        <translation>Clique para restaurar</translation>
    </message>
</context>
<context>
    <name>PropertyLinksConnection</name>
    <message>
        <source>Change coloring</source>
        <translation type="vanished">Alterar coloração</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqDisplayRepresentationWidget.cxx" line="46"/>
        <source>Change representation type</source>
        <translation>Alterar tipo de representação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqDisplayColorWidget.cxx" line="131"/>
        <source>Change </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxyInformationWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="34"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="41"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="54"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="201"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="278"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="370"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="387"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="480"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="493"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="506"/>
        <source>TextLabel</source>
        <translation>Texto da Etiqueta</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="67"/>
        <source>0 - 100
0 - 100
0 - 100</source>
        <translation>0 - 100
0 - 100
0 - 100</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="86"/>
        <source>Hierarchy</source>
        <translation>Hierarquia</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="129"/>
        <source>Hierarchy shown here</source>
        <translation>Hierarquia mostrada aqui</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="137"/>
        <source>Assembly</source>
        <translation>Montagem</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="180"/>
        <source>Assembly shown here</source>
        <translation>Montagem mostrada aqui</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="194"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="556"/>
        <source>(n/a)</source>
        <translation>(n/d)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="227"/>
        <source># of Rows</source>
        <translation># de Linhas</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="245"/>
        <source>Data Statistics</source>
        <translation>Estatísticas dos Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="261"/>
        <source># of Vertices</source>
        <translation># de Vértices</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="268"/>
        <source>Extents</source>
        <translation>Extensões</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="302"/>
        <source>Data Arrays</source>
        <translation>Arrays de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="329"/>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="345"/>
        <source>0.000	- 0.000
100.000	- 100.000
200.000	- 200.00</source>
        <translation>0.000	- 0.000
100.000	- 100.000
200.000	- 200.00</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="363"/>
        <source># of Cells</source>
        <translation># de Células</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="380"/>
        <source># of Points</source>
        <translation># de Pontos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="400"/>
        <source># of Edges</source>
        <translation># de Bordas</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="437"/>
        <source>Data Grouping</source>
        <translation>Agrupamento de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="453"/>
        <source>Memory:</source>
        <translation>Memória:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="460"/>
        <source>Bounds</source>
        <translation>Limites</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="470"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="530"/>
        <source>File Properties</source>
        <translation>Propriedades do Arquivo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="546"/>
        <source>Path</source>
        <translation>Caminho</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="563"/>
        <source># of TimeSteps</source>
        <translation># de Passos de Tempo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="570"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="577"/>
        <source>Current Time</source>
        <translation>Tempo Atual</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="584"/>
        <source>1 (range: [0, 1])</source>
        <translation>1 (intervalo: [0, 1])</translation>
    </message>
</context>
<context>
    <name>ProxySelectionWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxySelectionWidget.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxySelectionWidget.ui" line="33"/>
        <source>TextLabel</source>
        <translation>Texto da Etiqueta</translation>
    </message>
</context>
<context>
    <name>ProxyWidgetDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="16"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="59"/>
        <source>Restore application default setting values</source>
        <translation>Restaurar valores padrão de configuração do aplicativo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="69"/>
        <source>Save current settings values as default</source>
        <translation>Salvar valores de configurações atuais como padrão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="98"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="110"/>
        <source>Reset</source>
        <translation>Redefinir</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="123"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="136"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
</context>
<context>
    <name>PythonAnimationCue</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPythonAnimationCue.ui" line="16"/>
        <source>Edit Python Animation Track</source>
        <translation>Editar Trilha de Animação Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPythonAnimationCue.ui" line="22"/>
        <source>Script:</source>
        <translation>Script:</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>unrecognized</source>
        <translation type="vanished">não reconhecido</translation>
    </message>
</context>
<context>
    <name>RescaleScalarRangeToCustomDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="8"/>
        <source>Set Range</source>
        <translation>Definir Intervalo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="14"/>
        <source>Enter the range for the color map</source>
        <translation>Insira o intervalo para o mapa de cores</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="21"/>
        <source>Enter the new range minimum for the color map here.</source>
        <translation>Insira aqui o novo valor mínimo do intervalo para o mapa de cores.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="24"/>
        <source>minimum</source>
        <translation>mínimo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="31"/>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="62"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="38"/>
        <source>Enter the new range maximum for the color map here.</source>
        <translation>Insira aqui o novo valor máximo do intervalo para o mapa de cores.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="41"/>
        <source>maximum</source>
        <translation>máximo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="48"/>
        <source>Enter the range for the opacity map</source>
        <translation>Insira o intervalo para o mapa de opacidade</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="55"/>
        <source>Enter the new range minimum for the opacity map here.</source>
        <translation>Insira aqui o novo valor mínimo do intervalo para o mapa de opacidade.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="69"/>
        <source>Enter the new range maximum for the opacity map here.</source>
        <translation>Insira aqui o novo valor máximo do intervalo para o mapa de opacidade.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="76"/>
        <source>If off lock the color map to avoid automatic rescaling</source>
        <translation>Se desmarcado, bloqueie o mapa de cores para evitar redimensionamento automático</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="79"/>
        <source>Enable automatic rescaling</source>
        <translation>Habilitar redimensionamento automático</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="91"/>
        <source>Apply rescale.</source>
        <translation>Aplicar redimensionamento.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="94"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="101"/>
        <source>Close without rescaling</source>
        <translation>Fechar sem redimensionar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="104"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="111"/>
        <source>Rescale and update automatic rescaling if changed.</source>
        <translation>Redimensionar e atualizar redimensionamento automático se alterado.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="114"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>RescaleScalarRangeToDataOverTimeDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="22"/>
        <source>Rescale range over time</source>
        <translation>Redimensionar intervalo ao longo do tempo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="37"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Determining range over all timesteps can potentially take a long time to complete. Are you sure you want to continue?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Determinar o intervalo ao longo de todos os passos de tempo pode potencialmente levar muito tempo para ser concluído. Tem certeza de que deseja continuar?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="50"/>
        <source>If off lock the color map to avoid automatic rescaling</source>
        <translation>Se desmarcado, bloqueie o mapa de cores para evitar redimensionamento automático</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="53"/>
        <source>Enable automatic rescaling</source>
        <translation>Habilitar redimensionamento automático</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="80"/>
        <source>Apply rescale</source>
        <translation>Aplicar redimensionamento</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="83"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="103"/>
        <source>Close without rescaling</source>
        <translation>Fechar sem redimensionar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="106"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="113"/>
        <source>Rescale and update automatic rescaling if changed.</source>
        <translation>Redimensionar e atualizar redimensionamento automático se alterado.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="116"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>ScalarValueListPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="52"/>
        <source>Add new entry</source>
        <translation>Adicionar nova entrada</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="55"/>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="69"/>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="83"/>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="110"/>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="124"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="66"/>
        <source>Remove current entry</source>
        <translation>Remover entrada atual</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="80"/>
        <source>Add a range of values</source>
        <translation>Adicionar um intervalo de valores</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="107"/>
        <source>Restore default values of the property</source>
        <translation>Restaurar valores padrão da propriedade</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="121"/>
        <source>Remove all entries</source>
        <translation>Remover todas as entradas</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="137"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Value Range:&lt;/span&gt;  [%1, %2]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Intervalo de Valores:&lt;/span&gt; [%1, %2]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSearchBox.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSearchBox.ui" line="28"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search for properties by name&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pesquisar propriedades por nome&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSearchBox.ui" line="31"/>
        <source>Search ... (use Esc to clear text)</source>
        <translation>Pesquisar... (use Esc para limpar texto)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSearchBox.ui" line="38"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Toggle advanced properties&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Alternar propriedades avançadas&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>SelectReaderDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="16"/>
        <source>Open Data With...</source>
        <translation>Abrir Dados Com...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="28"/>
        <source>A reader for FileName could not be found.  Please choose one:</source>
        <translation>Um leitor para FileName não pôde ser encontrado. Por favor, escolha um:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="45"/>
        <source>Opening the file with an incompatible reader may result in unpredictable behavior or a crash.  Please choose the correct reader.</source>
        <translation>Abrir o arquivo com um leitor incompatível pode resultar em um comportamento imprevisível ou travamento. Por favor, escolha o leitor correto.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="76"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="83"/>
        <source>Set reader as default</source>
        <translation>Definir leitor como padrão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="90"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>SelectionLinkDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionLinkDialog.ui" line="8"/>
        <source>Selection Link Mode</source>
        <translation>Modo de Ligação de Seleção</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionLinkDialog.ui" line="21"/>
        <source>&lt;b&gt;Link Selected Elements:&lt;/b&gt; link selection by evaluating the&lt;br/&gt;selection on the data source and select corresponding&lt;br/&gt;elements based on their indices on other linked data sources.</source>
        <translation>&lt;b&gt;Ligar Elementos Selecionados:&lt;/b&gt; ligar seleção avaliando a &lt;br/&gt;seleção na fonte de dados e selecionando elementos&lt;br/&gt;correspondentes com base em seus índices em outras fontes de dados ligadas.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionLinkDialog.ui" line="38"/>
        <source>&lt;b&gt;Link Selection:&lt;/b&gt; link selection by sharing the actual selection&lt;br/&gt;between the data sources. The selection is then evaluated for&lt;br/&gt;each linked sources separately.</source>
        <translation>&lt;b&gt;Ligar Seleção:&lt;/b&gt; ligar seleção compartilhando a seleção atual&lt;br/&gt;entre as fontes de dados. A seleção é então avaliada para&lt;br/&gt;cada fonte ligada separadamente.</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSettingsDialog.ui" line="16"/>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSettingsDialog.ui" line="70"/>
        <source>* Restart required for some settings to take effect</source>
        <translation>* Reiniciar requerido para algumas configurações fazer efeito</translation>
    </message>
</context>
<context>
    <name>displayRepresentationWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqDisplayRepresentationWidget.ui" line="19"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
</context>
<context>
    <name>pqAboutDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="29"/>
        <source>About ParaView</source>
        <translation>Sobre o ParaView</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="52"/>
        <source>Client Information</source>
        <translation>Informações do Cliente</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="80"/>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="122"/>
        <source>Item</source>
        <translation>Item</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="85"/>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="127"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="94"/>
        <source>Connection Information</source>
        <translation>Informações da Conexão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="154"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.kitware.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:palette(link);&quot;&gt;www.kitware.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.kitware.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:palette(link);&quot;&gt;www.kitware.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="164"/>
        <source>&lt;html&gt;&lt;b&gt;Version: &lt;i&gt;3.x.x&lt;/i&gt;&lt;/b&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Versão: &lt;i&gt;3.x.x&lt;/i&gt;&lt;/b&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="177"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.paraview.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:palette(link);&quot;&gt;www.paraview.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.paraview.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:palette(link);&quot;&gt;www.paraview.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="195"/>
        <source>Copy to Clipboard</source>
        <translation>Copiar para a Área de Transferência</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="202"/>
        <source>Save to File...</source>
        <translation>Salvar em Arquivo...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="57"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="90"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="91"/>
        <source>VTK Version</source>
        <translation>Versão VTK</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="92"/>
        <source>Qt Version</source>
        <translation>Versão Qt</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="94"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="219"/>
        <source>vtkIdType size</source>
        <translation>tamanho vtkIdType</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="94"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="219"/>
        <source>%1bits</source>
        <translation>%1bits</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="99"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="227"/>
        <source>Embedded Python</source>
        <translation>Python Embutido</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="99"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="107"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="116"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="127"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="133"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="144"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="207"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="208"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="212"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="227"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="235"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="244"/>
        <source>On</source>
        <translation>Ligado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="99"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="107"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="116"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="129"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="135"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="144"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="207"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="208"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="216"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="227"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="235"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="244"/>
        <source>Off</source>
        <translation>Desligado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="102"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="230"/>
        <source>Python Library Path</source>
        <translation>Caminho da Biblioteca Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="104"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="232"/>
        <source>Python Library Version</source>
        <translation>Versão da Biblioteca Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="107"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="235"/>
        <source>Python Numpy Support</source>
        <translation>Suporte Numpy do Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="110"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="238"/>
        <source>Python Numpy Path</source>
        <translation>Caminho Numpy do Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="112"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="240"/>
        <source>Python Numpy Version</source>
        <translation>Versão Numpy do Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="115"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="243"/>
        <source>Python Matplotlib Support</source>
        <translation>Suporte Matplotlib do Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="119"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="247"/>
        <source>Python Matplotlib Path</source>
        <translation>Caminho Matplotlib do Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="121"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="249"/>
        <source>Python Matplotlib Version</source>
        <translation>Versão Matplotlib do Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="127"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="129"/>
        <source>Python Testing</source>
        <translation>Testes Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="133"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="135"/>
        <source>MPI Enabled</source>
        <translation>MPI Habilitado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="139"/>
        <source>ParaView Build ID</source>
        <translation>ID de Build do ParaView</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="144"/>
        <source>Disable Registry</source>
        <translation>Desabilitar Registro</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="145"/>
        <source>Test Directory</source>
        <translation>Diretório de Teste</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="146"/>
        <source>Data Directory</source>
        <translation>Diretório de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="148"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="221"/>
        <source>SMP Backend</source>
        <translation>Backend SMP</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="149"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="222"/>
        <source>SMP Max Number of Threads</source>
        <translation>Número Máximo de Threads SMP</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="158"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="260"/>
        <source>OpenGL Vendor</source>
        <translation>Fornecedor OpenGL</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="159"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="261"/>
        <source>OpenGL Version</source>
        <translation>Versão OpenGL</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="160"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="262"/>
        <source>OpenGL Renderer</source>
        <translation>Renderizador OpenGL</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="164"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="166"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="287"/>
        <source>Accelerated filters overrides available</source>
        <translation>Substituições de filtros acelerados disponíveis</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="164"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="199"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="200"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="201"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="288"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="166"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="191"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="200"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="201"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="288"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="191"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="199"/>
        <source>Remote Connection</source>
        <translation>Conexão Remota</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="200"/>
        <source>Separate Render Server</source>
        <translation>Servidor de Renderização Separado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="201"/>
        <source>Reverse Connection</source>
        <translation>Conexão Reversa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="204"/>
        <source>Number of Processes</source>
        <translation>Número de Processos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="207"/>
        <source>Disable Remote Rendering</source>
        <translation>Desabilitar Renderização Remota</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="208"/>
        <source>IceT</source>
        <translation>IceT</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="212"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="216"/>
        <source>Tile Display</source>
        <translation>Exibição em Telhas</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="275"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="279"/>
        <source>Headless support</source>
        <translation>Suporte headless</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="279"/>
        <source>None</source>
        <translation>Nenhum</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="284"/>
        <source>Not supported</source>
        <translation>Não suportado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="307"/>
        <source>Client Information:
</source>
        <translation>Informações do Cliente:
</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="311"/>
        <source>
Connection Information:
</source>
        <translation>
Informações de Conexão:
</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="319"/>
        <source>Save to File</source>
        <translation>Salvar em Arquivo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="320"/>
        <source>Text Files</source>
        <translation>Arquivos de Texto</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="320"/>
        <source>All Files</source>
        <translation>Todos os Arquivos</translation>
    </message>
</context>
<context>
    <name>pqAnimationManager</name>
    <message>
        <location filename="../Qt/Components/pqAnimationManager.cxx" line="203"/>
        <source>save animation geometry from a view</source>
        <translation>salvar geometria de animação de uma vista</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationManager.cxx" line="218"/>
        <source>Saving Animation</source>
        <translation>Salvando Animação</translation>
    </message>
</context>
<context>
    <name>pqAnimationTimeWidget</name>
    <message>
        <location filename="../Qt/Components/pqAnimationTimeWidget.cxx" line="239"/>
        <source>max is %1</source>
        <translation>máx. é %1</translation>
    </message>
</context>
<context>
    <name>pqAnimationTrackEditor</name>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="64"/>
        <source>Edit Python cue</source>
        <translation>Editar dica Python</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="78"/>
        <source>Cannot edit track: python is not supported</source>
        <translation>Não é possível editar a faixa: python não é suportado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="92"/>
        <source>Data Source to Follow:</source>
        <translation>Fonte de Dados a Seguir:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="94"/>
        <source>Select Data Source</source>
        <translation>Selecionar Fonte de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="110"/>
        <source>Editing </source>
        <translation>Editando </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="122"/>
        <source>Animation Keyframes</source>
        <translation>Quadros-chave de Animação</translation>
    </message>
</context>
<context>
    <name>pqAnimationViewWidget</name>
    <message>
        <source>Mode:</source>
        <translation type="vanished">Modo:</translation>
    </message>
    <message>
        <source>Snap to Timesteps</source>
        <translation type="vanished">Ajustar aos Passos de Tempo</translation>
    </message>
    <message>
        <source>Start Time:</source>
        <translation type="vanished">Tempo Inicial:</translation>
    </message>
    <message>
        <source>Lock the start time to keep ParaView from changing it as available data times change</source>
        <translation type="vanished">Travar o tempo inicial para evitar que o ParaView o altere à medida que os tempos de dados disponíveis mudam</translation>
    </message>
    <message>
        <source>End Time:</source>
        <translation type="vanished">Tempo Final:</translation>
    </message>
    <message>
        <source>Lock the end time to keep ParaView from changing it as available data times change</source>
        <translation type="vanished">Travar o tempo final para evitar que o ParaView o altere à medida que os tempos de dados disponíveis mudam</translation>
    </message>
    <message>
        <source>Stride</source>
        <translation type="vanished">Passo</translation>
    </message>
    <message>
        <source>Data Source to Follow:</source>
        <translation type="vanished">Fonte de Dados a Seguir:</translation>
    </message>
    <message>
        <source>Select Data Source</source>
        <translation type="vanished">Selecionar Fonte de Dados</translation>
    </message>
    <message>
        <source>Editing </source>
        <translation type="vanished">Editando </translation>
    </message>
    <message>
        <source>Animation Keyframes</source>
        <translation type="vanished">Quadros-chave de Animação</translation>
    </message>
    <message>
        <source>No. Frames</source>
        <translation type="vanished">Número de Quadros</translation>
    </message>
    <message>
        <source>Toggle Animation Track</source>
        <translation type="vanished">Alternar Faixa de Animação</translation>
    </message>
    <message>
        <source>Remove Animation Track</source>
        <translation type="vanished">Remover Faixa de Animação</translation>
    </message>
    <message>
        <source>Add Animation Track</source>
        <translation type="vanished">Adicionar Faixa de Animação</translation>
    </message>
    <message>
        <source>Start %1:</source>
        <translation type="vanished">Início %1:</translation>
    </message>
    <message>
        <source>End %1:</source>
        <translation type="vanished">Fim %1:</translation>
    </message>
</context>
<context>
    <name>pqArrayListModel</name>
    <message>
        <location filename="../Qt/Components/pqArrayListWidget.cxx" line="37"/>
        <source>New Name</source>
        <translation>Novo Nome</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqArrayListWidget.cxx" line="89"/>
        <source>Array Name</source>
        <translation>Nome da Matriz</translation>
    </message>
</context>
<context>
    <name>pqCameraDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="25"/>
        <source>Adjusting Camera</source>
        <translation>Ajustando Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="68"/>
        <source>Standard Viewpoints</source>
        <translation>Pontos de Vista Padrão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="92"/>
        <source>Looking down X axis from (1, 0, 0)</source>
        <translation>Olhando para baixo no eixo X de (1, 0, 0)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="112"/>
        <source>Looking down X axis from (-1, 0, 0)</source>
        <translation>Olhando para baixo no eixo X de (-1, 0, 0)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="132"/>
        <source>Looking down Y axis from (0, 1, 0)</source>
        <translation>Olhando para baixo no eixo Y de (0, 1, 0)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="152"/>
        <source>Looking down Y axis from (0, -1, 0)</source>
        <translation>Olhando para baixo no eixo Y de (0, -1, 0)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="172"/>
        <source>Looking down X axis from (0, 0, 1)</source>
        <translation>Olhando para baixo no eixo X de (0, 0, 1)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="192"/>
        <source>Looking down Z axis from (0, 0, -1)</source>
        <translation>Olhando para baixo no eixo Z de (0, 0, -1)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="212"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="263"/>
        <source>Custom Viewpoints</source>
        <translation>Pontos de Vista Personalizados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="303"/>
        <source>Configure custom viewpoint buttons.</source>
        <translation>Configurar botões de pontos de vista personalizados.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="306"/>
        <source>Configure ...</source>
        <translation>Configurar...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="334"/>
        <source>Center of Rotation</source>
        <translation>Centro de Rotação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="369"/>
        <source>Reset center of rotation when camera is reset.</source>
        <translation>Redefinir centro de rotação quando a câmera é redefinida.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="372"/>
        <source>Reset Center of Rotation When Camera is Reset</source>
        <translation>Redefinir Centro de Rotação Quando a Câmera é Redefinida</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="398"/>
        <source>Rotation Factor</source>
        <translation>Fator de Rotação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="420"/>
        <source>Define the rotation speed factor.</source>
        <translation>Definir o fator de velocidade de rotação.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="446"/>
        <source>Camera Parameters</source>
        <translation>Parâmetros da Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="470"/>
        <source>Position</source>
        <translation>Posição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="486"/>
        <source>Focal Point</source>
        <translation>Ponto Focal</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="502"/>
        <source>View Up</source>
        <translation>Visualização Para Cima</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="518"/>
        <source>View Angle</source>
        <translation>Ângulo de Visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="535"/>
        <source>Eye Angle</source>
        <translation>Ângulo do Olho</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="542"/>
        <source>The separation between eyes (in degrees). This is used when rendering in stereo mode.</source>
        <translation>A separação entre os olhos (em graus). Isto é usado durante a renderização no modo estéreo.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="549"/>
        <source>Focal Disk</source>
        <translation>Disco Focal</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="556"/>
        <source> The size of the camera&apos;s lens in world coordinates. Affects ray traced depth of field rendering.
             </source>
        <translation> O tamanho da lente da câmera em coordenadas de mundo. Afeta a renderização de profundidade de campo rastreada por raio.
             </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="570"/>
        <source>Focal Distance</source>
        <translation>Distância Focal</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="577"/>
        <source>Depth of field rendering focus distance. Only affects ray traced renderers. 0 (default) disables it.</source>
        <translation>Distância de foco de renderização de profundidade de campo. Afetam apenas renderizadores rastreados por raio. 0 (padrão) desabilita.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="590"/>
        <source>Load camera properties.</source>
        <translation>Carregar propriedades da câmera.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="593"/>
        <source>Load</source>
        <translation>Carregar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="600"/>
        <source>Save camera properties.</source>
        <translation>Salvar propriedades da câmera.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="603"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="631"/>
        <source>Manipulate Camera</source>
        <translation>Manipular Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="657"/>
        <source>Roll</source>
        <translation>Rolagem</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="667"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="710"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="753"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="796"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="674"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="717"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="760"/>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="693"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="736"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="779"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="825"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="700"/>
        <source>Elevation</source>
        <translation>Elevação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="743"/>
        <source>Azimuth</source>
        <translation>Azimute</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="786"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="803"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="834"/>
        <source>Apply a manipulation to the current camera using the buttons on the left.</source>
        <translation>Aplicar uma manipulação à câmera atual usando os botões à esquerda.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="866"/>
        <source>Interactive View Link Parameters</source>
        <translation>Parâmetros de Ligação de Visualização Interativa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="888"/>
        <source>Select Interactive View Link to manage.</source>
        <translation>Selecione a Ligação de Visualização Interativa para gerenciar.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="895"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="898"/>
        <source>Hide background from linked view.</source>
        <translation>Ocultar fundo da visualização ligada.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="905"/>
        <source>Opacity:</source>
        <translation>Opacidade:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="912"/>
        <source>Define the view link opacity.</source>
        <translation>Definir a opacidade da ligação de visualização.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="97"/>
        <source>Add Current Viewpoint</source>
        <translation>Adicionar Ponto de Vista Atual</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="673"/>
        <source>Current Viewpoint %1</source>
        <translation>Ponto de Vista Atual %1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="823"/>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="850"/>
        <source>All Files</source>
        <translation>Todos os Arquivos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="825"/>
        <source>Save Camera Configuration</source>
        <translation>Salvar Configuração da Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="852"/>
        <source>Load Camera Configuration</source>
        <translation>Carregar Configuração da Câmera</translation>
    </message>
</context>
<context>
    <name>pqChangeInputDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqChangeInputDialog.ui" line="16"/>
        <source>Change Input Dialog</source>
        <translation>Diálogo para Alterar Entrada</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqChangeInputDialog.ui" line="31"/>
        <source>Available Input Ports</source>
        <translation>Portas de Entrada Disponíveis</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqChangeInputDialog.ui" line="48"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Select &lt;span style=&quot; font-weight:600;&quot;&gt;INPUT0&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Select &lt;span style=&quot; font-weight:600;&quot;&gt;INPUT0&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqChangeInputDialog.cxx" line="172"/>
        <source>Select &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Selecionar &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>pqCollaborationPanel</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="79"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="84"/>
        <source>Participant</source>
        <translation>Participante</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="112"/>
        <source>I&apos;m alone</source>
        <translation>Estou sozinho</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="131"/>
        <source>Share mouse pointer</source>
        <translation>Compartilhar ponteiro do mouse</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="161"/>
        <source>Disable further connections</source>
        <translation>Desabilitar novas conexões</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="173"/>
        <source>Server Connect ID </source>
        <translation>ID de Conexão do Servidor </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="204"/>
        <source>Chat room</source>
        <translation>sala de bate-papo</translation>
    </message>
</context>
<context>
    <name>pqColorChooserButtonWithPalettes</name>
    <message>
        <location filename="../Qt/Components/pqColorChooserButtonWithPalettes.cxx" line="106"/>
        <source>Black</source>
        <translation>Preto</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqColorChooserButtonWithPalettes.cxx" line="108"/>
        <source>White</source>
        <translation>Branco</translation>
    </message>
</context>
<context>
    <name>pqComparativeCueWidget</name>
    <message>
        <location filename="../Qt/Components/pqComparativeCueWidget.cxx" line="170"/>
        <source>Parameter Changed</source>
        <translation>Parâmetro Alterado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeCueWidget.cxx" line="256"/>
        <source>Update Parameter Values</source>
        <translation>Atualizar Valores dos Parâmetros</translation>
    </message>
</context>
<context>
    <name>pqComparativeParameterRangeDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="16"/>
        <source>Enter Parameter Range</source>
        <translation>Inserir Intervalo de Parâmetros</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="22"/>
        <source>Use comma-separated values to enter multiple values.
However number of values in both entires must match.</source>
        <translation>Use valores separados por vírgula para inserir múltiplos valores.
No entanto, o número de valores em ambas as entradas deve corresponder.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="35"/>
        <source>  to  </source>
        <translation>  a  </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="60"/>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="63"/>
        <source>Controls the direction in which the parameter is varied.</source>
        <translation>Controla a direção na qual o parâmetro é variado.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="67"/>
        <source>Vary horizontally first</source>
        <translation>Variar horizontalmente primeiro</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="72"/>
        <source>Vary vertically first</source>
        <translation>Variar verticalmente primeiro</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="77"/>
        <source>Only vary horizontally</source>
        <translation>Apenas variar horizontalmente</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="82"/>
        <source>Only vary vertically</source>
        <translation>Apenas variar verticalmente</translation>
    </message>
</context>
<context>
    <name>pqComparativeVisPanel</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="22"/>
        <source>Layout:</source>
        <translation>Layout:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="36"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="66"/>
        <source>Automatic Parameter Labels</source>
        <translation>Rótulos de Parâmetro Automáticos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="122"/>
        <source>Parameter</source>
        <translation>Parâmetro</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="162"/>
        <source>[Select Parameter]</source>
        <translation>[Selecionar Parâmetro]</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="187"/>
        <source>Comma-separated values accepted.</source>
        <translation>Valores separados por vírgula aceitos.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="197"/>
        <source>Overlay all comparisons</source>
        <translation>Sobrepor todas as comparações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="336"/>
        <source>Add parameter %1 : %2</source>
        <translation>Adicionar parâmetro %1: %2</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="342"/>
        <source>Add parameter Time</source>
        <translation>Adicionar parâmetro Tempo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="411"/>
        <source>Remove Parameter</source>
        <translation>Remover Parâmetro</translation>
    </message>
</context>
<context>
    <name>pqComparativeVisPanelNS</name>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="80"/>
        <source>unrecognized-proxy</source>
        <translation>Proxy não reconhecido</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="89"/>
        <source>unrecognized-property</source>
        <translation>Propriedade não reconhecida</translation>
    </message>
</context>
<context>
    <name>pqConnectIdDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqConnectIdDialog.ui" line="22"/>
        <source>Connect ID</source>
        <translation>ID da Conexão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqConnectIdDialog.ui" line="34"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The server requires a connection ID and the provided one does not match.&lt;br/&gt;If you don&apos;t know what is the connection ID for this server, ask its administrator.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;O servidor requer um ID de conexão e o fornecido não corresponde.&lt;br/&gt;Se você não sabe qual é o ID de conexão para este servidor, pergunte ao administrador.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqConnectIdDialog.ui" line="53"/>
        <source>Connect ID :</source>
        <translation>ID de Conexão:</translation>
    </message>
</context>
<context>
    <name>pqContourControls</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="40"/>
        <source>Generate Triangles</source>
        <translation>Gerar Triângulos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="47"/>
        <source>Compute Scalars</source>
        <translation>Calcular Escalares</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="54"/>
        <source>Compute Gradients</source>
        <translation>Calcular Gradientes</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="61"/>
        <source>Compute Normals</source>
        <translation>Calcular Normais</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="71"/>
        <source>Contour By</source>
        <translation>Contornar Por</translation>
    </message>
</context>
<context>
    <name>pqCustomFilterDefinitionWizard</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="16"/>
        <source>Create Custom Filter</source>
        <translation>Criar Filtro Personalizado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="95"/>
        <source>&lt;b&gt;Choose a Name&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Enter a name for the custom filter.</source>
        <translation>&lt;b&gt;Escolha um Nome&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Digite um nome para o filtro personalizado.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="112"/>
        <source>&lt;b&gt;Define the Inputs&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Select and name the input ports.</source>
        <translation>&lt;b&gt;Definir as Entradas&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Selecione e nomeie as portas de entrada.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="129"/>
        <source>&lt;b&gt;Define the Outputs&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Select and name the output ports.</source>
        <translation>&lt;b&gt;Define as Saídas&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Selecione e nomeie as portas de saída.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="146"/>
        <source>&lt;b&gt;Define the Properties&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Select and name the exposed properties.</source>
        <translation>&lt;b&gt;Definir as Propriedades&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Selecione e nomeie as propriedades expostas.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="211"/>
        <source>The name will be used to identify the custom filter. It should be unique. The name should also indicate what the custom filter will be used for.</source>
        <translation>O nome será usado para identificar o filtro personalizado. Deve ser único. O nome também deve indicar para que o filtro personalizado será usado.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="221"/>
        <source>Custom Filter Name</source>
        <translation>Nome do Filtro Personalizado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="228"/>
        <source>Enter the custom filter name here.</source>
        <translation>Digite o nome do filtro personalizado aqui.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="258"/>
        <source>Select an object from the pipeline layout on the left. Then, select the input property from that object to expose. Give the input port a name and add it to the list.</source>
        <translation>Selecione um objeto no layout do pipeline à esquerda. Em seguida, selecione a propriedade de entrada desse objeto para expor. Dê um nome à porta de entrada e adicione-a à lista.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="302"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="543"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="716"/>
        <source>Object</source>
        <translation>Objeto</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="307"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="609"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="721"/>
        <source>Property</source>
        <translation>Propriedade</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="312"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="548"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="726"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="323"/>
        <source>Input Property</source>
        <translation>Propriedade de Entrada</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="330"/>
        <source>Input Name</source>
        <translation>Nome da Entrada</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="368"/>
        <source>Add Input</source>
        <translation>Adicionar Entrada</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="381"/>
        <source>Remove Input</source>
        <translation>Remover Entrada</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="394"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="490"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="677"/>
        <source>Move Up</source>
        <translation>Mover para cima</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="407"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="503"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="690"/>
        <source>Move Down</source>
        <translation>Mover para Baixo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="464"/>
        <source>Add Output</source>
        <translation>Adicionar Saída</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="477"/>
        <source>Remove Output</source>
        <translation>Remover Saída</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="519"/>
        <source>Output Port</source>
        <translation>Porta de Saída</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="526"/>
        <source>Output Name</source>
        <translation>Nome de Saída</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="580"/>
        <source>Select an object from the pipeline layout on the left. Give the output port a name and add it to the list.</source>
        <translation>Selecione um objeto no layout do pipeline à esquerda. Dê um nome à porta de saída e adicione-a à lista.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="616"/>
        <source>Property Name</source>
        <translation>Nome da Propriedade</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="651"/>
        <source>Add Property</source>
        <translation>Adicionar Propriedade</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="664"/>
        <source>Remove Property</source>
        <translation>Remover Propriedade</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="758"/>
        <source>Select an object from the pipeline layout on the left. Then, select the property from that object to expose. Give the property a name and add it to the list.</source>
        <translation>Selecione um objeto no layout do pipeline à esquerda. Depois, selecione a propriedade desse objeto para expor. Dê um nome à propriedade e adicione-a à lista.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="835"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Voltar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="845"/>
        <source>&amp;Next &gt;</source>
        <translation>&amp;Próximo &gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="874"/>
        <source>&amp;Finish</source>
        <translation>&amp;Concluir</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="884"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>pqCustomFilterManager</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="19"/>
        <source>Custom Filter Manager</source>
        <translation>Gerenciador de Filtro Personalizado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="31"/>
        <source>Displays the list of registered custom filter definitions.</source>
        <translation>Exibe a lista de definições de filtros personalizados registrados.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="60"/>
        <source>&amp;Close</source>
        <translation>&amp;Fechar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="70"/>
        <source>Removes the selected custom filter definitions.</source>
        <translation>Remove as definições de filtros personalizados selecionados.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="73"/>
        <source>&amp;Remove</source>
        <translation>&amp;Remover</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="80"/>
        <source>Exports the selected custom filter definitions to a file.</source>
        <translation>Exporta as definições de filtros personalizados selecionados para um arquivo.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="83"/>
        <source>&amp;Export</source>
        <translation>&amp;Exportar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="90"/>
        <source>Imports custom filter definitions from a file.</source>
        <translation>Importa definições de filtros personalizados de um arquivo.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="93"/>
        <source>&amp;Import</source>
        <translation>&amp;Importar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="205"/>
        <source>Open Custom Filter File</source>
        <translation>Abrir Arquivo de Filtro Personalizado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="206"/>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="225"/>
        <source>Custom Filter Files</source>
        <translation>Arquivos de Filtro Personalizado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="206"/>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="225"/>
        <source>All Files</source>
        <translation>Todos os Arquivos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="224"/>
        <source>Save Custom Filter File</source>
        <translation>Salvar Arquivo de Filtro Personalizado</translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointButtonDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="22"/>
        <source>Configure Custom Viewpoints</source>
        <translation>Configurar Pontos de Vista Personalizados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="52"/>
        <source>Viewpoint Name</source>
        <translation>Nome do Ponto de Vista</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="69"/>
        <source>Assign</source>
        <translation>Atribuir</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="86"/>
        <source>Button</source>
        <translation>Botão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="130"/>
        <source>Add new custom viewpoint</source>
        <translation>Adicionar novo ponto de vista personalizado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="144"/>
        <source>Clear All</source>
        <translation>Limpar Tudo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="151"/>
        <source>Import...</source>
        <translation>Importar...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="158"/>
        <source>Export...</source>
        <translation>Exportar...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="322"/>
        <source>All Files</source>
        <translation>Todos os Arquivos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="325"/>
        <source>Load Custom Viewpoints Configuration</source>
        <translation>Carregar Configuração de Pontos de Vista Personalizados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="441"/>
        <source>Save Custom Viewpoints Configuration</source>
        <translation>Salvar Configuração de Pontos de Vista Personalizados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="511"/>
        <source>Current Viewpoint %1</source>
        <translation>Ponto de Vista Atual %1</translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointButtonDialogUI</name>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="85"/>
        <source>This text will be set to the buttons tool tip.</source>
        <translation>Este texto será definido para a dica de ferramenta do botão.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="90"/>
        <source>Current Viewpoint</source>
        <translation>Ponto de Vista Atual</translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointButtonFileInfo</name>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="205"/>
        <source>Unnamed Viewpoint</source>
        <translation>Ponto de Vista Sem Nome</translation>
    </message>
</context>
<context>
    <name>pqDataInformationWidget</name>
    <message>
        <location filename="../Qt/Components/pqDataInformationWidget.cxx" line="152"/>
        <source>Column Titles</source>
        <translation>Títulos das Colunas</translation>
    </message>
</context>
<context>
    <name>pqDisplayColorWidget</name>
    <message>
        <location filename="../Qt/Components/pqDisplayColorWidget.cxx" line="519"/>
        <source>Solid Color</source>
        <translation>Cor Sólida</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqDisplayColorWidget.cxx" line="610"/>
        <source>Change Color Component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change color component</source>
        <translation type="vanished">Alterar componente de cor</translation>
    </message>
</context>
<context>
    <name>pqDisplayRepresentationWidget</name>
    <message>
        <location filename="../Qt/Components/pqDisplayRepresentationWidget.cxx" line="161"/>
        <source>Representation</source>
        <translation>Representação</translation>
    </message>
</context>
<context>
    <name>pqDoubleVectorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqDoubleVectorPropertyWidget.cxx" line="315"/>
        <source>Reset using current data values</source>
        <translation>Redefinir usando valores de dados atuais</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqDoubleVectorPropertyWidget.cxx" line="347"/>
        <source>Reset to active data bounds</source>
        <translation>Redefinir para limites de dados ativos</translation>
    </message>
</context>
<context>
    <name>pqExpressionChooserButton</name>
    <message>
        <location filename="../Qt/Components/pqExpressionChooserButton.cxx" line="18"/>
        <source>Choose Expression</source>
        <translation>Escolher Expressão</translation>
    </message>
</context>
<context>
    <name>pqExpressionsManagerDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="16"/>
        <source>Choose Expression</source>
        <translation>Escolher Expressão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="83"/>
        <source>Use current</source>
        <translation>Usar atual</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="86"/>
        <source>Use expression in current filter</source>
        <translation>Usar expressão no filtro atual</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="93"/>
        <source>Add</source>
        <translation>Adicionar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="96"/>
        <source>Add a new expression</source>
        <translation>Adicionar uma nova expressão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="106"/>
        <source>Remove selected expressions</source>
        <translation>Remover expressões selecionadas</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="109"/>
        <source>Remove</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="119"/>
        <source>Remove all expressions</source>
        <translation>Remover todas as expressões</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="122"/>
        <source>Remove All</source>
        <translation>Remover Tudo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="136"/>
        <source>Import expressions from a json file</source>
        <translation>Importar expressões de um arquivo json</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="139"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="146"/>
        <source>Export expressions to a json file</source>
        <translation>Exportar expressões para um arquivo json</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="149"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="169"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="172"/>
        <source>Close without saving</source>
        <translation>Fechar sem salvar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="179"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="182"/>
        <source>Save to settings</source>
        <translation>Salvar configurações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="189"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="192"/>
        <source>Save and close</source>
        <translation>Salvar e fechar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="380"/>
        <source>Remove all expressions ?</source>
        <translation>Remover todas as expressões?</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="460"/>
        <source>Export Expressions(s)</source>
        <translation>Exportar Expressão(ões)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="461"/>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="501"/>
        <source>ParaView Expressions</source>
        <translation>Expressões ParaView</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="461"/>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="501"/>
        <source>All Files</source>
        <translation>Todos os Arquivos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="492"/>
        <source>Failed to save expressions to </source>
        <translation>Falha ao salvar expressões em </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="500"/>
        <source>Import Expressions(s)</source>
        <translation>Importar Expressão(ões)</translation>
    </message>
</context>
<context>
    <name>pqExpressionsWidget</name>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="62"/>
        <source>Save expression</source>
        <translation>Salvar expressão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="67"/>
        <source>Open Expressions Manager</source>
        <translation>Abrir Gerenciador de Expressões</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="72"/>
        <source>Clear expression</source>
        <translation>Limpar expressão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="81"/>
        <source>Saved! </source>
        <translation>Salvo! </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="81"/>
        <source>Already Exists</source>
        <translation>Já Existe</translation>
    </message>
</context>
<context>
    <name>pqFavoritesDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="17"/>
        <source>Favorites Manager</source>
        <translation>Gerenciador de Favoritos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="26"/>
        <source>&lt;&lt;&lt; Remove</source>
        <translation>&lt;&lt;&lt; Remover</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="39"/>
        <source>Available Filters</source>
        <translation>Filtros Disponíveis</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="82"/>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="156"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="116"/>
        <source>Favorites</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="164"/>
        <source>Add Category ...</source>
        <translation>Adicionar Categoria...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="200"/>
        <source>Add &gt;&gt;&gt;</source>
        <translation>Adicionar &gt;&gt;&gt;</translation>
    </message>
</context>
<context>
    <name>pqFileChooserWidget</name>
    <message>
        <location filename="../Qt/Components/pqFileChooserWidget.cxx" line="146"/>
        <source>Open Directory:</source>
        <translation>Abrir Diretório:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFileChooserWidget.cxx" line="150"/>
        <source>Save File:</source>
        <translation>Salvar Arquivo:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFileChooserWidget.cxx" line="154"/>
        <source>Open File:</source>
        <translation>Abrir Arquivo:</translation>
    </message>
</context>
<context>
    <name>pqFindDataSelectionDisplayFrame</name>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="142"/>
        <source>not available</source>
        <translation>não disponível</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="160"/>
        <source>%1 (partial)</source>
        <translation>%1 (parcial)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="223"/>
        <source>Change labels</source>
        <translation>Mudar rótulos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="398"/>
        <source>Interactive selection label properties</source>
        <translation>Propriedades de rótulo de seleção interativa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="400"/>
        <source>Interactive Selection Label Properties</source>
        <translation>Propriedades de Rótulo de Seleção Interativa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="436"/>
        <source>Change selection display properties</source>
        <translation>Mudar propriedades de exibição de seleção</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="438"/>
        <source>Selection Label Properties</source>
        <translation>Propriedades de Rótulo de Seleção</translation>
    </message>
</context>
<context>
    <name>pqHelpWindow</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="16"/>
        <source>Online Documentation</source>
        <translation>Documentação Online</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="47"/>
        <source>Contents</source>
        <translation>Conteúdo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="56"/>
        <source>Search</source>
        <translation>Pesquisar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="65"/>
        <source>toolBar</source>
        <translation>barra de ferramentas</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="84"/>
        <source>Home</source>
        <translation>Início</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="93"/>
        <source>Go backward</source>
        <translation>Voltar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="102"/>
        <source>Go forward</source>
        <translation>Avançar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="111"/>
        <source>Save as homepage</source>
        <translation>Salvar como página inicial</translation>
    </message>
</context>
<context>
    <name>pqIconBrowser</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="16"/>
        <source>Browse icons</source>
        <translation>Navegar por ícones</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="88"/>
        <source>Import new icon</source>
        <translation>Importar novo ícone</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="91"/>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="105"/>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="132"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="102"/>
        <source>Remove custom icon</source>
        <translation>Remover ícone personalizado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="129"/>
        <source>Remove all custom icons</source>
        <translation>Remover todos os ícones personalizados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="40"/>
        <source>All</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="101"/>
        <source>Delete All</source>
        <translation>Excluir Todos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="102"/>
        <source>All custom icons will be deleted. Are you sure?</source>
        <translation>Todos os ícones personalizados serão excluídos. Tem certeza?</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="147"/>
        <source>Images</source>
        <translation>Imagens</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="150"/>
        <source>Load Icon</source>
        <translation>Carregar Ícone</translation>
    </message>
</context>
<context>
    <name>pqInputSelectorWidget</name>
    <message>
        <location filename="../Qt/Components/pqInputSelectorWidget.cxx" line="129"/>
        <source>none</source>
        <translation>nenhum</translation>
    </message>
</context>
<context>
    <name>pqInternals</name>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="96"/>
        <source>Resize Frame</source>
        <translation>Redimensionar Quadro</translation>
    </message>
</context>
<context>
    <name>pqItemViewSearchWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="86"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="138"/>
        <source>Find</source>
        <translation>Localizar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="160"/>
        <source>Next (ALT+N)</source>
        <translation>Próximo (ALT+N)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="189"/>
        <source>Match Case</source>
        <translation>Diferenciar Maiúsculas/Minúsculas</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="208"/>
        <source>Close (Esc)</source>
        <translation>Fechar (Esc)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="237"/>
        <source>Previous (ALT+P)</source>
        <translation>Anterior (ALT+P)</translation>
    </message>
</context>
<context>
    <name>pqKeyFrameEditor</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="16"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="50"/>
        <source>Label</source>
        <translation>Rótulo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="60"/>
        <source>Add a new keyframe before selected</source>
        <translation>Adicionar um novo quadro-chave antes do selecionado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="63"/>
        <source>New</source>
        <translation>Novo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="70"/>
        <source>Delete selected keyframe</source>
        <translation>Excluir quadro-chave selecionado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="73"/>
        <source>Delete</source>
        <translation>Excluir</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="80"/>
        <source>Delete all keyframe</source>
        <translation>Excluir todos os quadros-chave</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="83"/>
        <source>Delete All</source>
        <translation>Excluir Todos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="90"/>
        <source>Import key frames from a .pvkfc file.</source>
        <translation>Importar quadros-chave de um arquivo .pvkfc.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="93"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="100"/>
        <source>Export key frames to a .pvkfc file.</source>
        <translation>Exportar quadros-chave para um arquivo .pvkfc.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="103"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="120"/>
        <source>Apply selected keyframe configuration to the current view camera</source>
        <translation>Aplicar a configuração do quadro-chave selecionado à câmera da visualização atual</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="123"/>
        <source>Apply to Camera</source>
        <translation>Aplicar à Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="130"/>
        <source>Use current camera for selected keyframe</source>
        <translation>Usar a câmera atual para o quadro-chave selecionado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="133"/>
        <source>Use current Camera</source>
        <translation>Usar a Câmera Atual</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="140"/>
        <source>Use spline interpolation for this cue</source>
        <translation>Usar interpolação spline para este evento</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="143"/>
        <source>Spline Interpolation</source>
        <translation>Interpolação Spline</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="154"/>
        <source>Create an orbit path for selected keyframe, starting with current camera.</source>
        <translation>Criar um caminho de órbita para o quadro-chave selecionado, começando com a câmera atual.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="157"/>
        <source>Create Orbit</source>
        <translation>Criar Órbita</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="464"/>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="470"/>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="465"/>
        <source>Camera Values</source>
        <translation>Valores da Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="471"/>
        <source>Interpolation</source>
        <translation>Interpolação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="471"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="541"/>
        <source>Edit Keyframes</source>
        <translation>Editar Quadros-Chave</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="837"/>
        <source>Load Custom KeyFrames Configuration</source>
        <translation>Carregar Configuração Personalizada de Quadros-Chave</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="943"/>
        <source>Save Current KeyFrames Configuration</source>
        <translation>Salvar Configuração Atual de Quadros-Chave</translation>
    </message>
</context>
<context>
    <name>pqKeyFrameEditorDialog</name>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="53"/>
        <source>Key Frame Interpolation</source>
        <translation>Interpolação de Quadros-Chave</translation>
    </message>
</context>
<context>
    <name>pqKeyFrameTypeWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="24"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="39"/>
        <source>Interpolation:</source>
        <translation>Interpolação:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="46"/>
        <location filename="../Qt/Components/pqKeyFrameTypeWidget.cxx" line="32"/>
        <source>Exponential</source>
        <translation>Exponencial</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="58"/>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="162"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="65"/>
        <source>Base</source>
        <translation>Base</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="72"/>
        <source>Start Power</source>
        <translation>Potência Inicial</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="79"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="86"/>
        <source>End Power</source>
        <translation>Potência Final</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="93"/>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="169"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="119"/>
        <location filename="../Qt/Components/pqKeyFrameTypeWidget.cxx" line="34"/>
        <source>Sinusoid</source>
        <translation>Sinusoidal</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="131"/>
        <source>Offset</source>
        <translation>Deslocamento</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="138"/>
        <source>Frequency</source>
        <translation>Frequência</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="145"/>
        <source>Phase</source>
        <translation>Fase</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameTypeWidget.cxx" line="30"/>
        <source>Ramp</source>
        <translation>Rampa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameTypeWidget.cxx" line="35"/>
        <source>Boolean</source>
        <translation>Booleano</translation>
    </message>
</context>
<context>
    <name>pqLightsEditor</name>
    <message>
        <location filename="../Qt/Components/pqLightsEditor.cxx" line="94"/>
        <source>Restore Default Lights</source>
        <translation>Restaurar Luzes Padrão</translation>
    </message>
</context>
<context>
    <name>pqLightsInspector</name>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="153"/>
        <source>Light %1</source>
        <translation>Luz %1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="164"/>
        <source>Move to Camera</source>
        <translation>Mover para a Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="168"/>
        <source>Match this light&apos;s position and focal point to the camera.</source>
        <translation>Corresponder a posição e o ponto focal desta luz à câmera.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="174"/>
        <source>Reset Light</source>
        <translation>Redefinir Luz</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="178"/>
        <source>Reset this light parameters to default</source>
        <translation>Redefinir os parâmetros desta luz para o padrão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="183"/>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="338"/>
        <source>Remove Light</source>
        <translation>Remover Luz</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="187"/>
        <source>Remove this light.</source>
        <translation>Remover esta luz.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="255"/>
        <source>Add Light</source>
        <translation>Adicionar Luz</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="335"/>
        <source>remove light added to the view</source>
        <translation>remover luz adicionada à visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="374"/>
        <source>reset a light</source>
        <translation>redefinir uma luz</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="398"/>
        <source>update a light</source>
        <translation>atualizar uma luz</translation>
    </message>
</context>
<context>
    <name>pqLinksEditor</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="16"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="106"/>
        <source>Name:</source>
        <translation>Nome:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="116"/>
        <source>Mode:</source>
        <translation>Modo:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="124"/>
        <source>Object Link</source>
        <translation>Link de Objeto</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="129"/>
        <source>Property Link</source>
        <translation>Link de Propriedade</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="134"/>
        <source>Selection Link</source>
        <translation>Link de Seleção</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="157"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="164"/>
        <source>Interactive View Link</source>
        <translation>Link de Visualização Interativa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="174"/>
        <source>Camera Widget View Link</source>
        <translation>Link de Visualização do Widget de Câmera</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="184"/>
        <source>&lt;span&gt;When enabled, selection is linked by evaluating the selection on the data source and select corresponding elements based on their indices on other linked data sources, instead of sharing the actual selection between the data sources.&lt;/span&gt;</source>
        <translation>&lt;span&gt;Quando ativado, a seleção é vinculada avaliando a seleção na fonte de dados e selecionando elementos correspondentes com base em seus índices em outras fontes de dados vinculadas, em vez de compartilhar a seleção real entre as fontes de dados.&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="187"/>
        <source>Link Selected Elements</source>
        <translation>Vincular Elementos Selecionados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="103"/>
        <source>Source %1 in View %2</source>
        <translation>Fonte %1 na Visualização %2</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="256"/>
        <source>Views</source>
        <translation>Visualizações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="258"/>
        <source>Representations</source>
        <translation>Representações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="260"/>
        <source>Sources</source>
        <translation>Fontes</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="262"/>
        <source>Unknown</source>
        <translation>Desconhecido</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="793"/>
        <source>Linking views will link only cameras.</source>
        <translation>Vincular visualizações vinculará apenas as câmeras.</translation>
    </message>
</context>
<context>
    <name>pqLinksManager</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksManager.ui" line="16"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksManager.ui" line="36"/>
        <source>Add...</source>
        <translation>Adicionar...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksManager.ui" line="43"/>
        <source>Edit...</source>
        <translation>Editar...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksManager.ui" line="50"/>
        <source>Remove</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksManager.cxx" line="56"/>
        <source>Add Link</source>
        <translation>Adicionar Link</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksManager.cxx" line="100"/>
        <source>Edit Link</source>
        <translation>Editar Link</translation>
    </message>
</context>
<context>
    <name>pqLiveInsituManager</name>
    <message>
        <location filename="../Qt/Components/pqLiveInsituManager.cxx" line="205"/>
        <source>Catalyst Server Port</source>
        <translation>Porta do Servidor Catalyst</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLiveInsituManager.cxx" line="206"/>
        <source>Enter the port number to accept connections
from Catalyst on:</source>
        <translation>Digite o número da porta para aceitar conexões
do Catalyst:</translation>
    </message>
    <message>
        <source>Ready for Catalyst connections</source>
        <translation type="vanished">Pronto para conexões do Catalyst</translation>
    </message>
    <message>
        <source>Accepting connections from Catalyst Co-Processor
for live-coprocessing on port %1</source>
        <translation type="vanished">Aceitando conexões do Coprocessador Catalyst
para coprocessamento ao vivo na porta %1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLiveInsituManager.cxx" line="253"/>
        <source>Catalyst Disconnected</source>
        <translation>Catalyst Desconectado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLiveInsituManager.cxx" line="254"/>
        <source>Connection to Catalyst Co-Processor has been terminated involuntarily. This implies either a communication error, or that the Catalyst co-processor has terminated. The Catalyst session will now be cleaned up. You can start a new one if you want to monitor for additional Catalyst connection requests.</source>
        <translation>A conexão com o Coprocessador Catalyst foi encerrada involuntariamente. Isso implica um erro de comunicação ou que o coprocessador Catalyst foi encerrado. A sessão do Catalyst agora será limpa. Você pode iniciar uma nova se quiser monitorar solicitações de conexão adicionais do Catalyst.</translation>
    </message>
</context>
<context>
    <name>pqLockViewSizeCustomDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLockViewSizeCustomDialog.ui" line="16"/>
        <source>Lock View to Custom Size</source>
        <translation>Bloquear Visualização para Tamanho Personalizado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLockViewSizeCustomDialog.ui" line="22"/>
        <source>Select Maximum Resolution for Each View</source>
        <translation>Selecionar Resolução Máxima para Cada Visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLockViewSizeCustomDialog.ui" line="41"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLockViewSizeCustomDialog.cxx" line="25"/>
        <source>Unlock</source>
        <translation>Desbloquear</translation>
    </message>
</context>
<context>
    <name>pqLogViewerDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="16"/>
        <source>Log Viewer</source>
        <translation>Visualizador de Log</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="53"/>
        <source>New Log</source>
        <translation>Novo Log</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="81"/>
        <source>Application process to log</source>
        <translation>Processo do aplicativo a ser registrado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="94"/>
        <source>Process</source>
        <translation>Processo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="101"/>
        <source>Rank</source>
        <translation>Classificação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="114"/>
        <source>Rank of process to log</source>
        <translation>Classificação do processo a ser registrado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="129"/>
        <source>Add Log</source>
        <translation>Adicionar Log</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="151"/>
        <source>Global Filter</source>
        <translation>Filtro Global</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="177"/>
        <source>Global filter that applies to all logs</source>
        <translation>Filtro global que se aplica a todos os logs</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="183"/>
        <source>Set filter on all logs</source>
        <translation>Definir filtro em todos os logs</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="205"/>
        <source>Verbosity Settings</source>
        <translation>Configurações de Verbosidade</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="225"/>
        <source>Client Verbosity Level</source>
        <translation>Nível de Verbosidade do Cliente</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="235"/>
        <source>Server Verbosity Level</source>
        <translation>Nível de Verbosidade do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="245"/>
        <source>Data Server Verbosity Level</source>
        <translation>Nível de Verbosidade do Servidor de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="255"/>
        <source>Render Server Verbosity Level</source>
        <translation>Nível de Verbosidade do Servidor de Renderização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="286"/>
        <source>Categories</source>
        <translation>Categorias</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="306"/>
        <source>Always Log Messages About...</source>
        <translation>Sempre Registrar Mensagens Sobre...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="328"/>
        <source>Data Movement</source>
        <translation>Movimentação de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="335"/>
        <source>Rendering</source>
        <translation>Renderização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="342"/>
        <source>Application</source>
        <translation>Aplicação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="349"/>
        <source>Pipeline</source>
        <translation>Pipeline</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="356"/>
        <source>Plugins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="363"/>
        <source>Filter Execution</source>
        <translation>Execução do Filtro</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="395"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="402"/>
        <source>Clear Logs</source>
        <translation>Limpar Logs</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="118"/>
        <source>Data Server</source>
        <translation>Servidor de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="119"/>
        <source>Render Server</source>
        <translation>Servidor de Renderização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="132"/>
        <source>Server</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="366"/>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="367"/>
        <source>Close log</source>
        <translation>Fechar log</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="413"/>
        <source>OFF</source>
        <translation>DESLIGADO</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="414"/>
        <source>ERROR</source>
        <translation>ERRO</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="415"/>
        <source>WARNING</source>
        <translation>AVISO</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="416"/>
        <source>INFO</source>
        <translation>INFO</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="417"/>
        <source>TRACE</source>
        <translation>RASTREAMENTO</translation>
    </message>
</context>
<context>
    <name>pqMemoryInspectorPanel</name>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="524"/>
        <source>System Total</source>
        <translation>Total do Sistema</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1471"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1480"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1472"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1481"/>
        <source>No process selected. Select a specific process first by clicking on its entry in the tree view widget.</source>
        <translation>Nenhum processo selecionado. Selecione primeiro um processo específico clicando em sua entrada no widget de visualização em árvore.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1585"/>
        <source>Client System Properties</source>
        <translation>Propriedades do Sistema Cliente</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1586"/>
        <source>Server System Properties</source>
        <translation>Propriedades do Sistema Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1587"/>
        <source>Host:</source>
        <translation>Host:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1588"/>
        <source>OS:</source>
        <translation>SO:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1589"/>
        <source>CPU:</source>
        <translation>CPU:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1590"/>
        <source>Memory:</source>
        <translation>Memória:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1623"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1663"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1676"/>
        <source>properties...</source>
        <translation>propriedades...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1634"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1669"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1690"/>
        <source>remote command...</source>
        <translation>comando remoto...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1641"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1682"/>
        <source>show only nodes</source>
        <translation>mostrar apenas nós</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1642"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1683"/>
        <source>show all ranks</source>
        <translation>mostrar todas as classificações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1704"/>
        <source>stack trace signal handler</source>
        <translation>manipulador de sinal de rastreamento de pilha</translation>
    </message>
</context>
<context>
    <name>pqMemoryInspectorPanelForm</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMemoryInspectorPanelForm.ui" line="22"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMemoryInspectorPanelForm.ui" line="78"/>
        <source>Auto-update</source>
        <translation>Atualização Automática</translation>
    </message>
</context>
<context>
    <name>pqMultiBlockPropertiesEditorWidget</name>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesEditorWidget.cxx" line="106"/>
        <location filename="../Qt/Components/pqMultiBlockPropertiesEditorWidget.cxx" line="138"/>
        <location filename="../Qt/Components/pqMultiBlockPropertiesEditorWidget.cxx" line="171"/>
        <source>Change </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMultiBlockPropertiesStateWidget</name>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="187"/>
        <source>Properties are </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="187"/>
        <source>Property is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="189"/>
        <source>disabled because no blocks are selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="191"/>
        <source>inherited from the representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="193"/>
        <source>inherited from block(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="195"/>
        <source>inherited from block(s) and the representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="196"/>
        <source>set in block(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="198"/>
        <source>set in block(s) and inherited from the representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="200"/>
        <source>set in block(s) and inherited from block(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="202"/>
        <source>set in block(s) and inherited from block(s) and the representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="218"/>
        <source>Reset to value(s) inherited from block/representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="356"/>
        <source>Reset </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMultiViewWidget</name>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="731"/>
        <source>Split View</source>
        <translation>Dividir Visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="755"/>
        <source>Close View</source>
        <translation>Fechar Visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="785"/>
        <source>Destroy all views</source>
        <translation>Destruir todas as visualizações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="850"/>
        <source>Swap Views</source>
        <translation>Trocar Visualizações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="931"/>
        <source>Exit preview mode</source>
        <translation>Sair do modo de pré-visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="932"/>
        <source>Enter preview mode</source>
        <translation>Entrar no modo de pré-visualização</translation>
    </message>
</context>
<context>
    <name>pqPipelineBrowserWidget</name>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="255"/>
        <source>Show %1</source>
        <translation>Mostrar %1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="256"/>
        <source>Hide %1</source>
        <translation>Ocultar %1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="260"/>
        <source>Show Selected</source>
        <translation>Mostrar Selecionados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="260"/>
        <source>Hide Selected</source>
        <translation>Ocultar Selecionados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="343"/>
        <source>You might have added a new scalar bar mode, you need to do something here, skipping.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPipelineModel</name>
    <message>
        <location filename="../Qt/Components/pqPipelineModel.cxx" line="891"/>
        <source>Rename %1 to %2</source>
        <translation>Renomear %1 para %2</translation>
    </message>
</context>
<context>
    <name>pqPipelineTimeKeyFrameEditor</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="16"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="44"/>
        <source>Variable Time:</source>
        <translation>Tempo Variável:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="74"/>
        <source>Constant Time:</source>
        <translation>Tempo Constante:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="93"/>
        <source>Animation Time</source>
        <translation>Tempo de Animação</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineTimeKeyFrameEditor.cxx" line="104"/>
        <source>Edit Keyframes</source>
        <translation>Editar Quadros-Chave</translation>
    </message>
</context>
<context>
    <name>pqPluginDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="16"/>
        <source>Plugin Manager</source>
        <translation>Gerenciador de Plugins</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="22"/>
        <source>TextLabel</source>
        <translation>Etiqueta de Texto</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="41"/>
        <source>Remote Plugins</source>
        <translation>Plugins Remotos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="73"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="157"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="81"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="165"/>
        <source>Load New ...</source>
        <translation>Carregar Novo ...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="101"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="185"/>
        <source>Remove</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="91"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="175"/>
        <source>Load Selected</source>
        <translation>Carregar Selecionados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="115"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="199"/>
        <source>Add plugin configuration file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="125"/>
        <source>Local Plugins</source>
        <translation>Plugins Locais</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="64"/>
        <source>Local plugins are automatically searched for in %1.</source>
        <translation>Os plugins locais são procurados automaticamente em %1.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="70"/>
        <source>Remote plugins are automatically searched for in %1.
Local plugins are automatically searched for in %2.</source>
        <translation>Os plugins remotos são procurados automaticamente em %1.
Os plugins locais são procurados automaticamente em %2.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="120"/>
        <source>Qt binary resource files</source>
        <translation>Arquivos de recursos binários do Qt</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="121"/>
        <source>XML plugins</source>
        <translation>Plugins XML</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="124"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="129"/>
        <source>Binary plugins</source>
        <translation>Plugins Binários</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="157"/>
        <source>Supported plugins</source>
        <translation>Plugins Suportados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="205"/>
        <source>Plugin config file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="267"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="267"/>
        <source>Property</source>
        <translation>Propriedade</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="334"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="554"/>
        <source>Loaded</source>
        <translation>Carregado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="342"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="554"/>
        <source>Not Loaded</source>
        <translation>Não Carregado</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="350"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="359"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="369"/>
        <source>Location</source>
        <translation>Localização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="379"/>
        <source>Required Plugins</source>
        <translation>Plugins Necessários</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="389"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="401"/>
        <source>Auto Load</source>
        <translation>Carregamento Automático</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="409"/>
        <source>Delayed Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="484"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="496"/>
        <source>Remove plugin?</source>
        <translation>Remover plugin?</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="485"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="497"/>
        <source>Are you sure you want to remove this plugin?</source>
        <translation>Tem certeza de que deseja remover este plugin?</translation>
    </message>
</context>
<context>
    <name>pqPresetDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="16"/>
        <source>Choose Preset</source>
        <translation>Escolher Predefinição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="56"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Tip: &amp;lt;click&amp;gt; to select, &amp;lt;double-click&amp;gt; to apply a preset.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Dica: &amp;lt;clique&amp;gt; para selecionar, &amp;lt;clique duplo&amp;gt; para aplicar uma predefinição.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="65"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Options to load:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Opções para carregar:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uncheck to not load colors from the selected preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Desmarque para não carregar cores da predefinição selecionada.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="82"/>
        <source>Colors</source>
        <translation>Cores</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="95"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uncheck to not load opacities from the selected preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Desmarque para não carregar opacidades da predefinição selecionada.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="98"/>
        <source>Opacities</source>
        <translation>Opacidades</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="111"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uncheck to not load annotations from the selected preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Desmarque para não carregar anotações da predefinição selecionada.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="114"/>
        <source>Annotations</source>
        <translation>Anotações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="127"/>
        <source>Check to use following regexp when loading annotations from the selected preset.</source>
        <translation>Marque para usar a expressão regular a seguir ao carregar anotações da predefinição selecionada.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="130"/>
        <source>Use Regular Expression</source>
        <translation>Usar Expressão Regular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="143"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This regexp will be applied to seriesName and preset value. If any, matching groups are used, otherwise whole matching. &lt;/p&gt;
           E.g. series name is `y1 ˓custom˒`, preset have `y1 ˓generic˒` and `z1 ˓custom˒`. Then with the regexp `(.*) ˓.*˒` the `y1 ˓custom˒` series will match with `y1 ˓generic˒`, whereas with `.* (˓custom˒)` it will match with `z1 ˓custom˒`.
           &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
       </source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esta expressão regular será aplicada ao nome da série e valor da predefinição. Se houver, os grupos correspondentes são usados, caso contrário, toda a correspondência. &lt;/p&gt;
           Por exemplo, o nome da série é `y1 ˓custom˒`, a predefinição tem `y1 ˓generic˒` e `z1 ˓custom˒`. Então, com a expressão regular `(.*) ˓.*˒`, a série `y1 ˓custom˒` corresponderá a `y1 ˓generic˒`, enquanto com `.* (˓custom˒)` corresponderá a `z1 ˓custom˒`.
           &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
       </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="149"/>
        <source>(.*)</source>
        <translation>(.*)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="159"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to use data range specified in the preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Marque para usar o intervalo de dados especificado na predefinição.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="162"/>
        <source>Use preset range</source>
        <translation>Usar intervalo da predefinição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="182"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Actions on selected:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ações no selecionado:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="196"/>
        <source>Show current preset
in default mode</source>
        <translation>Mostrar predefinição atual
no modo padrão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="220"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="227"/>
        <source>Import a preset from a json file, imported preset will appear in italics</source>
        <translation>Importar uma predefinição de um arquivo json, a predefinição importada aparecerá em itálico</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="230"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="240"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="250"/>
        <source>Remove</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="257"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="869"/>
        <source>Import Presets</source>
        <translation>Importar Predefinições</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="870"/>
        <source>Supported Presets/Color Map Files</source>
        <translation>Arquivos de Predefinições/Mapa de Cores Suportados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="871"/>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="911"/>
        <source>ParaView Color/Opacity Presets</source>
        <translation>Predefinições de Cor/Opacidade do ParaView</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="871"/>
        <source>Legacy Color Maps</source>
        <translation>Mapas de Cores Antigos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="872"/>
        <source>VisIt Color Table</source>
        <translation>Tabela de Cores do VisIt</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="872"/>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="911"/>
        <source>All Files</source>
        <translation>Todos os Arquivos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="910"/>
        <source>Export Preset(s)</source>
        <translation>Exportar Predefinição(ões)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="941"/>
        <source>Failed to save preset collection to </source>
        <translation>Falha ao salvar a coleção de predefinições em </translation>
    </message>
</context>
<context>
    <name>pqPropertiesPanel</name>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="457"/>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="475"/>
        <source>Properties</source>
        <translation>Propriedades</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="529"/>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="536"/>
        <source>Display</source>
        <translation>Exibição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="581"/>
        <source>View (%1)</source>
        <translation>Visualizar (%1)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="590"/>
        <source>View</source>
        <translation>Visualizar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="765"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="829"/>
        <source>Delete</source>
        <translation>Excluir</translation>
    </message>
</context>
<context>
    <name>pqPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqPropertyWidget.cxx" line="131"/>
        <source>Property Changed</source>
        <translation>Propriedade Alterada</translation>
    </message>
</context>
<context>
    <name>pqProxyAction</name>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="63"/>
        <source>(deprecated) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="224"/>
        <source>Requires an input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="239"/>
        <source>Not supported in parallel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="243"/>
        <source>Supported only in parallel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="255"/>
        <source>Multiple inputs not supported</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyEditorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqProxyEditorPropertyWidget.cxx" line="26"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
</context>
<context>
    <name>pqProxyInformationWidget</name>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="395"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="396"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="500"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="555"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="564"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="588"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="589"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="590"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="618"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="619"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="620"/>
        <source>n/a</source>
        <translation>n/d</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="529"/>
        <source>Data Statistics (# of datasets: %1)</source>
        <translation>Estatísticas dos Dados (# de conjuntos de dados: %1)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="530"/>
        <source>Data Statistics</source>
        <translation>Estatísticas dos Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="570"/>
        <source>%1 to %2 (delta: %3)
%4 to %5 (delta: %6)
%7 to %8 (delta: %9)</source>
        <translation>%1 a %2 (delta: %3)
%4 a %5 (delta: %6)
%7 a %8 (delta: %9)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="599"/>
        <source>%1 to %2 (dimension: %3)
%4 to %5 (dimension: %6)
%7 to %8 (dimension: %9)</source>
        <translation>%1 a %2 (dimensão: %3)
%4 a %5 (dimensão: %6)
%7 a %8 (dimensão: %9)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="636"/>
        <source>%1 (range: [%2, %3])</source>
        <translation>%1 (intervalo: [%2, %3])</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="671"/>
        <source>Show hierarchy / assembly as text</source>
        <translation>Mostrar hierarquia / montagem como texto</translation>
    </message>
</context>
<context>
    <name>pqProxyWidget</name>
    <message>
        <location filename="../Qt/Components/pqProxyWidget.cxx" line="1570"/>
        <source>Use as Default</source>
        <translation>Usar como Padrão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyWidget.cxx" line="1584"/>
        <source>Reset to Application Default</source>
        <translation>Redefinir para o Padrão do Aplicativo</translation>
    </message>
</context>
<context>
    <name>pqRecentFilesMenu</name>
    <message>
        <location filename="../Qt/Components/pqRecentFilesMenu.cxx" line="190"/>
        <source>Disconnect from current server?</source>
        <translation>Desconectar do servidor atual?</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqRecentFilesMenu.cxx" line="191"/>
        <source>The file you opened requires connecting to a new server.
The current connection will be closed.

Are you sure you want to continue?</source>
        <translation>O arquivo que você abriu requer conexão a um novo servidor.
A conexão atual será fechada.

Tem certeza de que deseja continuar?</translation>
    </message>
</context>
<context>
    <name>pqRemoteCommandDialog</name>
    <message>
        <location filename="../Qt/Components/pqRemoteCommandDialog.cxx" line="543"/>
        <source>All Files</source>
        <translation>Todos os Arquivos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqRemoteCommandDialog.cxx" line="545"/>
        <source>Find file</source>
        <translation>Localizar arquivo</translation>
    </message>
</context>
<context>
    <name>pqRemoteCommandDialogForm</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="28"/>
        <source>Remote Command Editor</source>
        <translation>Editor de Comando Remoto</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="34"/>
        <source>Command Template</source>
        <translation>Modelo de Comando</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="95"/>
        <source>Add</source>
        <translation>Adicionar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="126"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="154"/>
        <source>Delete</source>
        <translation>Excluir</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="172"/>
        <source>Parameters</source>
        <translation>Parâmetros</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="181"/>
        <source>FE_URL</source>
        <translation>FE_URL</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="191"/>
        <source>ssh url to cluster login (eg user@login.com)</source>
        <translation>url ssh para login no cluster (ex: user@login.com)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="198"/>
        <source>SSH_EXEC</source>
        <translation>SSH_EXEC</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="207"/>
        <source>path to ssh</source>
        <translation>caminho para ssh</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="246"/>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="303"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="255"/>
        <source>TERM_EXEC</source>
        <translation>TERM_EXEC</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="264"/>
        <source>path to terminal</source>
        <translation>caminho para o terminal</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="312"/>
        <source>TERM_OPTS</source>
        <translation>TERM_OPTS</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="319"/>
        <source>terminal options</source>
        <translation>opções do terminal</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="329"/>
        <source>PV_HOST</source>
        <translation>PV_HOST</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="342"/>
        <source>Command Preview</source>
        <translation>Pré-visualização do Comando</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="385"/>
        <source>execute</source>
        <translation>executar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="395"/>
        <source>cancel</source>
        <translation>cancelar</translation>
    </message>
</context>
<context>
    <name>pqRemoteCommandTemplateDialogForm</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandTemplateDialogForm.ui" line="16"/>
        <source>Remote Command</source>
        <translation>Comando Remoto</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandTemplateDialogForm.ui" line="22"/>
        <source>Name:</source>
        <translation>Nome:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandTemplateDialogForm.ui" line="36"/>
        <source>Template:</source>
        <translation>Modelo:</translation>
    </message>
</context>
<context>
    <name>pqRescaleRangeDialog</name>
    <message>
        <source>Set Range</source>
        <translation type="vanished">Definir Intervalo</translation>
    </message>
    <message>
        <source>Enter the range for the opacity map</source>
        <translation type="vanished">Insira o intervalo para o mapa de opacidade</translation>
    </message>
    <message>
        <source>Enter the new range minimum for the opacity map here.</source>
        <translation type="vanished">Insira aqui o novo valor mínimo do intervalo para o mapa de opacidade.</translation>
    </message>
    <message>
        <source>minimum</source>
        <translation type="vanished">mínimo</translation>
    </message>
    <message>
        <source>-</source>
        <translation type="vanished">-</translation>
    </message>
    <message>
        <source>Rescale and lock the color map to avoid automatic rescaling.</source>
        <translation type="vanished">Reescale e trave o mapa de cores para evitar o reescalonamento automático.</translation>
    </message>
    <message>
        <source>Rescale and disable automatic rescaling</source>
        <translation type="vanished">Reescale e desative o reescalonamento automático</translation>
    </message>
    <message>
        <source>Rescale and leave automatic rescaling mode unchanged.</source>
        <translation type="vanished">Reescale e deixe o modo de reescalonamento automático inalterado.</translation>
    </message>
    <message>
        <source>Rescale</source>
        <translation type="vanished">Reescalonar</translation>
    </message>
    <message>
        <source>Close without rescaling</source>
        <translation type="vanished">Fechar sem reescalonar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancelar</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter the range for the color map&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Insira o intervalo para o mapa de cores&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Enter the new range minimum for the color map here.</source>
        <translation type="vanished">Insira aqui o novo valor mínimo do intervalo para o mapa de cores.</translation>
    </message>
    <message>
        <source>Enter the new range maximum for the color map here.</source>
        <translation type="vanished">Insira aqui o novo valor máximo do intervalo para o mapa de cores.</translation>
    </message>
    <message>
        <source>maximum</source>
        <translation type="vanished">máximo</translation>
    </message>
    <message>
        <source>Enter the new range maximum for the opacity map here.</source>
        <translation type="vanished">Insira aqui o novo valor máximo do intervalo para o mapa de opacidade.</translation>
    </message>
</context>
<context>
    <name>pqScalarValueListPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqScalarValueListPropertyWidget.cxx" line="353"/>
        <location filename="../Qt/Components/pqScalarValueListPropertyWidget.cxx" line="354"/>
        <source>unknown</source>
        <translation>desconhecido</translation>
    </message>
</context>
<context>
    <name>pqSelectReaderDialog</name>
    <message>
        <location filename="../Qt/Components/pqSelectReaderDialog.cxx" line="65"/>
        <source>A reader for &quot;%1&quot; could not be found. Please choose one:</source>
        <translation>Não foi possível encontrar um leitor para &quot;%1&quot;. Por favor, escolha um:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectReaderDialog.cxx" line="80"/>
        <source>More than one reader for &quot;%1&quot; found. Please choose one:</source>
        <translation>Mais de um leitor encontrado para &quot;%1&quot;. Por favor, escolha um:</translation>
    </message>
</context>
<context>
    <name>pqSelectionInputWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionInputWidget.ui" line="23"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionInputWidget.ui" line="50"/>
        <source>Copy Active Selection</source>
        <translation>Copiar Seleção Ativa</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionInputWidget.ui" line="72"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="81"/>
        <source>Copied Selection</source>
        <translation>Seleção Copiada</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="76"/>
        <source>No selection</source>
        <translation>Nenhuma seleção</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="92"/>
        <source>Number of Selections: </source>
        <translation>Número de Seleções: </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="93"/>
        <source>Selection Expression: </source>
        <translation>Expressão de Seleção: </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="95"/>
        <source>Invert Selection: </source>
        <translation>Inverter Seleção: </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="96"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="97"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="114"/>
        <source>Elements: </source>
        <translation>Elementos: </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="120"/>
        <source>Type: </source>
        <translation>Tipo: </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="123"/>
        <source>Frustum Selection</source>
        <translation>Seleção de Frustum</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="123"/>
        <source>Values:</source>
        <translation>Valores:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="142"/>
        <source>Values Selection</source>
        <translation>Seleção de Valores</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="142"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="317"/>
        <source>Array Name: </source>
        <translation>Nome do Array: </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="144"/>
        <source>Values</source>
        <translation>Valores</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="162"/>
        <source>Pedigree ID Selection</source>
        <translation>Seleção de ID de Genealogia</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="167"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="185"/>
        <source>Pedigree Domain</source>
        <translation>Domínio de Genealogia</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="167"/>
        <source>String ID</source>
        <translation>ID de String</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="185"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="202"/>
        <source>Global ID Selection</source>
        <translation>Seleção de ID Global</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="202"/>
        <source>Global ID</source>
        <translation>ID Global</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="214"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="233"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="252"/>
        <source>ID Selection</source>
        <translation>Seleção de ID</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="214"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="234"/>
        <source>Process ID</source>
        <translation>ID de Processo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="215"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="234"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="253"/>
        <source>Index</source>
        <translation>Índice</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="233"/>
        <source>Composite ID</source>
        <translation>ID Composto</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="253"/>
        <source>Level</source>
        <translation>Nível</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="253"/>
        <source>Dataset</source>
        <translation>Conjunto de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="272"/>
        <source>Location-based Selection</source>
        <translation>Seleção Baseada em Localização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="273"/>
        <source>Probe Locations</source>
        <translation>Localizações da Sonda</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="291"/>
        <source>Block Selection</source>
        <translation>Seleção de Bloco</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="291"/>
        <source>Blocks</source>
        <translation>Blocos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="304"/>
        <source>Block Selectors Selection</source>
        <translation>Seleção de Seletores de Bloco</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="304"/>
        <source>Assembly Name: </source>
        <translation>Nome da Montagem: </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="306"/>
        <source>Block Selectors</source>
        <translation>Seletores de Bloco</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="317"/>
        <source>Threshold Selection</source>
        <translation>Seleção de Limiar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="319"/>
        <source>Thresholds</source>
        <translation>Limiares</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="337"/>
        <source>Query Selection</source>
        <translation>Seleção de Consulta</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="337"/>
        <source>Query: </source>
        <translation>Consulta: </translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="349"/>
        <source>Number of Selections: 0</source>
        <translation>Número de Seleções: 0</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="433"/>
        <source>Copied Selection (Active Selection Changed)</source>
        <translation>Seleção Copiada (Seleção Ativa Alterada)</translation>
    </message>
</context>
<context>
    <name>pqSelectionManager</name>
    <message>
        <location filename="../Qt/Components/pqSelectionManager.cxx" line="175"/>
        <source>clear all selections</source>
        <translation>limpar todas as seleções</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionManager.cxx" line="193"/>
        <source>clear selection for source</source>
        <translation>limpar seleção para a fonte</translation>
    </message>
</context>
<context>
    <name>pqServerConnectDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="16"/>
        <source>Choose Server</source>
        <translation>Escolher Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="40"/>
        <source>Add Server</source>
        <translation>Adicionar Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="50"/>
        <source>Edit Server</source>
        <translation>Editar Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="60"/>
        <source>Delete Server</source>
        <translation>Excluir Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="67"/>
        <source>Load Servers</source>
        <translation>Carregar Servidores</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="74"/>
        <source>Save Servers</source>
        <translation>Salvar Servidores</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="81"/>
        <source>Fetch Servers</source>
        <translation>Obter Servidores</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="105"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="108"/>
        <source>Delete all user server configurations</source>
        <translation>Excluir todas as configurações de servidor do usuário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="111"/>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="661"/>
        <source>Delete All</source>
        <translation>Excluir Tudo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="135"/>
        <source>Timeout (s) </source>
        <translation>Tempo Limite (s) </translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="142"/>
        <source>Specify the timeout in seconds to use when waiting for server connection. Set it to -1 to wait indefinitely. This value will be saved in your settings for this server once you click connect.</source>
        <translation>Especifique o tempo limite em segundos a ser usado ao aguardar a conexão com o servidor. Defina como -1 para aguardar indefinidamente. Esse valor será salvo nas suas configurações para este servidor quando você clicar em conectar.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="161"/>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="171"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="214"/>
        <source>Configuration</source>
        <translation>Configuração</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="219"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="666"/>
        <source>Server</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="231"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="247"/>
        <source>Server Type</source>
        <translation>Tipo de Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="258"/>
        <source>Client / Server</source>
        <translation>Cliente / Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="263"/>
        <source>Client / Server (reverse connection)</source>
        <translation>Cliente / Servidor (conexão reversa)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="268"/>
        <source>Client / Data Server / Render Server</source>
        <translation>Cliente / Servidor de Dados / Servidor de Renderização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="273"/>
        <source>Client / Data Server / Render Server (reverse connection)</source>
        <translation>Cliente / Servidor de Dados / Servidor de Renderização (conexão reversa)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="281"/>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="288"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="322"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="356"/>
        <source>localhost</source>
        <translation>localhost</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="295"/>
        <source>Port</source>
        <translation>Porta</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="315"/>
        <source>Data Server host</source>
        <translation>Host do Servidor de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="329"/>
        <source>Data Server port</source>
        <translation>Porta do Servidor de Dados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="349"/>
        <source>Render Server host</source>
        <translation>Host do Servidor de Renderização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="363"/>
        <source>Render Server port</source>
        <translation>Porta do Servidor de Renderização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="414"/>
        <source>Configure</source>
        <translation>Configurar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="421"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="714"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="434"/>
        <source>Configure server foobar (cs://foobar)</source>
        <translation>Configurar servidor foobar (cs://foobar)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="441"/>
        <source>Please configure the startup procedure to be used when connecting to this server:</source>
        <translation>Por favor, configure o procedimento de inicialização a ser usado ao se conectar a este servidor:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="468"/>
        <source>Startup Type:</source>
        <translation>Tipo de Inicialização:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="479"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="484"/>
        <source>Command</source>
        <translation>Comando</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="526"/>
        <source>Manual Startup - no attempt will be made to start the server.  You must start the server manually before trying to connect.</source>
        <translation>Inicialização Manual - nenhuma tentativa será feita para iniciar o servidor. Você deve iniciar o servidor manualmente antes de tentar se conectar.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="549"/>
        <source>Execute an external command to start the server:</source>
        <translation>Execute um comando externo para iniciar o servidor:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="567"/>
        <source>After executing the startup command, wait</source>
        <translation>Após executar o comando de inicialização, aguarde</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="574"/>
        <source> seconds</source>
        <translation> segundos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="590"/>
        <source>before connecting.</source>
        <translation>antes de conectar.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="661"/>
        <source>Configuration Name</source>
        <translation>Nome da Configuração</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="671"/>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="681"/>
        <source>Edit Sources</source>
        <translation>Editar Fontes</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="704"/>
        <source>Import Selected</source>
        <translation>Importar Selecionados</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="730"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Enter list of URLs to obtain the servers configurations from, using the syntax:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;lt;type&amp;gt; &amp;lt;url&amp;gt; &amp;lt;userfriendly name&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Insira a lista de URLs para obter as configurações dos servidores, usando a sintaxe:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;lt;tipo&amp;gt; &amp;lt;url&amp;gt; &amp;lt;nome amigável&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="45"/>
        <source>Enter list of URLs to obtain server configurations from.</source>
        <translation>Insira a lista de URLs para obter as configurações do servidor.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="47"/>
        <source>Syntax:</source>
        <translation>Sintaxe:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="48"/>
        <source>userfriendly-name</source>
        <translation>nome-amigável</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="49"/>
        <source>Official Kitware Server Configurations</source>
        <translation>Configurações Oficiais do Servidor Kitware</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="230"/>
        <source>Edit Server Configuration</source>
        <translation>Editar Configuração do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="234"/>
        <source>Edit Server Launch Configuration</source>
        <translation>Editar Configuração de Inicialização do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="238"/>
        <source>Fetch Server Configurations</source>
        <translation>Obter Configurações do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="242"/>
        <source>Edit Server Configuration Sources</source>
        <translation>Editar Fontes de Configuração do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="247"/>
        <source>Choose Server Configuration</source>
        <translation>Escolher Configuração do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="302"/>
        <source>My Server</source>
        <translation>Meu Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="645"/>
        <source>Delete Server Configuration</source>
        <translation>Excluir Configuração do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="646"/>
        <source>Are you sure you want to delete &quot;%1&quot;?</source>
        <translation>Tem certeza de que deseja excluir &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="661"/>
        <source>All servers will be deleted. Are you sure?</source>
        <translation>Todos os servidores serão excluídos. Tem certeza?</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="675"/>
        <source>Save Server Configuration File</source>
        <translation>Salvar Arquivo de Configuração do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="693"/>
        <source>Load Server Configuration File</source>
        <translation>Carregar Arquivo de Configuração do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="830"/>
        <source>Fetching configurations ...</source>
        <translation>Obtendo configurações ...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="851"/>
        <source>Authenticate Connection</source>
        <translation>Autenticar Conexão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="853"/>
        <source>%1 at %2</source>
        <translation>%1 em %2</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="856"/>
        <source>Accept</source>
        <translation>Aceitar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="860"/>
        <source>Username</source>
        <translation>Nome de Usuário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="861"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
</context>
<context>
    <name>pqServerLauncher</name>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="223"/>
        <source>Connection Options for &quot;%1&quot;</source>
        <translation>Opções de Conexão para &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="616"/>
        <source>Establishing connection to &apos;%1&apos;
Waiting for server to connect.</source>
        <translation>Estabelecendo conexão com &apos;%1&apos;
Aguardando o servidor se conectar.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="619"/>
        <source>Waiting for Server Connection</source>
        <translation>Aguardando Conexão do Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="623"/>
        <source>Establishing connection to &apos;%1&apos;
Waiting %2 seconds for connection to server.</source>
        <translation>Estabelecendo conexão com &apos;%1&apos;
Aguardando %2 segundos pela conexão com o servidor.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="627"/>
        <source>Waiting for Connection to Server</source>
        <translation>Aguardando Conexão com o Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="643"/>
        <source>Connection Failed</source>
        <translation>Falha na Conexão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="644"/>
        <source>Unable to connect sucessfully. Try again for %1 seconds ?</source>
        <translation>Não foi possível conectar com sucesso. Tentar novamente por %1 segundos?</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="746"/>
        <source>Launching server &apos;%1&apos;</source>
        <translation>Iniciando servidor &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>pqServerLauncherDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerLauncherDialog.ui" line="16"/>
        <source>Starting Server</source>
        <translation>Iniciando Servidor</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerLauncherDialog.ui" line="28"/>
        <source>Please wait while server cs://foobar starts ...</source>
        <translation>Por favor, aguarde enquanto o servidor cs://foobar inicia ...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerLauncherDialog.ui" line="59"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>pqSetBreakpointDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="16"/>
        <source>Set Breakpoint</source>
        <translation>Definir Ponto de Interrupção</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="22"/>
        <source>Breakpoint:</source>
        <translation>Ponto de Interrupção:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="32"/>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="52"/>
        <source>Time Step</source>
        <translation>Passo de Tempo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSetBreakpointDialog.cxx" line="64"/>
        <source>Breakpoint time is invalid or it is smaller than current time</source>
        <translation>O tempo do ponto de interrupção é inválido ou é menor que o tempo atual</translation>
    </message>
</context>
<context>
    <name>pqShaderReplacementsSelectorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="61"/>
        <source>List of previously loaded Json files</source>
        <translation>Lista de arquivos Json carregados anteriormente</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="67"/>
        <source>Load</source>
        <translation>Carregar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="68"/>
        <source>Load a Json preset file</source>
        <translation>Carregar um arquivo de predefinição Json</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="74"/>
        <source>Delete</source>
        <translation>Excluir</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="75"/>
        <source>Delete the selected preset from the preset list or clear the current shader replacement string.</source>
        <translation>Excluir a predefinição selecionada da lista de predefinições ou limpar a string atual de substituição do shader.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="138"/>
        <source>Shader Replacements Change</source>
        <translation>Alteração de Substituições de Shader</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="168"/>
        <source>Shader replacements files</source>
        <translation>Arquivos de substituições de shader</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="168"/>
        <source>All files (*)</source>
        <translation>Todos os arquivos (*)</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="169"/>
        <source>Open ShaderReplacements:</source>
        <translation>Abrir Substituições de Shader:</translation>
    </message>
</context>
<context>
    <name>pqSignalAdaptorKeyFrameType</name>
    <message>
        <location filename="../Qt/Components/pqSignalAdaptorKeyFrameType.cxx" line="99"/>
        <source>Amplitude</source>
        <translation>Amplitude</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSignalAdaptorKeyFrameType.cxx" line="103"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
</context>
<context>
    <name>pqSpreadSheetColumnsVisibility</name>
    <message>
        <location filename="../Qt/Components/pqSpreadSheetColumnsVisibility.cxx" line="128"/>
        <source>All Columns</source>
        <translation>Todas as Colunas</translation>
    </message>
</context>
<context>
    <name>pqStringVectorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqStringVectorPropertyWidget.cxx" line="143"/>
        <source>Select %1</source>
        <translation>Selecionar %1</translation>
    </message>
</context>
<context>
    <name>pqTabWidget</name>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="120"/>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="121"/>
        <source>Close layout</source>
        <translation>Fechar layout</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="134"/>
        <source>Bring popped out window back to the frame</source>
        <translation>Trazer janela destacada de volta para o quadro</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="135"/>
        <source>Pop out layout in separate window</source>
        <translation>Destacar layout em janela separada</translation>
    </message>
</context>
<context>
    <name>pqTabbedMultiViewWidget</name>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="689"/>
        <source>Remove View Tab</source>
        <translation>Remover Aba de Visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="719"/>
        <source>Add View Tab</source>
        <translation>Adicionar Aba de Visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="763"/>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="920"/>
        <source>Close Tab</source>
        <translation>Fechar Aba</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="910"/>
        <source>Rename</source>
        <translation>Renomear</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="911"/>
        <source>Close layout</source>
        <translation>Fechar layout</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="912"/>
        <source>Equalize Views</source>
        <translation>Equalizar Visualizações</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="913"/>
        <source>Horizontally</source>
        <translation>Horizontalmente</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="914"/>
        <source>Vertically</source>
        <translation>Verticalmente</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="915"/>
        <source>Both</source>
        <translation>Ambos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="929"/>
        <source>Rename Layout...</source>
        <translation>Renomear Layout...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="929"/>
        <source>New name:</source>
        <translation>Novo nome:</translation>
    </message>
</context>
<context>
    <name>pqTextureComboBox</name>
    <message>
        <location filename="../Qt/Components/pqTextureComboBox.cxx" line="99"/>
        <source>None</source>
        <translation>Nenhum</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureComboBox.cxx" line="102"/>
        <source>Load ...</source>
        <translation>Carregar ...</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureComboBox.cxx" line="137"/>
        <source>Open Texture:</source>
        <translation>Abrir Textura:</translation>
    </message>
</context>
<context>
    <name>pqTextureSelectorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="32"/>
        <source>Select/Load texture to apply.</source>
        <translation>Selecionar/Carregar textura para aplicar.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="86"/>
        <source>Texture Change</source>
        <translation>Alteração de Textura</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="117"/>
        <source>No tcoords present in the data. Cannot apply texture.</source>
        <translation>Nenhuma coordenada de textura presente nos dados. Não é possível aplicar textura.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="123"/>
        <source>No tangents present in the data. Cannot apply texture.</source>
        <translation>Nenhuma tangente presente nos dados. Não é possível aplicar textura.</translation>
    </message>
</context>
<context>
    <name>pqTimerLogDisplay</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="16"/>
        <source>Timer Log</source>
        <translation>Log de Temporizador</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="36"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="43"/>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="71"/>
        <source>Time Threshold:</source>
        <translation>Limite de Tempo:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="104"/>
        <source>Buffer Length:</source>
        <translation>Tamanho do Buffer:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="116"/>
        <source>Enable</source>
        <translation>Habilitar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="126"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTimerLogDisplay.cxx" line="264"/>
        <source>Save Timer Log</source>
        <translation>Salvar Log de Temporizador</translation>
    </message>
</context>
<context>
    <name>pqTransferFunction2DWidget</name>
    <message>
        <location filename="../Qt/Components/pqTransferFunction2DWidget.cxx" line="214"/>
        <source>Double click to add a box. Grab and drag to move the box.</source>
        <translation>Clique duas vezes para adicionar uma caixa. Arraste para mover a caixa.</translation>
    </message>
</context>
<context>
    <name>pqTransferFunctionWidget</name>
    <message>
        <location filename="../Qt/Components/pqTransferFunctionWidget.cxx" line="698"/>
        <source>Drag a handle to change the range. Double click it to set custom range. Return/Enter to edit color.</source>
        <translation>Arraste um identificador para alterar o intervalo. Clique duas vezes nele para definir um intervalo personalizado. Pressione Return/Enter para editar a cor.</translation>
    </message>
</context>
<context>
    <name>pqViewFrame</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqViewFrame.ui" line="19"/>
        <source>Form</source>
        <translation>Formulário</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="63"/>
        <source>Split Vertical Axis</source>
        <translation>Dividir Eixo Vertical</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="66"/>
        <source>Split Horizontal Axis</source>
        <translation>Dividir Eixo Horizontal</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="70"/>
        <source>Maximize</source>
        <translation>Maximizar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="74"/>
        <source>Restore</source>
        <translation>Restaurar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="78"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
</context>
<context>
    <name>propertiesPanel</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="16"/>
        <source>Properties Panel</source>
        <translation>Painel de Propriedades</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="27"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="41"/>
        <source>Resets any changed properties to their values from the last time &apos;Apply&apos; was clicked.</source>
        <translation>Redefine quaisquer propriedades alteradas para seus valores da última vez que &apos;Aplicar&apos; foi clicado.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="44"/>
        <source>Reset</source>
        <translation>Redefinir</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="55"/>
        <source>Delete</source>
        <translation>Excluir</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="66"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="73"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="132"/>
        <source>Properties</source>
        <translation>Propriedades</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="151"/>
        <source>Copy properties</source>
        <translation>Copiar propriedades</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="174"/>
        <source>Paste copied properties</source>
        <translation>Colar propriedades copiadas</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="194"/>
        <source>Restore application default setting values</source>
        <translation>Restaurar valores de configuração padrão do aplicativo</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="214"/>
        <source>Save current settings values as default</source>
        <translation>Salvar valores de configuração atuais como padrão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="252"/>
        <source>Display</source>
        <translation>Exibição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="271"/>
        <source>Copy display properties</source>
        <translation>Copiar propriedades de exibição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="294"/>
        <source>Paste copied display properties</source>
        <translation>Colar propriedades de exibição copiadas</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="314"/>
        <source>Restore application default setting values for display</source>
        <translation>Restaurar valores de configuração padrão do aplicativo para exibição</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="334"/>
        <source>Save current display settings values as default</source>
        <translation>Salvar valores de configuração de exibição atuais como padrão</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="372"/>
        <source>View</source>
        <translation>Visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="391"/>
        <source>Copy view properties</source>
        <translation>Copiar propriedades de visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="414"/>
        <source>Paste view properties</source>
        <translation>Colar propriedades de visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="434"/>
        <source>Restore application default setting values for view</source>
        <translation>Restaurar valores de configuração padrão do aplicativo para visualização</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="454"/>
        <source>Save current view settings values as default</source>
        <translation>Salvar valores de configuração de visualização atuais como padrão</translation>
    </message>
</context>
</TS>
