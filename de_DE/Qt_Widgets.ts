<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>QuickLaunchDialog</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqQuickLaunchDialog.ui" line="16"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
</context>
<context>
    <name>SeriesGeneratorDialog</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="16"/>
        <source>Generate Number Series</source>
        <translation>Zahlenfolge generieren</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="23"/>
        <source>Linear</source>
        <translation>Linear</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="28"/>
        <source>Logarithmic</source>
        <translation>Logarithmisch</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="33"/>
        <source>Geometric (samples)</source>
        <translation>Geometrisch (Stichproben)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="38"/>
        <source>Geometric (common ratio)</source>
        <translation>Geometrisch (gemeinsamer Quotient)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="58"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="81"/>
        <source>Reset using current data range values</source>
        <translation type="unfinished">Zurücksetzen mittels aktueller Datenbereichswerte</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="90"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="97"/>
        <source>Number of Samples:</source>
        <translation>Anzahl der Stichproben:</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="117"/>
        <source>Common Ratio:</source>
        <translation>Gemeinsamer Quotient:</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="124"/>
        <source>1.1</source>
        <translation>1.1</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqSeriesGeneratorDialog.ui" line="146"/>
        <source>Range</source>
        <translation>Bereich</translation>
    </message>
</context>
<context>
    <name>pqColorChooserButton</name>
    <message>
        <location filename="../Qt/Widgets/pqColorChooserButton.cxx" line="154"/>
        <source>Set Color</source>
        <translation>Farbe einstellen</translation>
    </message>
</context>
<context>
    <name>pqExpanderButton</name>
    <message>
        <location filename="../Qt/Widgets/Resources/UI/pqExpanderButton.ui" line="68"/>
        <source>Properties</source>
        <translation>Eigenschaften</translation>
    </message>
</context>
<context>
    <name>pqProgressWidget</name>
    <message>
        <location filename="../Qt/Widgets/pqProgressWidget.cxx" line="114"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>pqQuickLaunchDialog</name>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="81"/>
        <source>Type to search.</source>
        <translation>Zum Suchen tippen.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="82"/>
        <source>Enter</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="83"/>
        <source>to create selected source/filter.</source>
        <translation>um ausgewähle(n) Quelle/Filter zu erstellen.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="84"/>
        <source>Shift + Enter</source>
        <translation>Shift + Enter</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="85"/>
        <source>to create and apply selected source/filter. </source>
        <translation>um ausgewählte(n) Quelle/Filter anzuwenden. </translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="87"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqQuickLaunchDialog.cxx" line="88"/>
        <source>to cancel.</source>
        <translation>um abzubrechen.</translation>
    </message>
</context>
<context>
    <name>pqScaleByButton</name>
    <message>
        <location filename="../Qt/Widgets/pqScaleByButton.cxx" line="46"/>
        <source>Scale by ...</source>
        <translation>Skaliere durch ...</translation>
    </message>
</context>
<context>
    <name>pqSeriesGeneratorDialog</name>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="56"/>
        <source>Error: range cannot contain 0 for log.</source>
        <translation>Fehler: der Bereich für das Protokoll darf nicht 0 enthalten.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="63"/>
        <source>Error: range cannot begin or end with 0 for a geometric series.</source>
        <translation>Fehler: der Bereich der geometrischen Reihe darf nicht mit 0 beginnen oder enden.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="67"/>
        <source>Error: range cannot contain 0 for a geometric series.</source>
        <translation>Fehler: der Bereich für eine geometrische Reihe darf nicht 0 enthalten.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="74"/>
        <source>Error: range cannot begin with 0 for a geometric series.</source>
        <translation>Fehler: der Bereich für eine geometrische Reihe darf nicht mit 0 beginnen.</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="78"/>
        <source>Error: common ratio cannot be 0 for a geometric series.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="93"/>
        <source>Sample series: </source>
        <translation>Stichprobenserie: </translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqSeriesGeneratorDialog.cxx" line="191"/>
        <source>&amp;Generate</source>
        <translation>&amp;Generieren</translation>
    </message>
</context>
<context>
    <name>pqTreeViewSelectionHelper</name>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="154"/>
        <source>Filter items (regex)</source>
        <translation>Objekte filtern (regex)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="181"/>
        <source>Check highlighted items</source>
        <translation>Markierte Elemente anwählen</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="189"/>
        <source>Uncheck highlighted items</source>
        <translation>Markierte Elemente abwählen</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="211"/>
        <source>Sort (ascending)</source>
        <translation>Sortieren (aufsteigend)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="212"/>
        <source>Sort (descending)</source>
        <translation>Sortieren (absteigend)</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeViewSelectionHelper.cxx" line="222"/>
        <source>Clear sorting</source>
        <translation>Sortierung zurücksetzen</translation>
    </message>
</context>
<context>
    <name>pqTreeWidgetSelectionHelper</name>
    <message>
        <location filename="../Qt/Widgets/pqTreeWidgetSelectionHelper.cxx" line="66"/>
        <source>Check</source>
        <translation>Wählen</translation>
    </message>
    <message>
        <location filename="../Qt/Widgets/pqTreeWidgetSelectionHelper.cxx" line="67"/>
        <source>Uncheck</source>
        <translation>Abwählen</translation>
    </message>
</context>
</TS>
