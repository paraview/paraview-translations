<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>PythonShell</name>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="62"/>
        <source>Run Script</source>
        <translation>Skript ausführen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="72"/>
        <source>Clear</source>
        <translation type="unfinished">Leeren</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.ui" line="82"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>pqPythonEditorActions</name>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="38"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="41"/>
        <source>Create a new file</source>
        <translation>Eine neue Datei erstellen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="44"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="47"/>
        <source>Open an existing file</source>
        <translation>Eine vorhandene Datei öffnen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="50"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="53"/>
        <source>Save the document to disk</source>
        <translation>Dokument auf der Festplatte speichern</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="56"/>
        <source>Save &amp;As...</source>
        <translation>Speichern &amp;Unter...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="58"/>
        <source>Save the document under a new name</source>
        <translation>Dokument unter einem neuen Namen speichern</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="61"/>
        <source>Save As &amp;Macro...</source>
        <translation>Als &amp;Makro speichern...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="63"/>
        <source>Save the document as a Macro</source>
        <translation>Dokument als Makro speichern</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="66"/>
        <source>Save As &amp;Script...</source>
        <translation>Als &amp;Skript speichern...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="68"/>
        <source>Save the document as a Script</source>
        <translation>Dokument als Skript speichern</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="71"/>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="373"/>
        <source>Delete All</source>
        <translation>Alle löschen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="73"/>
        <source>Delete all scripts from disk</source>
        <translation>Alle Skripte von der Festplatte löschen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="76"/>
        <source>Run...</source>
        <translation>Auführen...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="78"/>
        <source>Run the currently edited script</source>
        <translation>Das aktuell bearbeitete Skript ausführen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="81"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="84"/>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation>Inhalte der aktuellen Auswahl in die Zwischenablage ausschneiden</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="89"/>
        <source>Undo</source>
        <translation>Rückgängig</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="92"/>
        <source>Undo the last edit of the file</source>
        <translation>Letzte Änderung der Datei rückgängig machen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="95"/>
        <source>Redo</source>
        <translation>Wiederholen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="98"/>
        <source>Redo the last undo of the file</source>
        <translation>Den letzten Rückgängig-Schritt der Datei wiederholen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="101"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="104"/>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation>Inhalt der aktuellen Auswahl in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="109"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="112"/>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation>Inhalte aus der Zwischenablage in die aktuelle Auswahl einfügen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="117"/>
        <source>C&amp;lose</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="120"/>
        <source>Close the script editor</source>
        <translation>Skripteditor schließen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="123"/>
        <source>Close Current Tab</source>
        <translation>Aktuelle Registerkarte schließen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="126"/>
        <source>Close the current opened tab</source>
        <translation>Die aktuell geöffnete Registerkarte schließen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="356"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="357"/>
        <source>Python Files</source>
        <translation>Python-Dateien</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonEditorActions.cxx" line="374"/>
        <source>All scripts will be deleted. Are you sure?</source>
        <translation>Alle Skripte werden gelöscht. Sind Sie sicher?</translation>
    </message>
</context>
<context>
    <name>pqPythonFileIO</name>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="34"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="287"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="314"/>
        <source>Sorry!</source>
        <translation>Entschuldigung!</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="35"/>
        <source>Could not create user PythonSwap directory: %1.</source>
        <translation>Benutzer-PyhtonSwap-Verzeichnis konnte nicht erstellt werden: %1.</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="80"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="94"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="81"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="95"/>
        <source>No Filename Given!</source>
        <translation>Kein Dateiname angegeben!</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="102"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="183"/>
        <source>Script Editor</source>
        <translation>Skript-Editor</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="103"/>
        <source>Paraview found an old automatic save file %1. Would you like to recover its content?</source>
        <translation>Paraview hat eine alte automatische Sicherungsdatei %1 gefunden. Möchten Sie den Inhalt wiederherstellen?</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="184"/>
        <source>The document has been modified.
 Do you want to save your changes?</source>
        <translation>Das Dokument wurde geändert.
 Möchten Sie Ihre Änderungen speichern?</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="260"/>
        <source>Save File As</source>
        <translation>Datei speichern unter</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="261"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="294"/>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="322"/>
        <source>Python Files</source>
        <translation>Python-Dateien</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="288"/>
        <source>Could not create user Macro directory: %1.</source>
        <translation>Benutzer-Makroverzeichnis konnte nicht erstellt werden: %1.</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="294"/>
        <source>Save As Macro</source>
        <translation>Als Makro speichern</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="315"/>
        <source>Could not create user Script directory: %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonFileIO.cxx" line="322"/>
        <source>Save As Script</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonMacroSupervisor</name>
    <message>
        <location filename="../Qt/Python/pqPythonMacroSupervisor.cxx" line="115"/>
        <source>empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonScriptEditor</name>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="44"/>
        <source>ParaView Python Script Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="48"/>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="54"/>
        <source>%1[*] - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="48"/>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="54"/>
        <source>Script Editor</source>
        <translation type="unfinished">Skript-Editor</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="49"/>
        <source>File %1 saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="55"/>
        <source>File %1 opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="126"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="140"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="151"/>
        <source>&amp;Scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="156"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="157"/>
        <source>Open a python script in a new tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="161"/>
        <source>Load script into current editor tab...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="163"/>
        <source>Load a python script in the current opened tab and override its content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="166"/>
        <source>Delete...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="167"/>
        <source>Delete the script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="170"/>
        <source>Run...</source>
        <translation type="unfinished">Auführen...</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="172"/>
        <source>Load a python script in a new tab and run it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonScriptEditor.cxx" line="184"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonShell</name>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="162"/>
        <source>resetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="182"/>
        <source>
Python %1 on %2
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="445"/>
        <source>Run Script</source>
        <translation type="unfinished">Skript ausführen</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="446"/>
        <source>Python Files</source>
        <translation type="unfinished">Python-Dateien</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="446"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="469"/>
        <source>Error: script </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonShell.cxx" line="469"/>
        <source> was empty or could not be opened.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonTabWidget</name>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="151"/>
        <source>Sorry!</source>
        <translation type="unfinished">Entschuldigung!</translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="152"/>
        <source>Cannot open file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="204"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="275"/>
        <source>Close Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="402"/>
        <location filename="../Qt/Python/pqPythonTabWidget.cxx" line="404"/>
        <source>New File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
