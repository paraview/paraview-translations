<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AbortAnimation</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="19"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="39"/>
        <source>Abort Saving Animation</source>
        <translation>Speichern der Animation abbrechen</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="42"/>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="45"/>
        <source>Interrupts the saving of animation and aborts it.</source>
        <translation type="unfinished">Bricht den Speichervorgang der Animation ab.</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAbortAnimation.ui" line="48"/>
        <source>Abort Animation</source>
        <translation>Animation abbrechen</translation>
    </message>
</context>
<context>
    <name>AnimationTimeWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="22"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="43"/>
        <source>Time:</source>
        <translation>Zeit:</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="70"/>
        <source>max is N</source>
        <translation>max ist N</translation>
    </message>
</context>
<context>
    <name>CalculatorWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="16"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="45"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="61"/>
        <source>sin</source>
        <translation>sin</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="77"/>
        <source>asin</source>
        <translation>asin</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="93"/>
        <source>sinh</source>
        <translation>sinh</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="109"/>
        <source>dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="132"/>
        <source>(</source>
        <translation>(</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="148"/>
        <source>cos</source>
        <translation>cos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="164"/>
        <source>acos</source>
        <translation>acos</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="180"/>
        <source>cosh</source>
        <translation>cosh</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="196"/>
        <source>mag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="216"/>
        <source>)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="232"/>
        <source>tan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="248"/>
        <source>atan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="264"/>
        <source>tanh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="280"/>
        <source>norm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="300"/>
        <source>iHat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="316"/>
        <source>abs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="332"/>
        <source>ceil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="348"/>
        <source>x^y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="364"/>
        <source>ln</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="384"/>
        <source>jHat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="400"/>
        <source>sqrt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="416"/>
        <source>floor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="432"/>
        <source>exp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="448"/>
        <source>log10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="468"/>
        <source>kHat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="484"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="500"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="516"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="532"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="548"/>
        <source>Scalars</source>
        <translation>Skalare</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="558"/>
        <source>Vectors</source>
        <translation>Vektoren</translation>
    </message>
</context>
<context>
    <name>CameraKeyFrameWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="16"/>
        <source>Camera Animation</source>
        <translation>Kameraanimation</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="38"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="43"/>
        <source>Camera Position</source>
        <translation>Kameraposition</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="48"/>
        <source>Camera Focus</source>
        <translation>Kamerafokus</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="53"/>
        <source>Up Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="71"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:600;&quot;&gt;Define Camera Parameters&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt;&quot;&gt;Using the left pane, edit the path followed by the camera&apos;s position and focal point for the keyframe being edited.&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="94"/>
        <source>Position Control Points</source>
        <translation>Positions-Kontrollpunkte</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="111"/>
        <source>Focus Control Points</source>
        <translation>Fokus-Kontrollpunkte</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="185"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="201"/>
        <source>Focal Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="217"/>
        <source>View Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="233"/>
        <source>View Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="240"/>
        <source>Parallel Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="260"/>
        <source>Use Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="267"/>
        <source>Apply to camera</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindDataCurrentSelectionFrame</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="31"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Attribute:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="73"/>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="76"/>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="79"/>
        <source>Toggle column visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="93"/>
        <source>Toggle field data visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="96"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="113"/>
        <source>Invert the selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="116"/>
        <source>Invert selection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindDataSelectionDisplayFrame</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Selection Labels&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="48"/>
        <source>&lt;p&gt;Set the array to label selected cells with&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="51"/>
        <source>Cell Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="62"/>
        <source>&lt;p&gt;Set the array to label to selected points with&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="65"/>
        <source>Point Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="78"/>
        <source>Edit selection label properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="81"/>
        <source>Edit Label Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="97"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Selection Appearance&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="122"/>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="162"/>
        <source>&lt;p&gt;Set the color to use to show selected elements&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="125"/>
        <source>Selection Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="137"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Interactive Selection&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="165"/>
        <source>Interactive Selection Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="172"/>
        <source>Edit interactive selection label properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="175"/>
        <source>Edit Interactive Label Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LightsEditor</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="22"/>
        <source>Lights Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="61"/>
        <source>Turn on or off all the lights in the lighting kit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="64"/>
        <source>Light Kit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="74"/>
        <source>Reset lights to their default values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="77"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="102"/>
        <source>The two back lights, one on the left of the object as seen from the observer and one on the right, fill on the high-contrast areas behind the object. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="105"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="118"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="376"/>
        <source>Set the Key-to-Head ratio. Similar to the Key-to-Fill ratio, this ratio controls how bright the headlight light is compared to the key light: larger values correspond to a dimmer headlight light. The headlight is special kind of fill light, lighting only the parts of the object that the camera can see. As such, a headlight tends to reduce the contrast of a scene. It can be used to fill in &quot;shadows&quot; of the object missed by the key and fill lights. The headlight should always be significantly dimmer than the key light: ratios of 2 to 15 are typical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="131"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="451"/>
        <source>The &quot;Warmth&quot; of the Fill Light. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="134"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="227"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="522"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="532"/>
        <source>Warm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="141"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="272"/>
        <source>The Fill Light Azimuth. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="144"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="154"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="542"/>
        <source>Azi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="151"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="357"/>
        <source>The Key Light Azimuth. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="167"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="489"/>
        <source>The Back Light Elevation. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="192"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="425"/>
        <source>The Intensity of the Key Light.  0 = off and 1 = full intensity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="211"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="499"/>
        <source>Set the Key-to-Back Ratio. This ratio controls how bright the back lights are compared to the key light: larger values correspond to dimmer back lights. The back lights fill in the remaining high-contrast regions behind the object. Values between 2 and 10 are good.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="224"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="392"/>
        <source>The &quot;Warmth&quot; of the Headlight. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="240"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="519"/>
        <source>The &quot;Warmth&quot; of the Key Light. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="256"/>
        <source>The Fill Light is usually positioned across from or opposite from the key light (though still on the same side of the object as the camera) in order to simulate diffuse reflections from other objects in the scene.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="259"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="297"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="405"/>
        <source>The Fill Light Elevation. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="322"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="509"/>
        <source>The Key Light Elevation. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="341"/>
        <source>The Key Light is the main light.  It is usually positioned so that it appears like an overhead light (like the sun or a ceiling light).  It is generally positioned to shine down from about a 45 degree angle vertically and at least a little offset side to side.  The key light is usually at least about twice as bright as the total of all other lights in the scene to provide good modeling of object features.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="344"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="379"/>
        <source>K:H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="408"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="492"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="512"/>
        <source>Ele</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="415"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="584"/>
        <source>Set the Key-to-Fill Ratio. This ratio controls how bright the fill light is compared to the key light: larger values correspond to a dimmer fill light. The purpose of the fill light is to light parts of the object not lit by the key light, while still maintaining contrast. This type of lighting may correspond to indirect illumination from the key light, bounced off a wall, floor, or other object. The fill light should never be brighter than the key light: a good range for the key-to-fill ratio is between 2 and 10.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="418"/>
        <source>K:F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="428"/>
        <source>Int</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="435"/>
        <source>The headlight, always located at the position of the camera, reduces the contrast between areas lit by the key and fill light. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="438"/>
        <source>Head</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="470"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="539"/>
        <source>The Back Light Azimuth. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="502"/>
        <source>K:B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="529"/>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="565"/>
        <source>The &quot;Warmth&quot; of the Back Light. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="549"/>
        <source>If Maintain Luminance is set, the LightKit will attempt to maintain the apparent intensity of lights based on their perceptual brightnesses.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsEditor.ui" line="552"/>
        <source>Maintain Luminance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LightsInspector</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsInspector.ui" line="16"/>
        <source>Light Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLightsInspector.ui" line="25"/>
        <source>Add Light</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultiBlockInspectorWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="16"/>
        <source>MultiBlock Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="43"/>
        <source>Extract Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="65"/>
        <source>Show/Hide legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="93"/>
        <source>State Legend:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="121"/>
        <source>At least one property is disabled because no blocks are selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="151"/>
        <source>At least one property is inherited from the representation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="181"/>
        <source>At least one property is inherited from block(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="211"/>
        <source>At least one property is inherited from block(s) and the representation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="241"/>
        <source>At least one property is set in block(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="271"/>
        <source>At least one property is set in block(s) and inherited from the representation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="301"/>
        <source>At least one property is set in block(s) and inherited from block(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="331"/>
        <source>At least one property is set in block(s) and inherited from block(s) and the representation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OrbitCreatorDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="16"/>
        <source>Create Orbit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="22"/>
        <source>Orbit Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="28"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="44"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="60"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="94"/>
        <source>Reset Center</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopoutPlaceholder</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPopoutPlaceholder.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPopoutPlaceholder.ui" line="35"/>
        <source>Layout shown in separate window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPopoutPlaceholder.ui" line="60"/>
        <source>Click to restore</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyLinksConnection</name>
    <message>
        <location filename="../Qt/Components/pqDisplayRepresentationWidget.cxx" line="46"/>
        <source>Change representation type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqDisplayColorWidget.cxx" line="131"/>
        <source>Change </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxyInformationWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="34"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="41"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="54"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="201"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="278"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="370"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="387"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="480"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="493"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="506"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="67"/>
        <source>0 - 100
0 - 100
0 - 100</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="86"/>
        <source>Hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="129"/>
        <source>Hierarchy shown here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="137"/>
        <source>Assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="180"/>
        <source>Assembly shown here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="194"/>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="556"/>
        <source>(n/a)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="227"/>
        <source># of Rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="245"/>
        <source>Data Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="261"/>
        <source># of Vertices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="268"/>
        <source>Extents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="302"/>
        <source>Data Arrays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="329"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="345"/>
        <source>0.000	- 0.000
100.000	- 100.000
200.000	- 200.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="363"/>
        <source># of Cells</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="380"/>
        <source># of Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="400"/>
        <source># of Edges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="437"/>
        <source>Data Grouping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="453"/>
        <source>Memory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="460"/>
        <source>Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="470"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="530"/>
        <source>File Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="546"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="563"/>
        <source># of TimeSteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="570"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="577"/>
        <source>Current Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="584"/>
        <source>1 (range: [0, 1])</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxySelectionWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxySelectionWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxySelectionWidget.ui" line="33"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxyWidgetDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="16"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="59"/>
        <source>Restore application default setting values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="69"/>
        <source>Save current settings values as default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="98"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="110"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="123"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="136"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PythonAnimationCue</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPythonAnimationCue.ui" line="16"/>
        <source>Edit Python Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPythonAnimationCue.ui" line="22"/>
        <source>Script:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RescaleScalarRangeToCustomDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="8"/>
        <source>Set Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="14"/>
        <source>Enter the range for the color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="21"/>
        <source>Enter the new range minimum for the color map here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="24"/>
        <source>minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="31"/>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="62"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="38"/>
        <source>Enter the new range maximum for the color map here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="41"/>
        <source>maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="48"/>
        <source>Enter the range for the opacity map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="55"/>
        <source>Enter the new range minimum for the opacity map here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="69"/>
        <source>Enter the new range maximum for the opacity map here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="76"/>
        <source>If off lock the color map to avoid automatic rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="79"/>
        <source>Enable automatic rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="91"/>
        <source>Apply rescale.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="94"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="101"/>
        <source>Close without rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="104"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="111"/>
        <source>Rescale and update automatic rescaling if changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToCustomDialog.ui" line="114"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RescaleScalarRangeToDataOverTimeDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="22"/>
        <source>Rescale range over time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="37"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Determining range over all timesteps can potentially take a long time to complete. Are you sure you want to continue?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="50"/>
        <source>If off lock the color map to avoid automatic rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="53"/>
        <source>Enable automatic rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="80"/>
        <source>Apply rescale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="83"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="103"/>
        <source>Close without rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="106"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="113"/>
        <source>Rescale and update automatic rescaling if changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRescaleScalarRangeToDataOverTimeDialog.ui" line="116"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScalarValueListPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="52"/>
        <source>Add new entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="55"/>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="69"/>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="83"/>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="110"/>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="124"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="66"/>
        <source>Remove current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="80"/>
        <source>Add a range of values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="107"/>
        <source>Restore default values of the property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="121"/>
        <source>Remove all entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="137"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Value Range:&lt;/span&gt;  [%1, %2]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSearchBox.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSearchBox.ui" line="28"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search for properties by name&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSearchBox.ui" line="31"/>
        <source>Search ... (use Esc to clear text)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSearchBox.ui" line="38"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Toggle advanced properties&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectReaderDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="16"/>
        <source>Open Data With...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="28"/>
        <source>A reader for FileName could not be found.  Please choose one:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="45"/>
        <source>Opening the file with an incompatible reader may result in unpredictable behavior or a crash.  Please choose the correct reader.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="76"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="83"/>
        <source>Set reader as default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="90"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionLinkDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionLinkDialog.ui" line="8"/>
        <source>Selection Link Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionLinkDialog.ui" line="21"/>
        <source>&lt;b&gt;Link Selected Elements:&lt;/b&gt; link selection by evaluating the&lt;br/&gt;selection on the data source and select corresponding&lt;br/&gt;elements based on their indices on other linked data sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionLinkDialog.ui" line="38"/>
        <source>&lt;b&gt;Link Selection:&lt;/b&gt; link selection by sharing the actual selection&lt;br/&gt;between the data sources. The selection is then evaluated for&lt;br/&gt;each linked sources separately.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSettingsDialog.ui" line="16"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSettingsDialog.ui" line="70"/>
        <source>* Restart required for some settings to take effect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>displayRepresentationWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqDisplayRepresentationWidget.ui" line="19"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
</context>
<context>
    <name>pqAboutDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="29"/>
        <source>About ParaView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="52"/>
        <source>Client Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="80"/>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="122"/>
        <source>Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="85"/>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="127"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="94"/>
        <source>Connection Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="154"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.kitware.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:palette(link);&quot;&gt;www.kitware.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="164"/>
        <source>&lt;html&gt;&lt;b&gt;Version: &lt;i&gt;3.x.x&lt;/i&gt;&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="177"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.paraview.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:palette(link);&quot;&gt;www.paraview.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="195"/>
        <source>Copy to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqAboutDialog.ui" line="202"/>
        <source>Save to File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="57"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="90"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="91"/>
        <source>VTK Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="92"/>
        <source>Qt Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="94"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="219"/>
        <source>vtkIdType size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="94"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="219"/>
        <source>%1bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="99"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="227"/>
        <source>Embedded Python</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="99"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="107"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="116"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="127"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="133"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="144"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="207"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="208"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="212"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="227"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="235"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="244"/>
        <source>On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="99"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="107"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="116"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="129"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="135"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="144"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="207"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="208"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="216"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="227"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="235"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="244"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="102"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="230"/>
        <source>Python Library Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="104"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="232"/>
        <source>Python Library Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="107"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="235"/>
        <source>Python Numpy Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="110"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="238"/>
        <source>Python Numpy Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="112"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="240"/>
        <source>Python Numpy Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="115"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="243"/>
        <source>Python Matplotlib Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="119"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="247"/>
        <source>Python Matplotlib Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="121"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="249"/>
        <source>Python Matplotlib Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="127"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="129"/>
        <source>Python Testing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="133"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="135"/>
        <source>MPI Enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="139"/>
        <source>ParaView Build ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="144"/>
        <source>Disable Registry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="145"/>
        <source>Test Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="146"/>
        <source>Data Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="148"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="221"/>
        <source>SMP Backend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="149"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="222"/>
        <source>SMP Max Number of Threads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="158"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="260"/>
        <source>OpenGL Vendor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="159"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="261"/>
        <source>OpenGL Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="160"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="262"/>
        <source>OpenGL Renderer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="164"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="166"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="287"/>
        <source>Accelerated filters overrides available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="164"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="199"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="200"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="201"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="288"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="166"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="191"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="200"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="201"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="288"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="191"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="199"/>
        <source>Remote Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="200"/>
        <source>Separate Render Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="201"/>
        <source>Reverse Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="204"/>
        <source>Number of Processes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="207"/>
        <source>Disable Remote Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="208"/>
        <source>IceT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="212"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="216"/>
        <source>Tile Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="275"/>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="279"/>
        <source>Headless support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="279"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="284"/>
        <source>Not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="307"/>
        <source>Client Information:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="311"/>
        <source>
Connection Information:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="319"/>
        <source>Save to File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="320"/>
        <source>Text Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAboutDialog.cxx" line="320"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationManager</name>
    <message>
        <location filename="../Qt/Components/pqAnimationManager.cxx" line="203"/>
        <source>save animation geometry from a view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationManager.cxx" line="218"/>
        <source>Saving Animation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationTimeWidget</name>
    <message>
        <location filename="../Qt/Components/pqAnimationTimeWidget.cxx" line="239"/>
        <source>max is %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationTrackEditor</name>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="64"/>
        <source>Edit Python cue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="78"/>
        <source>Cannot edit track: python is not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="92"/>
        <source>Data Source to Follow:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="94"/>
        <source>Select Data Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="110"/>
        <source>Editing </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqAnimationTrackEditor.cxx" line="122"/>
        <source>Animation Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqArrayListModel</name>
    <message>
        <location filename="../Qt/Components/pqArrayListWidget.cxx" line="37"/>
        <source>New Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqArrayListWidget.cxx" line="89"/>
        <source>Array Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCameraDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="25"/>
        <source>Adjusting Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="68"/>
        <source>Standard Viewpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="92"/>
        <source>Looking down X axis from (1, 0, 0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="112"/>
        <source>Looking down X axis from (-1, 0, 0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="132"/>
        <source>Looking down Y axis from (0, 1, 0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="152"/>
        <source>Looking down Y axis from (0, -1, 0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="172"/>
        <source>Looking down X axis from (0, 0, 1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="192"/>
        <source>Looking down Z axis from (0, 0, -1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="212"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="263"/>
        <source>Custom Viewpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="303"/>
        <source>Configure custom viewpoint buttons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="306"/>
        <source>Configure ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="334"/>
        <source>Center of Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="369"/>
        <source>Reset center of rotation when camera is reset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="372"/>
        <source>Reset Center of Rotation When Camera is Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="398"/>
        <source>Rotation Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="420"/>
        <source>Define the rotation speed factor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="446"/>
        <source>Camera Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="486"/>
        <source>Focal Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="542"/>
        <source>The separation between eyes (in degrees). This is used when rendering in stereo mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="535"/>
        <source>Eye Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="556"/>
        <source> The size of the camera&apos;s lens in world coordinates. Affects ray traced depth of field rendering.
             </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="549"/>
        <source>Focal Disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="577"/>
        <source>Depth of field rendering focus distance. Only affects ray traced renderers. 0 (default) disables it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="518"/>
        <source>View Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="502"/>
        <source>View Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="590"/>
        <source>Load camera properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="593"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="470"/>
        <source>Position</source>
        <translation type="unfinished">Position</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="600"/>
        <source>Save camera properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="603"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="570"/>
        <source>Focal Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="631"/>
        <source>Manipulate Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="667"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="710"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="753"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="796"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="743"/>
        <source>Azimuth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="693"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="736"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="779"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="825"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="786"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="674"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="717"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="760"/>
        <source>°</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="803"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="700"/>
        <source>Elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="657"/>
        <source>Roll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="834"/>
        <source>Apply a manipulation to the current camera using the buttons on the left.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="866"/>
        <source>Interactive View Link Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="895"/>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="898"/>
        <source>Hide background from linked view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="905"/>
        <source>Opacity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="912"/>
        <source>Define the view link opacity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCameraDialog.ui" line="888"/>
        <source>Select Interactive View Link to manage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="97"/>
        <source>Add Current Viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="673"/>
        <source>Current Viewpoint %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="823"/>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="850"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="825"/>
        <source>Save Camera Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCameraDialog.cxx" line="852"/>
        <source>Load Camera Configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqChangeInputDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqChangeInputDialog.ui" line="16"/>
        <source>Change Input Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqChangeInputDialog.ui" line="31"/>
        <source>Available Input Ports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqChangeInputDialog.ui" line="48"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Select &lt;span style=&quot; font-weight:600;&quot;&gt;INPUT0&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqChangeInputDialog.cxx" line="172"/>
        <source>Select &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCollaborationPanel</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="79"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="84"/>
        <source>Participant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="112"/>
        <source>I&apos;m alone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="131"/>
        <source>Share mouse pointer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="161"/>
        <source>Disable further connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="173"/>
        <source>Server Connect ID </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="204"/>
        <source>Chat room</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorChooserButtonWithPalettes</name>
    <message>
        <location filename="../Qt/Components/pqColorChooserButtonWithPalettes.cxx" line="106"/>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqColorChooserButtonWithPalettes.cxx" line="108"/>
        <source>White</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqComparativeCueWidget</name>
    <message>
        <location filename="../Qt/Components/pqComparativeCueWidget.cxx" line="170"/>
        <source>Parameter Changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeCueWidget.cxx" line="256"/>
        <source>Update Parameter Values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqComparativeParameterRangeDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="16"/>
        <source>Enter Parameter Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="22"/>
        <source>Use comma-separated values to enter multiple values.
However number of values in both entires must match.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="35"/>
        <source>  to  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="60"/>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="63"/>
        <source>Controls the direction in which the parameter is varied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="67"/>
        <source>Vary horizontally first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="72"/>
        <source>Vary vertically first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="77"/>
        <source>Only vary horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="82"/>
        <source>Only vary vertically</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqComparativeVisPanel</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="22"/>
        <source>Layout:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="36"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="66"/>
        <source>Automatic Parameter Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="122"/>
        <source>Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="162"/>
        <source>[Select Parameter]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="187"/>
        <source>Comma-separated values accepted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="197"/>
        <source>Overlay all comparisons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="336"/>
        <source>Add parameter %1 : %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="342"/>
        <source>Add parameter Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="411"/>
        <source>Remove Parameter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqComparativeVisPanelNS</name>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="80"/>
        <source>unrecognized-proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqComparativeVisPanel.cxx" line="89"/>
        <source>unrecognized-property</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqConnectIdDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqConnectIdDialog.ui" line="22"/>
        <source>Connect ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqConnectIdDialog.ui" line="34"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The server requires a connection ID and the provided one does not match.&lt;br/&gt;If you don&apos;t know what is the connection ID for this server, ask its administrator.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqConnectIdDialog.ui" line="53"/>
        <source>Connect ID :</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqContourControls</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="40"/>
        <source>Generate Triangles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="47"/>
        <source>Compute Scalars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="54"/>
        <source>Compute Gradients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="61"/>
        <source>Compute Normals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqContourControls.ui" line="71"/>
        <source>Contour By</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomFilterDefinitionWizard</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="16"/>
        <source>Create Custom Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="95"/>
        <source>&lt;b&gt;Choose a Name&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Enter a name for the custom filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="112"/>
        <source>&lt;b&gt;Define the Inputs&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Select and name the input ports.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="129"/>
        <source>&lt;b&gt;Define the Outputs&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Select and name the output ports.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="146"/>
        <source>&lt;b&gt;Define the Properties&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Select and name the exposed properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="211"/>
        <source>The name will be used to identify the custom filter. It should be unique. The name should also indicate what the custom filter will be used for.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="221"/>
        <source>Custom Filter Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="228"/>
        <source>Enter the custom filter name here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="258"/>
        <source>Select an object from the pipeline layout on the left. Then, select the input property from that object to expose. Give the input port a name and add it to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="302"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="543"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="716"/>
        <source>Object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="307"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="609"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="721"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="312"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="548"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="726"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="323"/>
        <source>Input Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="330"/>
        <source>Input Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="368"/>
        <source>Add Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="381"/>
        <source>Remove Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="394"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="490"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="677"/>
        <source>Move Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="407"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="503"/>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="690"/>
        <source>Move Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="464"/>
        <source>Add Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="477"/>
        <source>Remove Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="519"/>
        <source>Output Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="526"/>
        <source>Output Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="580"/>
        <source>Select an object from the pipeline layout on the left. Give the output port a name and add it to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="616"/>
        <source>Property Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="651"/>
        <source>Add Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="664"/>
        <source>Remove Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="758"/>
        <source>Select an object from the pipeline layout on the left. Then, select the property from that object to expose. Give the property a name and add it to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="835"/>
        <source>&lt; &amp;Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="845"/>
        <source>&amp;Next &gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="874"/>
        <source>&amp;Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="884"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomFilterManager</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="19"/>
        <source>Custom Filter Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="31"/>
        <source>Displays the list of registered custom filter definitions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="60"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="70"/>
        <source>Removes the selected custom filter definitions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="73"/>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="80"/>
        <source>Exports the selected custom filter definitions to a file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="83"/>
        <source>&amp;Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="90"/>
        <source>Imports custom filter definitions from a file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="93"/>
        <source>&amp;Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="205"/>
        <source>Open Custom Filter File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="206"/>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="225"/>
        <source>Custom Filter Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="206"/>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="225"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomFilterManager.cxx" line="224"/>
        <source>Save Custom Filter File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointButtonDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="22"/>
        <source>Configure Custom Viewpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="52"/>
        <source>Viewpoint Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="69"/>
        <source>Assign</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="86"/>
        <source>Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="130"/>
        <source>Add new custom viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="144"/>
        <source>Clear All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="151"/>
        <source>Import...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="158"/>
        <source>Export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="322"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="325"/>
        <source>Load Custom Viewpoints Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="441"/>
        <source>Save Custom Viewpoints Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="511"/>
        <source>Current Viewpoint %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointButtonDialogUI</name>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="85"/>
        <source>This text will be set to the buttons tool tip.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="90"/>
        <source>Current Viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointButtonFileInfo</name>
    <message>
        <location filename="../Qt/Components/pqCustomViewpointButtonDialog.cxx" line="205"/>
        <source>Unnamed Viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDataInformationWidget</name>
    <message>
        <location filename="../Qt/Components/pqDataInformationWidget.cxx" line="152"/>
        <source>Column Titles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDisplayColorWidget</name>
    <message>
        <location filename="../Qt/Components/pqDisplayColorWidget.cxx" line="519"/>
        <source>Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqDisplayColorWidget.cxx" line="610"/>
        <source>Change Color Component</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDisplayRepresentationWidget</name>
    <message>
        <location filename="../Qt/Components/pqDisplayRepresentationWidget.cxx" line="161"/>
        <source>Representation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDoubleVectorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqDoubleVectorPropertyWidget.cxx" line="315"/>
        <source>Reset using current data values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqDoubleVectorPropertyWidget.cxx" line="347"/>
        <source>Reset to active data bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExpressionChooserButton</name>
    <message>
        <location filename="../Qt/Components/pqExpressionChooserButton.cxx" line="18"/>
        <source>Choose Expression</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExpressionsManagerDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="16"/>
        <source>Choose Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="83"/>
        <source>Use current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="86"/>
        <source>Use expression in current filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="93"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="96"/>
        <source>Add a new expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="106"/>
        <source>Remove selected expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="109"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="119"/>
        <source>Remove all expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="122"/>
        <source>Remove All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="136"/>
        <source>Import expressions from a json file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="139"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="146"/>
        <source>Export expressions to a json file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="149"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="169"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="172"/>
        <source>Close without saving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="179"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="182"/>
        <source>Save to settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="189"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="192"/>
        <source>Save and close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="380"/>
        <source>Remove all expressions ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="460"/>
        <source>Export Expressions(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="461"/>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="501"/>
        <source>ParaView Expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="461"/>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="501"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="492"/>
        <source>Failed to save expressions to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsDialog.cxx" line="500"/>
        <source>Import Expressions(s)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExpressionsWidget</name>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="62"/>
        <source>Save expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="67"/>
        <source>Open Expressions Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="72"/>
        <source>Clear expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="81"/>
        <source>Saved! </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqExpressionsWidget.cxx" line="81"/>
        <source>Already Exists</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFavoritesDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="17"/>
        <source>Favorites Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="26"/>
        <source>&lt;&lt;&lt; Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="39"/>
        <source>Available Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="82"/>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="156"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="116"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="164"/>
        <source>Add Category ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="200"/>
        <source>Add &gt;&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFileChooserWidget</name>
    <message>
        <location filename="../Qt/Components/pqFileChooserWidget.cxx" line="146"/>
        <source>Open Directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFileChooserWidget.cxx" line="150"/>
        <source>Save File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFileChooserWidget.cxx" line="154"/>
        <source>Open File:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFindDataSelectionDisplayFrame</name>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="142"/>
        <source>not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="160"/>
        <source>%1 (partial)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="223"/>
        <source>Change labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="398"/>
        <source>Interactive selection label properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="400"/>
        <source>Interactive Selection Label Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="436"/>
        <source>Change selection display properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="438"/>
        <source>Selection Label Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqHelpWindow</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="16"/>
        <source>Online Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="47"/>
        <source>Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="56"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="65"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="84"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="93"/>
        <source>Go backward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="102"/>
        <source>Go forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqHelpWindow.ui" line="111"/>
        <source>Save as homepage</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqIconBrowser</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="16"/>
        <source>Browse icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="88"/>
        <source>Import new icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="91"/>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="105"/>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="132"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="102"/>
        <source>Remove custom icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqIconBrowser.ui" line="129"/>
        <source>Remove all custom icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="40"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="101"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="102"/>
        <source>All custom icons will be deleted. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="147"/>
        <source>Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqIconBrowser.cxx" line="150"/>
        <source>Load Icon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqInputSelectorWidget</name>
    <message>
        <location filename="../Qt/Components/pqInputSelectorWidget.cxx" line="129"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqInternals</name>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="96"/>
        <source>Resize Frame</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqItemViewSearchWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="86"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="138"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="160"/>
        <source>Next (ALT+N)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="189"/>
        <source>Match Case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="208"/>
        <source>Close (Esc)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="237"/>
        <source>Previous (ALT+P)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqKeyFrameEditor</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="50"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="63"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="60"/>
        <source>Add a new keyframe before selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="73"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="70"/>
        <source>Delete selected keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="83"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="80"/>
        <source>Delete all keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="90"/>
        <source>Import key frames from a .pvkfc file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="93"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="100"/>
        <source>Export key frames to a .pvkfc file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="103"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="157"/>
        <source>Create Orbit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="154"/>
        <source>Create an orbit path for selected keyframe, starting with current camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="123"/>
        <source>Apply to Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="120"/>
        <source>Apply selected keyframe configuration to the current view camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="133"/>
        <source>Use current Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="130"/>
        <source>Use current camera for selected keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="143"/>
        <source>Spline Interpolation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="140"/>
        <source>Use spline interpolation for this cue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="464"/>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="470"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="465"/>
        <source>Camera Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="471"/>
        <source>Interpolation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="471"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="541"/>
        <source>Edit Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="837"/>
        <source>Load Custom KeyFrames Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="943"/>
        <source>Save Current KeyFrames Configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqKeyFrameEditorDialog</name>
    <message>
        <location filename="../Qt/Components/pqKeyFrameEditor.cxx" line="53"/>
        <source>Key Frame Interpolation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqKeyFrameTypeWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="24"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="39"/>
        <source>Interpolation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="46"/>
        <location filename="../Qt/Components/pqKeyFrameTypeWidget.cxx" line="32"/>
        <source>Exponential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="58"/>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="162"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="65"/>
        <source>Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="72"/>
        <source>Start Power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="79"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="86"/>
        <source>End Power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="93"/>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="169"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="119"/>
        <location filename="../Qt/Components/pqKeyFrameTypeWidget.cxx" line="34"/>
        <source>Sinusoid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="131"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="138"/>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="145"/>
        <source>Phase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameTypeWidget.cxx" line="30"/>
        <source>Ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqKeyFrameTypeWidget.cxx" line="35"/>
        <source>Boolean</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLightsEditor</name>
    <message>
        <location filename="../Qt/Components/pqLightsEditor.cxx" line="94"/>
        <source>Restore Default Lights</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLightsInspector</name>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="153"/>
        <source>Light %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="164"/>
        <source>Move to Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="168"/>
        <source>Match this light&apos;s position and focal point to the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="174"/>
        <source>Reset Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="178"/>
        <source>Reset this light parameters to default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="183"/>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="338"/>
        <source>Remove Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="187"/>
        <source>Remove this light.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="255"/>
        <source>Add Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="335"/>
        <source>remove light added to the view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="374"/>
        <source>reset a light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLightsInspector.cxx" line="398"/>
        <source>update a light</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLinksEditor</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="16"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="106"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="116"/>
        <source>Mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="124"/>
        <source>Object Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="129"/>
        <source>Property Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="134"/>
        <source>Selection Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="164"/>
        <source>Interactive View Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="157"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="174"/>
        <source>Camera Widget View Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="184"/>
        <source>&lt;span&gt;When enabled, selection is linked by evaluating the selection on the data source and select corresponding elements based on their indices on other linked data sources, instead of sharing the actual selection between the data sources.&lt;/span&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksEditor.ui" line="187"/>
        <source>Link Selected Elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="103"/>
        <source>Source %1 in View %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="256"/>
        <source>Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="258"/>
        <source>Representations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="260"/>
        <source>Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="262"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksEditor.cxx" line="793"/>
        <source>Linking views will link only cameras.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLinksManager</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksManager.ui" line="16"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksManager.ui" line="36"/>
        <source>Add...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksManager.ui" line="43"/>
        <source>Edit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLinksManager.ui" line="50"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksManager.cxx" line="56"/>
        <source>Add Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLinksManager.cxx" line="100"/>
        <source>Edit Link</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLiveInsituManager</name>
    <message>
        <location filename="../Qt/Components/pqLiveInsituManager.cxx" line="205"/>
        <source>Catalyst Server Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLiveInsituManager.cxx" line="206"/>
        <source>Enter the port number to accept connections
from Catalyst on:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLiveInsituManager.cxx" line="253"/>
        <source>Catalyst Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLiveInsituManager.cxx" line="254"/>
        <source>Connection to Catalyst Co-Processor has been terminated involuntarily. This implies either a communication error, or that the Catalyst co-processor has terminated. The Catalyst session will now be cleaned up. You can start a new one if you want to monitor for additional Catalyst connection requests.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLockViewSizeCustomDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLockViewSizeCustomDialog.ui" line="16"/>
        <source>Lock View to Custom Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLockViewSizeCustomDialog.ui" line="22"/>
        <source>Select Maximum Resolution for Each View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLockViewSizeCustomDialog.ui" line="41"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLockViewSizeCustomDialog.cxx" line="25"/>
        <source>Unlock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLogViewerDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="16"/>
        <source>Log Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="53"/>
        <source>New Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="81"/>
        <source>Application process to log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="94"/>
        <source>Process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="101"/>
        <source>Rank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="114"/>
        <source>Rank of process to log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="129"/>
        <source>Add Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="151"/>
        <source>Global Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="177"/>
        <source>Global filter that applies to all logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="183"/>
        <source>Set filter on all logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="205"/>
        <source>Verbosity Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="225"/>
        <source>Client Verbosity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="235"/>
        <source>Server Verbosity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="245"/>
        <source>Data Server Verbosity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="255"/>
        <source>Render Server Verbosity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="286"/>
        <source>Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="306"/>
        <source>Always Log Messages About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="328"/>
        <source>Data Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="335"/>
        <source>Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="342"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="349"/>
        <source>Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="356"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="363"/>
        <source>Filter Execution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="395"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="402"/>
        <source>Clear Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="118"/>
        <source>Data Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="119"/>
        <source>Render Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="132"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="366"/>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="367"/>
        <source>Close log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="413"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="414"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="415"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="416"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqLogViewerDialog.cxx" line="417"/>
        <source>TRACE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMemoryInspectorPanel</name>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="524"/>
        <source>System Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1471"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1480"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1472"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1481"/>
        <source>No process selected. Select a specific process first by clicking on its entry in the tree view widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1585"/>
        <source>Client System Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1586"/>
        <source>Server System Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1587"/>
        <source>Host:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1588"/>
        <source>OS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1589"/>
        <source>CPU:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1590"/>
        <source>Memory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1623"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1663"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1676"/>
        <source>properties...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1634"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1669"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1690"/>
        <source>remote command...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1641"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1682"/>
        <source>show only nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1642"/>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1683"/>
        <source>show all ranks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMemoryInspectorPanel.cxx" line="1704"/>
        <source>stack trace signal handler</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMemoryInspectorPanelForm</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMemoryInspectorPanelForm.ui" line="22"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqMemoryInspectorPanelForm.ui" line="78"/>
        <source>Auto-update</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMultiBlockPropertiesEditorWidget</name>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesEditorWidget.cxx" line="106"/>
        <location filename="../Qt/Components/pqMultiBlockPropertiesEditorWidget.cxx" line="138"/>
        <location filename="../Qt/Components/pqMultiBlockPropertiesEditorWidget.cxx" line="171"/>
        <source>Change </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMultiBlockPropertiesStateWidget</name>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="187"/>
        <source>Properties are </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="187"/>
        <source>Property is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="189"/>
        <source>disabled because no blocks are selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="191"/>
        <source>inherited from the representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="193"/>
        <source>inherited from block(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="195"/>
        <source>inherited from block(s) and the representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="196"/>
        <source>set in block(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="198"/>
        <source>set in block(s) and inherited from the representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="200"/>
        <source>set in block(s) and inherited from block(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="202"/>
        <source>set in block(s) and inherited from block(s) and the representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="218"/>
        <source>Reset to value(s) inherited from block/representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiBlockPropertiesStateWidget.cxx" line="356"/>
        <source>Reset </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMultiViewWidget</name>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="731"/>
        <source>Split View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="755"/>
        <source>Close View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="785"/>
        <source>Destroy all views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="850"/>
        <source>Swap Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="931"/>
        <source>Exit preview mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqMultiViewWidget.cxx" line="932"/>
        <source>Enter preview mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPipelineBrowserWidget</name>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="255"/>
        <source>Show %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="256"/>
        <source>Hide %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="260"/>
        <source>Show Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="260"/>
        <source>Hide Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineBrowserWidget.cxx" line="343"/>
        <source>You might have added a new scalar bar mode, you need to do something here, skipping.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPipelineModel</name>
    <message>
        <location filename="../Qt/Components/pqPipelineModel.cxx" line="891"/>
        <source>Rename %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPipelineTimeKeyFrameEditor</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="16"/>
        <source>Dialog</source>
        <translation type="unfinished">Dialog</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="44"/>
        <source>Variable Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="74"/>
        <source>Constant Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="93"/>
        <source>Animation Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPipelineTimeKeyFrameEditor.cxx" line="104"/>
        <source>Edit Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPluginDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="16"/>
        <source>Plugin Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="22"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="41"/>
        <source>Remote Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="73"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="157"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="81"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="165"/>
        <source>Load New ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="101"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="185"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="91"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="175"/>
        <source>Load Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="115"/>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="199"/>
        <source>Add plugin configuration file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPluginDialog.ui" line="125"/>
        <source>Local Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="64"/>
        <source>Local plugins are automatically searched for in %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="70"/>
        <source>Remote plugins are automatically searched for in %1.
Local plugins are automatically searched for in %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="120"/>
        <source>Qt binary resource files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="121"/>
        <source>XML plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="124"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="129"/>
        <source>Binary plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="157"/>
        <source>Supported plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="205"/>
        <source>Plugin config file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="267"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="267"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="334"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="554"/>
        <source>Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="342"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="554"/>
        <source>Not Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="350"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="359"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="369"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="379"/>
        <source>Required Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="389"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="401"/>
        <source>Auto Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="409"/>
        <source>Delayed Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="484"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="496"/>
        <source>Remove plugin?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="485"/>
        <location filename="../Qt/Components/pqPluginDialog.cxx" line="497"/>
        <source>Are you sure you want to remove this plugin?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPresetDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="16"/>
        <source>Choose Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="56"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Tip: &amp;lt;click&amp;gt; to select, &amp;lt;double-click&amp;gt; to apply a preset.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="65"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Options to load:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uncheck to not load colors from the selected preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="82"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="95"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uncheck to not load opacities from the selected preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="98"/>
        <source>Opacities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="111"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uncheck to not load annotations from the selected preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="114"/>
        <source>Annotations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="127"/>
        <source>Check to use following regexp when loading annotations from the selected preset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="130"/>
        <source>Use Regular Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="143"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This regexp will be applied to seriesName and preset value. If any, matching groups are used, otherwise whole matching. &lt;/p&gt;
           E.g. series name is `y1 ˓custom˒`, preset have `y1 ˓generic˒` and `z1 ˓custom˒`. Then with the regexp `(.*) ˓.*˒` the `y1 ˓custom˒` series will match with `y1 ˓generic˒`, whereas with `.* (˓custom˒)` it will match with `z1 ˓custom˒`.
           &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
       </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="149"/>
        <source>(.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="159"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to use data range specified in the preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="162"/>
        <source>Use preset range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="182"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Actions on selected:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="196"/>
        <source>Show current preset
in default mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="220"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="227"/>
        <source>Import a preset from a json file, imported preset will appear in italics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="230"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="240"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="250"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPresetDialog.ui" line="257"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="869"/>
        <source>Import Presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="870"/>
        <source>Supported Presets/Color Map Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="871"/>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="911"/>
        <source>ParaView Color/Opacity Presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="871"/>
        <source>Legacy Color Maps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="872"/>
        <source>VisIt Color Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="872"/>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="911"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="910"/>
        <source>Export Preset(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPresetDialog.cxx" line="941"/>
        <source>Failed to save preset collection to </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPropertiesPanel</name>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="457"/>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="475"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="529"/>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="536"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="581"/>
        <source>View (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="590"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="765"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqPropertiesPanel.cxx" line="829"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqPropertyWidget.cxx" line="131"/>
        <source>Property Changed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyAction</name>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="63"/>
        <source>(deprecated) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="224"/>
        <source>Requires an input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="239"/>
        <source>Not supported in parallel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="243"/>
        <source>Supported only in parallel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyAction.cxx" line="255"/>
        <source>Multiple inputs not supported</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyEditorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqProxyEditorPropertyWidget.cxx" line="26"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyInformationWidget</name>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="395"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="396"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="500"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="555"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="564"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="588"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="589"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="590"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="618"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="619"/>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="620"/>
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="529"/>
        <source>Data Statistics (# of datasets: %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="530"/>
        <source>Data Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="570"/>
        <source>%1 to %2 (delta: %3)
%4 to %5 (delta: %6)
%7 to %8 (delta: %9)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="599"/>
        <source>%1 to %2 (dimension: %3)
%4 to %5 (dimension: %6)
%7 to %8 (dimension: %9)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="636"/>
        <source>%1 (range: [%2, %3])</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyInformationWidget.cxx" line="671"/>
        <source>Show hierarchy / assembly as text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyWidget</name>
    <message>
        <location filename="../Qt/Components/pqProxyWidget.cxx" line="1570"/>
        <source>Use as Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqProxyWidget.cxx" line="1584"/>
        <source>Reset to Application Default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRecentFilesMenu</name>
    <message>
        <location filename="../Qt/Components/pqRecentFilesMenu.cxx" line="190"/>
        <source>Disconnect from current server?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqRecentFilesMenu.cxx" line="191"/>
        <source>The file you opened requires connecting to a new server.
The current connection will be closed.

Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRemoteCommandDialog</name>
    <message>
        <location filename="../Qt/Components/pqRemoteCommandDialog.cxx" line="543"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqRemoteCommandDialog.cxx" line="545"/>
        <source>Find file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRemoteCommandDialogForm</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="28"/>
        <source>Remote Command Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="34"/>
        <source>Command Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="95"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="126"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="154"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="172"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="181"/>
        <source>FE_URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="191"/>
        <source>ssh url to cluster login (eg user@login.com)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="198"/>
        <source>SSH_EXEC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="207"/>
        <source>path to ssh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="246"/>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="303"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="255"/>
        <source>TERM_EXEC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="264"/>
        <source>path to terminal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="312"/>
        <source>TERM_OPTS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="319"/>
        <source>terminal options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="329"/>
        <source>PV_HOST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="342"/>
        <source>Command Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="385"/>
        <source>execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="395"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRemoteCommandTemplateDialogForm</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandTemplateDialogForm.ui" line="16"/>
        <source>Remote Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandTemplateDialogForm.ui" line="22"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqRemoteCommandTemplateDialogForm.ui" line="36"/>
        <source>Template:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqScalarValueListPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqScalarValueListPropertyWidget.cxx" line="353"/>
        <location filename="../Qt/Components/pqScalarValueListPropertyWidget.cxx" line="354"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectReaderDialog</name>
    <message>
        <location filename="../Qt/Components/pqSelectReaderDialog.cxx" line="65"/>
        <source>A reader for &quot;%1&quot; could not be found. Please choose one:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectReaderDialog.cxx" line="80"/>
        <source>More than one reader for &quot;%1&quot; found. Please choose one:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectionInputWidget</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionInputWidget.ui" line="23"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionInputWidget.ui" line="50"/>
        <source>Copy Active Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSelectionInputWidget.ui" line="72"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="81"/>
        <source>Copied Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="76"/>
        <source>No selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="92"/>
        <source>Number of Selections: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="93"/>
        <source>Selection Expression: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="95"/>
        <source>Invert Selection: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="96"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="97"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="114"/>
        <source>Elements: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="120"/>
        <source>Type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="123"/>
        <source>Frustum Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="123"/>
        <source>Values:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="142"/>
        <source>Values Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="142"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="317"/>
        <source>Array Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="144"/>
        <source>Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="162"/>
        <source>Pedigree ID Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="167"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="185"/>
        <source>Pedigree Domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="167"/>
        <source>String ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="185"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="202"/>
        <source>Global ID Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="202"/>
        <source>Global ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="214"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="233"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="252"/>
        <source>ID Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="214"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="234"/>
        <source>Process ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="215"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="234"/>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="253"/>
        <source>Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="233"/>
        <source>Composite ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="253"/>
        <source>Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="253"/>
        <source>Dataset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="272"/>
        <source>Location-based Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="273"/>
        <source>Probe Locations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="291"/>
        <source>Block Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="291"/>
        <source>Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="304"/>
        <source>Block Selectors Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="304"/>
        <source>Assembly Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="306"/>
        <source>Block Selectors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="317"/>
        <source>Threshold Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="319"/>
        <source>Thresholds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="337"/>
        <source>Query Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="337"/>
        <source>Query: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="349"/>
        <source>Number of Selections: 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionInputWidget.cxx" line="433"/>
        <source>Copied Selection (Active Selection Changed)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectionManager</name>
    <message>
        <location filename="../Qt/Components/pqSelectionManager.cxx" line="175"/>
        <source>clear all selections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSelectionManager.cxx" line="193"/>
        <source>clear selection for source</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerConnectDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="16"/>
        <source>Choose Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="40"/>
        <source>Add Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="50"/>
        <source>Edit Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="60"/>
        <source>Delete Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="67"/>
        <source>Load Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="74"/>
        <source>Save Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="81"/>
        <source>Fetch Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="105"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="108"/>
        <source>Delete all user server configurations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="111"/>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="661"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="135"/>
        <source>Timeout (s) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="142"/>
        <source>Specify the timeout in seconds to use when waiting for server connection. Set it to -1 to wait indefinitely. This value will be saved in your settings for this server once you click connect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="161"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="171"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="214"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="219"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="666"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="231"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="247"/>
        <source>Server Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="258"/>
        <source>Client / Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="263"/>
        <source>Client / Server (reverse connection)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="268"/>
        <source>Client / Data Server / Render Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="273"/>
        <source>Client / Data Server / Render Server (reverse connection)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="281"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="288"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="322"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="356"/>
        <source>localhost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="295"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="315"/>
        <source>Data Server host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="329"/>
        <source>Data Server port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="349"/>
        <source>Render Server host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="363"/>
        <source>Render Server port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="414"/>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="421"/>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="714"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="434"/>
        <source>Configure server foobar (cs://foobar)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="441"/>
        <source>Please configure the startup procedure to be used when connecting to this server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="468"/>
        <source>Startup Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="479"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="484"/>
        <source>Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="526"/>
        <source>Manual Startup - no attempt will be made to start the server.  You must start the server manually before trying to connect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="549"/>
        <source>Execute an external command to start the server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="567"/>
        <source>After executing the startup command, wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="574"/>
        <source> seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="590"/>
        <source>before connecting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="661"/>
        <source>Configuration Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="671"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="681"/>
        <source>Edit Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="704"/>
        <source>Import Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="730"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Enter list of URLs to obtain the servers configurations from, using the syntax:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;lt;type&amp;gt; &amp;lt;url&amp;gt; &amp;lt;userfriendly name&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="45"/>
        <source>Enter list of URLs to obtain server configurations from.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="47"/>
        <source>Syntax:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="48"/>
        <source>userfriendly-name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="49"/>
        <source>Official Kitware Server Configurations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="230"/>
        <source>Edit Server Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="234"/>
        <source>Edit Server Launch Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="238"/>
        <source>Fetch Server Configurations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="242"/>
        <source>Edit Server Configuration Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="247"/>
        <source>Choose Server Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="302"/>
        <source>My Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="645"/>
        <source>Delete Server Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="646"/>
        <source>Are you sure you want to delete &quot;%1&quot;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="661"/>
        <source>All servers will be deleted. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="675"/>
        <source>Save Server Configuration File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="693"/>
        <source>Load Server Configuration File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="830"/>
        <source>Fetching configurations ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="851"/>
        <source>Authenticate Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="853"/>
        <source>%1 at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="856"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="860"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerConnectDialog.cxx" line="861"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerLauncher</name>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="223"/>
        <source>Connection Options for &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="616"/>
        <source>Establishing connection to &apos;%1&apos;
Waiting for server to connect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="619"/>
        <source>Waiting for Server Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="623"/>
        <source>Establishing connection to &apos;%1&apos;
Waiting %2 seconds for connection to server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="627"/>
        <source>Waiting for Connection to Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="643"/>
        <source>Connection Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="644"/>
        <source>Unable to connect sucessfully. Try again for %1 seconds ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqServerLauncher.cxx" line="746"/>
        <source>Launching server &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerLauncherDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerLauncherDialog.ui" line="16"/>
        <source>Starting Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerLauncherDialog.ui" line="28"/>
        <source>Please wait while server cs://foobar starts ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqServerLauncherDialog.ui" line="59"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSetBreakpointDialog</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="16"/>
        <source>Set Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="22"/>
        <source>Breakpoint:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="32"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="52"/>
        <source>Time Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSetBreakpointDialog.cxx" line="64"/>
        <source>Breakpoint time is invalid or it is smaller than current time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqShaderReplacementsSelectorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="61"/>
        <source>List of previously loaded Json files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="67"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="68"/>
        <source>Load a Json preset file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="74"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="75"/>
        <source>Delete the selected preset from the preset list or clear the current shader replacement string.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="138"/>
        <source>Shader Replacements Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="168"/>
        <source>Shader replacements files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="168"/>
        <source>All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="169"/>
        <source>Open ShaderReplacements:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSignalAdaptorKeyFrameType</name>
    <message>
        <location filename="../Qt/Components/pqSignalAdaptorKeyFrameType.cxx" line="99"/>
        <source>Amplitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqSignalAdaptorKeyFrameType.cxx" line="103"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSpreadSheetColumnsVisibility</name>
    <message>
        <location filename="../Qt/Components/pqSpreadSheetColumnsVisibility.cxx" line="128"/>
        <source>All Columns</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqStringVectorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqStringVectorPropertyWidget.cxx" line="143"/>
        <source>Select %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTabWidget</name>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="120"/>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="121"/>
        <source>Close layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="134"/>
        <source>Bring popped out window back to the frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="135"/>
        <source>Pop out layout in separate window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTabbedMultiViewWidget</name>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="689"/>
        <source>Remove View Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="719"/>
        <source>Add View Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="763"/>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="920"/>
        <source>Close Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="910"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="911"/>
        <source>Close layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="912"/>
        <source>Equalize Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="915"/>
        <source>Both</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="913"/>
        <source>Horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="914"/>
        <source>Vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="929"/>
        <source>Rename Layout...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTabbedMultiViewWidget.cxx" line="929"/>
        <source>New name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTextureComboBox</name>
    <message>
        <location filename="../Qt/Components/pqTextureComboBox.cxx" line="99"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureComboBox.cxx" line="102"/>
        <source>Load ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureComboBox.cxx" line="137"/>
        <source>Open Texture:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTextureSelectorPropertyWidget</name>
    <message>
        <location filename="../Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="32"/>
        <source>Select/Load texture to apply.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="86"/>
        <source>Texture Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="117"/>
        <source>No tcoords present in the data. Cannot apply texture.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="123"/>
        <source>No tangents present in the data. Cannot apply texture.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTimerLogDisplay</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="16"/>
        <source>Timer Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="36"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="43"/>
        <source>Clear</source>
        <translation type="unfinished">Löschen</translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="71"/>
        <source>Time Threshold:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="104"/>
        <source>Buffer Length:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="116"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="126"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqTimerLogDisplay.cxx" line="264"/>
        <source>Save Timer Log</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTransferFunction2DWidget</name>
    <message>
        <location filename="../Qt/Components/pqTransferFunction2DWidget.cxx" line="214"/>
        <source>Double click to add a box. Grab and drag to move the box.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTransferFunctionWidget</name>
    <message>
        <location filename="../Qt/Components/pqTransferFunctionWidget.cxx" line="698"/>
        <source>Drag a handle to change the range. Double click it to set custom range. Return/Enter to edit color.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqViewFrame</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqViewFrame.ui" line="19"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="63"/>
        <source>Split Vertical Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="66"/>
        <source>Split Horizontal Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="70"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="74"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/pqViewFrame.cxx" line="78"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>propertiesPanel</name>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="16"/>
        <source>Properties Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="27"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="41"/>
        <source>Resets any changed properties to their values from the last time &apos;Apply&apos; was clicked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="44"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="55"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="66"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="73"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="132"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="151"/>
        <source>Copy properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="174"/>
        <source>Paste copied properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="194"/>
        <source>Restore application default setting values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="214"/>
        <source>Save current settings values as default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="252"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="271"/>
        <source>Copy display properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="294"/>
        <source>Paste copied display properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="314"/>
        <source>Restore application default setting values for display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="334"/>
        <source>Save current display settings values as default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="372"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="391"/>
        <source>Copy view properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="414"/>
        <source>Paste view properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="434"/>
        <source>Restore application default setting values for view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="454"/>
        <source>Save current view settings values as default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
