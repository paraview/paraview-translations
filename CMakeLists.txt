cmake_minimum_required(VERSION 3.12)
project(ParaViewTranslations LANGUAGES C CXX)

set(PARAVIEW_TRANSLATIONS_QT_VERSION "Auto" CACHE STRING "Qt version to use")
set_property(CACHE PARAVIEW_TRANSLATIONS_QT_VERSION PROPERTY STRINGS Auto 6 5)
if (PARAVIEW_TRANSLATIONS_QT_VERSION STREQUAL "Auto")
  find_package(Qt6 QUIET COMPONENTS Core LinguistTools)
  if (Qt6_FOUND)
    set(PARAVIEW_TRANSLATIONS_QT_VERSION "6")
  else ()
    find_package(Qt5 QUIET COMPONENTS Core LinguistTools)
    if (Qt5_FOUND)
      set(PARAVIEW_TRANSLATIONS_QT_VERSION "5")
    endif ()
  endif ()
elseif (NOT PARAVIEW_TRANSLATIONS_QT_VERSION STREQUAL "5" AND
        NOT PARAVIEW_TRANSLATIONS_QT_VERSION STREQUAL "6")
  message(FATAL_ERROR
    "Unknown Qt version `${PARAVIEW_TRANSLATIONS_QT_VERSION}`")
else ()
  find_package("Qt${PARAVIEW_TRANSLATIONS_QT_VERSION}" COMPONENTS LinguistTools)
endif ()

if (NOT Qt${PARAVIEW_TRANSLATIONS_QT_VERSION}_FOUND)
  message(FATAL_ERROR
    "Failed to locate Qt LinguistTools")
endif ()

set(default "share/translations")
if (DEFINED INSTALL_SUBDIR)
  set(default "${INSTALL_SUBDIR}")
  unset(INSTALL_SUBDIR CACHE)
endif ()
set(CMAKE_INSTALL_TRANSLATIONDIR "${default}"
  CACHE STRING "Destination of the translation files in the prefix")
set(default OFF)
if (DEFINED PROVIDE_QT_TRANSLATIONS)
  set(default "${PROVIDE_QT_TRANSLATIONS}")
  unset(PROVIDE_QT_TRANSLATIONS CACHE)
endif ()
option(PARAVIEW_TRANSLATIONS_INSTALL_QT_TRANSLATIONS "Build and install official Qt translation files" "${default}")

if (PARAVIEW_TRANSLATIONS_INSTALL_QT_TRANSLATIONS)
  set(qt_ts_prefix
    qt
    qtbase
    qtscript
    qtmultimedia
    qtxmlpatterns)
endif ()

set(ts_files
  Clients_ParaView.ts
  Qt_ApplicationComponents.ts
  Qt_Core.ts
  Qt_Widgets.ts
  Clients_ParaView-XMLs.ts
  Qt_Components.ts
  Qt_Python.ts
  ServerManager-XMLs.ts
  )

set(languages
  de_DE
  en
  es_ES
  fr_FR
  it_IT
  ja_JP
  ko_KR
  nl_NL
  pt_BR
  tr_TR
  )

add_custom_target("files_update")

foreach (output_language IN LISTS languages)
  if (PARAVIEW_TRANSLATIONS_INSTALL_QT_TRANSLATIONS)
    # Get the language code without the country code
    string(REGEX MATCH "^([a-z]+)"
      _language_code "${output_language}")

    foreach (_qt_component IN LISTS qt_ts_prefix)
      if ("${output_language}" STREQUAL "en")
        # Skip en as qt does not provide it.
        continue()
      endif()
      if ("${output_language}" STREQUAL "tr_TR" AND ${_qt_component} STREQUAL "qtxmlpatterns")
        # Skip qtxmlpatterns_tr.ts as qt does not provide it.
        continue()
      endif()

      # Create qm generation targets
      set(_qt_ts "${_qt_component}_${_language_code}")
      set(source_ts "${CMAKE_SOURCE_DIR}/qttranslations/translations/${_qt_ts}.ts")
      if (NOT EXISTS "${source_ts}")
        set(_qt_ts "${_qt_component}_${output_language}")
        set(source_ts "${CMAKE_SOURCE_DIR}/qttranslations/translations/${_qt_ts}.ts")
      endif()

      set(destination_qt_qm "${CMAKE_BINARY_DIR}/${_qt_ts}.qm")

      add_custom_command(
        OUTPUT "${destination_qt_qm}"
        COMMAND "$<TARGET_FILE:Qt${PARAVIEW_TRANSLATIONS_QT_VERSION}::lconvert>" ${source_ts} -o "${destination_qt_qm}"
        DEPENDS "${source_ts}")
      add_custom_target("${_language_code}_${_qt_ts}" ALL DEPENDS "${destination_qt_qm}")
      install(
        FILES "${destination_qt_qm}"
        DESTINATION "${CMAKE_INSTALL_TRANSLATIONDIR}"
      )
    endforeach ()
  endif ()

  set(ts_language_files ${ts_files})
  list(TRANSFORM ts_language_files PREPEND "${CMAKE_SOURCE_DIR}/${output_language}/")
  # Create qm generation targets
  set(destination_qm "${CMAKE_BINARY_DIR}/paraview_${output_language}.qm")
  add_custom_command(
    OUTPUT "${destination_qm}"
    COMMAND "$<TARGET_FILE:Qt${PARAVIEW_TRANSLATIONS_QT_VERSION}::lconvert>" ${ts_language_files} -o "${destination_qm}"
    DEPENDS ${ts_language_files})
  add_custom_target("${output_language}" ALL DEPENDS "${destination_qm}")
  if ("${output_language}" STREQUAL "en")
    # en files will never be used by ParaView
    continue()
  endif()
  install(
    FILES "${destination_qm}"
    DESTINATION "${CMAKE_INSTALL_TRANSLATIONDIR}"
  )

  # Create ts files update target
  if ("${output_language}" STREQUAL "en")
    # Skip en as they are the template files
    continue()
  endif()
  add_custom_target("${output_language}_update")
  foreach (ts_file IN LISTS ts_files)
    set(absolute_ts_file "${CMAKE_SOURCE_DIR}/${output_language}/${ts_file}")
    set(absolute_en_file "${CMAKE_SOURCE_DIR}/en/${ts_file}")
    add_custom_target("${output_language}_${ts_file}_update")
    add_custom_command(
      TARGET "${output_language}_${ts_file}_update"
      COMMAND "$<TARGET_FILE:Qt${PARAVIEW_TRANSLATIONS_QT_VERSION}::lupdate>" "${absolute_en_file}" -ts "${absolute_ts_file}" -target-language ${output_language}
      )
    add_dependencies("${output_language}_update" "${output_language}_${ts_file}_update")
  endforeach ()
  add_dependencies("files_update" "${output_language}_update")
endforeach ()
